export NoOutput, OnlyConsole, OnlyPlots, ConsoleAndPlots
export log_verbose, log_plot, log_console, log_all

VerbosityType = Union{DataType,Union}

abstract type Verbosity end
"**No output** is allowed"
struct NoOutput <: Verbosity end
"Only **console** output is allowed"
struct OnlyConsole <: Verbosity end
"Only **plot** output is allowed"
struct OnlyPlots <: Verbosity end

"Both **console and plot** outputs are allowed"
ConsoleAndPlots = Union{OnlyConsole,OnlyPlots}

DEFAULT_VERBOSITY = ConsoleAndPlots

abstract type VerboseOutput end
"Block or command requires **console** output permission"
struct ConsoleOutput <: VerboseOutput end
"Block or command requires **plot** output permission"
struct PlotOutput <: VerboseOutput end

function log_verbose(permisions::DataType...)
    log_verbose([permisions...])
end

"Returns the value of `VERBOSE` const if set up, otherwise uses `DEFAULT_VERBOSITY = ConsoleAndPlots`"
function get_verbosity()::VerbosityType
    if (!isdefined(Main, :VERBOSE))
        return DEFAULT_VERBOSITY
    end

    return Main.VERBOSE
end

"""
Returns whether logging command/block should be executed based on the value of VERBOSE global constant
"""
function log_verbose(permissions::Vector{DataType})
    all_ok = true
    for permission in permissions
        all_ok = all_ok && verbosity_rules(get_verbosity(), permission)
    end

    return all_ok
end

"""
Definition of verbosity rules used by function `log_verbose`
"""
function verbosity_rules(allowed_verbosity::VerbosityType, permission_wanted::DataType)
    console = permission_wanted <: ConsoleOutput && (
        allowed_verbosity <: OnlyConsole || (
            allowed_verbosity >: OnlyConsole &&
            allowed_verbosity != Verbosity
        )
    )


    plots = permission_wanted <: PlotOutput && (
        allowed_verbosity <: OnlyPlots || (
            allowed_verbosity >: OnlyPlots &&
            allowed_verbosity != Verbosity
        )
    )

    return console || plots
end

"Returns whether **console** output is allowed by VERBOSE"
log_console() = log_verbose(ConsoleOutput)

"Returns whether **plot** output is allowed by VERBOSE"
log_plot() = log_verbose(PlotOutput)

"Returns whether both **console** and **plot** outputs are allowed by VERBOSE"
log_all() = log_verbose(ConsoleOutput, PlotOutput)