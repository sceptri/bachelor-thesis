export @overrides, @passup, @assign

TAKE_FROM_VARIABLE = :PARAMETERS

IGNORE_MACROS = ["@later"]

"""
Macro to override an assignment to a variable with a value from `PARAMETERS`, 
if such key-value pair exists in the `Dict`
"""
macro overrides(expression)
    splitted = split(string(expression), "="; limit=2)
    var_name = splitted[1]
    var_expr = splitted[2]

    while var_name[end] == ' '
        var_name = chop(var_name)
    end

    remove_macro_info = r"(#=.*?=#)"
    var_expr = replace(var_expr, remove_macro_info => "")

    while var_expr[1] == ' '
        var_expr = chop(var_expr; head=1, tail=0)
    end

    transform_expr = identity
    if var_expr[1] == '@'
        catch_macro = r"(@.+?)\(.|\n*?\)"
        remove_macro = r"(@.+?)\("

        macro_match = match(catch_macro, var_expr)
        macroname = macro_match.captures[1]

        if !(macroname in IGNORE_MACROS)
            var_expr = replace(var_expr, remove_macro => "")
            var_expr = chop(var_expr)

            transform_expr = val -> Expr(:macrocall, Symbol(macroname), LineNumberNode(1), val)
        end
    end

    if !isdefined(Main, TAKE_FROM_VARIABLE)
        return expression |> Main.eval |> QuoteNode
    end

    last_var = split(var_name, ',')[end]
    if last_var[end] == ')'
        last_var = chop(last_var)
    end
    if last_var[1] == ' '
        last_var = chop(last_var; head=1, tail=0)
    end

    parameters = Main.eval(TAKE_FROM_VARIABLE)

    if parameters isa Dict
        assign_to = Symbol(var_name)
        if occursin(",", var_name)
            var_name = chop(var_name; head=1, tail=1)
            vars = split(var_name, ",")
            vars = [(var[1] == ' ' ? chop(var; head=1, tail=0) : var) for var in vars]

            assign_to = Expr(:tuple, Symbol.(vars)...)
        end

        if haskey(parameters, last_var)
            transform_expr = transform_expr == identity && (
                (parameters[last_var] isa Symbol) || (parameters[last_var] isa Expr)
            ) ? QuoteNode : transform_expr

            return Expr(:(=), assign_to, transform_expr(parameters[last_var])) |> Main.eval |> QuoteNode
        elseif haskey(parameters, Symbol(last_var))
            transform_expr = transform_expr == identity && (
                (parameters[Symbol(last_var)] isa Symbol) || (parameters[Symbol(last_var)] isa Expr)
            ) ? QuoteNode : transform_expr

            return Expr(:(=), assign_to, transform_expr(parameters[Symbol(last_var)])) |> Main.eval |> QuoteNode
        end
    end

    expression |> Main.eval |> QuoteNode
end

macro passup(expression)
    if !isdefined(Main, TAKE_FROM_VARIABLE)
        return expression |> Main.eval |> QuoteNode
    end

    parameters = Main.eval(TAKE_FROM_VARIABLE)

    if !(parameters isa Dict)
        return expression |> Main.eval |> QuoteNode
    end

    return :nothing
end

"""
Macro to run an assignment to a given variable, where the assigned value is it's current value evaluated
> Useful when its current value is `Expr` or `Symbol`
"""
macro assign(symbol)
    Expr(:(=), symbol, Main.eval(symbol)) |> Main.eval |> QuoteNode
end