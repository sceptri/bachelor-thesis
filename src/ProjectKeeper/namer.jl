import Base: isless, push!, deleteat!, getindex, convert, broadcastable, copy

export here
export NamingField, Naming, name
export savetext, push!, deleteat!, convert, broadcastable, copy
export filesave, filesave_text
export not_test_suite
export parse_param_vector, fix_name

pathDelim() = Sys.iswindows() ? '\\' : '/'

"""
	here(filename::AbstractString)

Returns folder of the script, where it is used, and appends a `filename` to it
"""
here(filename::AbstractString) = string(Base.source_dir(), pathDelim(), filename)

get_current_file() = split(Base.source_path(), pathDelim())[end]

Evaluateable = Union{Expr,Symbol}
EvaluateableString = Union{Expr,Symbol,String}

"""
	evaluate(expression::EvaluateableString, cast::Base.Callable=identity)

Evaluates and casts given evaluatable variable 
"""
evaluate(expression::EvaluateableString, cast::Base.Callable=identity) = cast(Core.eval(Main, expression))

# The ordering of naming fields is based upon sorting
@enum SubdirectoryBehavior begin
    basic_subdirectory = 0
    make_subdirectory = 1
end

"""
	mutable struct NamingField

Core type used for defining a deterministic naming.

**Arguments**:
- `expression` - expression that when evaluated gives the value of the NamingField
- `key` - key that can be evaluated to tell different NamingFields apart if not already apparent
- `delimiter` - delimiter between the `key` and the `expression`
- `subdirectory_behavior` - signals whether this `NamingField` should create a directory or be simply used in the filename
- `include_key` - signals, whether the key should be included when parsing the `NamingField` into string
- `condition` - gives the condition, only under which to include this `NamingField` in the final name
- `transform` - a function to transform the `NamingField` parsed to string before handling it over to `Naming` struct
"""
mutable struct NamingField
    expression::Evaluateable
    key::Symbol
    delimiter::AbstractString
    subdirectory_behavior::SubdirectoryBehavior
    include_key::Bool
    condition::Union{Nothing,Expr}
    transform::Base.Callable

    NamingField(
        expression=Symbol(), key=Symbol(), delimiter="=";
        subdirectory_behavior=basic_subdirectory, include_key=false, condition=nothing, transform=identity) =
        new(expression, key, delimiter, subdirectory_behavior, include_key, condition, transform)

    NamingField(expression::Symbol, delimiter::String="="; kwargs...) =
        NamingField(expression, expression, delimiter; kwargs...)

    NamingField(pair::Pair{Symbol,T}, delimiter::String="="; kwargs...) where {T<:Evaluateable} =
        NamingField(last(pair), first(pair), delimiter; kwargs...)
end

# Functions to improve the ease-of-use of a `NamingField` type 
copy(field::NamingField) = NamingField(field.expression, field.key, field.delimiter;
    subdirectory_behavior=field.subdirectory_behavior, include_key=field.include_key)
convert(::Type{NamingField}, value::Symbol) = NamingField(value, value)
convert(::Type{NamingField}, value::Pair{Symbol,<:Evaluateable}) = NamingField(value.second, value.first)
broadcastable(field::NamingField) = Ref(field)

"""
	function name(field::NamingField; check_defined=false)

Converts `NamingField` into a string
- if `check_defined == true`, it only tries to do this for a defined values in `NamingField`
"""
function name(field::NamingField; check_defined=false)
    if check_defined && !is_defined(field)
        return ""
    end

    if !isnothing(field.condition) && ((is_defined(field.condition) && !evaluate(field.condition)) || !is_defined(field.condition))
        return ""
    end

    evaluated = evaluate(field.expression, string)
    evaluated = field.transform(evaluated)
    if field.include_key
        evaluated = string(field.key) * field.delimiter * evaluated
    end

    return evaluated
end

"""
	EvaluateableField

Type that encapsulates anything useable by the Namer
"""
EvaluateableField = Union{NamingField,Symbol,Pair{Symbol,<:Evaluateable}}

"""
	mutable struct Naming

Struct defining naming scheme for a given experiment
- `fields` define Symbols or Expressions of variables to be evaluated at runtime and used in the filename
- `delimiter` between variable values (default `"_"`)
- `filetype` defines filetype *without* a dot (default `"pdf"`)
- `directory` in which any named subject will be placed (default `"figures"`)
- `as_directory` defines whether by default function `name` creates folder of the given name or adds it to the filename (default `true`)
- `filesystem` flags if the naming scheme is supposed to be used with filesystem. Setting it to `false` leads to ignoring `filetype`, `directory` and `as_directory` options (default `true`)
"""
mutable struct Naming
    fields::Vector{NamingField}
    delimiter::AbstractString
    filetype::AbstractString
    directory::Union{AbstractString,Array{String}}
    as_directory::Bool
    filesystem::Bool

    function Naming(fields=NamingField[]; delimiter="_", filetype="pdf", directory="figures", as_directory=true, filesystem=true)
        if !filesystem
            filetype = ""
            directory = ""
            as_directory = false
        end
        return new(fields, delimiter, filetype, directory, as_directory, filesystem)
    end
end

# Functions that allow to work better with `Naming` type
copy(naming::Naming) = Naming(copy(naming.fields); delimiter=naming.delimiter, filetype=naming.filetype,
    directory=naming.directory, as_directory=naming.as_directory, filesystem=naming.filesystem)
broadcastable(naming::Naming) = Ref(naming)
getindex(naming::Naming, index::Symbol) = filter(field -> field.key == index, naming.fields) |> (vec -> length(vec) == 1 ? vec[1] : vec)

# Functions that define necessary rules for sorting the naming fields
isless(a::String, b::Symbol) = false
isless(a::Expr, b::Symbol) = false
isless(a::Expr, b::Expr) = false
isless(a::NamingField, b::NamingField) = isless(a.key, b.key)

function push!(naming::Naming, field::EvaluateableField)
    field = convert(NamingField, field)

    index = findfirst(element -> element.key == field.key, naming.fields)
    if isnothing(index)
        push!(naming.fields, field)
    else
        naming.fields[index] = field
    end
    naming.fields
end

# Functions to use `Naming` type as if it was an array, in some contexts
push!(naming::Naming, fields::EvaluateableField...) = push!.(naming, fields)

deleteat!(naming::Naming, key::Symbol) = filter!(field -> field.key != key, naming.fields)
deleteat!(naming::Naming, keys::Vector{Symbol}) = deleteat!.(naming, keys)

"""
	function is_defined(value::Evaluateable)

Tells whether all symbols and expressions in the `Evaluateable` are defined
"""
function is_defined(value::Evaluateable)
    if isa(value, Symbol)
        return isdefined(Main, value) && evaluate(value, string) != "" && evaluate(value) != 0
    end

    all_defined = true
    for argument in value.args
        if isa(argument, Symbol)
            all_defined &= isdefined(Main, argument) && evaluate(argument, string) != "" && evaluate(argument) != 0
        end
    end

    return all_defined && evaluate(value, string) != "" && evaluate(value) != 0
end
is_defined(::Nothing) = true
is_defined(::String) = true
is_defined(field::NamingField) = is_defined(field.expression) && is_defined(field.condition)

"""
Filters supplied fields and returns only those, that are defined
"""
only_defined(fields::Vector{NamingField}) = filter(field -> is_defined(field), fields)
only_defined(fields::Vector{<:EvaluateableString}) = filter(field -> is_defined(field), fields)

"""
Converts vector of folders to a path (using the correct path delimiter for the platform)
"""
to_path(string::AbstractString) = string
to_path(folders::Array{String}; with::Union{String,Char}=pathDelim()) = join(folders, with)

# Returns filename based on a `Naming` struct. Key word arguments:
# - `without` declares which fields from `naming` should not be used (by keys)
# - `including` adds vector of `Symbol` or `Expression`, which should be added to the name
# - `filetype` overrides `naming.filetype` and signals the filetype (and mimetype) of the generated file
# - `as_directory` overrides `naming.as_directory` and signals, whether Namer should create a separate folder
# - `directory` overrides `naming.directory`
# - `filesystem` overrides `naming.filesystem` and signals, whether anything will be saved to filesystem by Namer
function (naming::Naming)(filename::AbstractString;
    without::Vector{Symbol}=Symbol[],
    only::Union{Nothing,Vector{Symbol}}=nothing,
    including::Vector{<:EvaluateableString}=Symbol[],
    filetype::Union{Nothing,AbstractString}=nothing,
    as_directory::Union{Nothing,Bool}=nothing,
    directory::Union{Array{String},String,Nothing}=nothing,
    filesystem::Union{Nothing,Bool}=nothing
)
    # Sort naming fields and includings to get the same result everytime
    sorted_fields = sort(filter(field -> !(field.key in without) && (isnothing(only) || field.key in only), naming.fields))
    sorted_including = sort(including)

    filesystem = isnothing(filesystem) ? naming.filesystem : filesystem
    # Correctly determine filenames and folders
    if filesystem
        filetype = '.' * (isnothing(filetype) ? naming.filetype : filetype)
        to_directory = !isempty(naming.directory) ? to_path(naming.directory) * pathDelim() : ""
        # Override naming.directory by local settings 
        to_directory = isnothing(directory) ? to_directory : (to_path(directory) * pathDelim())
        locate = here
    else
        filetype = ""
        to_directory = ""
        locate = identity
    end

    naming_array = []

    for field in only_defined(sorted_fields)
        if field.subdirectory_behavior == make_subdirectory && filesystem
            to_directory *= name(field) * pathDelim()
        elseif name(field) != ""
            push!(naming_array, name(field))
        end
    end

    including_fields = (evaluate(expression, string) for expression in only_defined(sorted_including))
    push!(naming_array, including_fields...)

    include_filename = ""

    as_directory = isnothing(as_directory) ? naming.as_directory : as_directory
    if as_directory && filesystem
        to_directory *= filename * pathDelim()
    else
        include_filename = filename * (isempty(naming_array) ?
                                       "" : naming.delimiter * include_filename)
    end

    # Be sure to create the path ONLY if filesystem is considered
    filesystem && mkpath(here(to_directory))

    param_naming = isempty(naming_array) || naming_array == [""] ? filename : join(naming_array, naming.delimiter)

    return locate(to_directory * include_filename * param_naming * filetype)
end

"""
	function savetext(print_command, filename; mode="w+")

Saves given text, by `print_command`, into a given file, by `filename`
> This file is opened with given `mode` 
"""
function savetext(print_command, filename; mode="w+")
    open(filename, mode) do io
        println(io, print_command)
    end
end

"""
	function not_test_suite()

Tests, whether a test suite is running
"""
function not_test_suite()
    if isdefined(Main, :TEST_DO_SAVES)
        return Main.TEST_DO_SAVES
    end

    return true
end

"""
Maximum allowed length of a **path** for Windows, if not tweaked by the user
"""
MAX_WINDOWS_FILENAME_LENGTH = 258

"""
	function fix_name(name)

function that fixes a supplied filename (or path) for Windows
- it replaces ':' with '--' (except for a colon used with drive)
- it shortens the path to `MAX_WINDOWS_FILENAME_LENGTH`
"""
function fix_name(name)
    if !Sys.iswindows()
        return name
    end

    drive, file = name[1:2], name[3:end]
    file = replace(file, ":" => "--")
    name = string(drive, file)

    if length(name) < MAX_WINDOWS_FILENAME_LENGTH
        return name
    end

    filetype_index = findlast(".", name)
    @show filetype_index
    filename = name[1:(last(filetype_index)-1)]
    filetype = name[last(filetype_index):end]

    cont = "_CONT"

    return string(filename[1:(MAX_WINDOWS_FILENAME_LENGTH-length(cont)-length(filetype))], cont, filetype)
end

"""
	function filesave_text(print_command, filename; kwargs...)

Wrapper around `savetext` function to handle not saving any files when running a test suite
"""
function filesave_text(print_command, filename; kwargs...)
    if not_test_suite()
        return savetext(print_command, fix_name(filename); kwargs...)
    end
end

import Makie: save

"""
	function filesave(name, object; kwargs...)

Wrapper around `Makie.save` function to handle not saving any files when running a test suite
"""
function filesave(name, object; kwargs...)
    if not_test_suite()
        return save(fix_name(name), object; kwargs...)
    end
end

"""
	function parse_param_vector(params::AbstractVector; equal="=")

Function that parses a vector of parameters and casts them to string in such a way,
that is useable with Windows filesystem limitations and our naming scheme
"""
function parse_param_vector(params::AbstractVector; equal="=")
    return string("(", join([
                string(first(param), equal, last(param)) for param in params
            ], ','), ")")
end