using Printf, LaTeXStrings
using Statistics, DataFrames, CSV
export values, symbol_at, show, to_string, sprintf, is_ones, remove_types
export calculate_ribbon, as_tex
export extract_data, ode_params
export do_unless, sprintf
export to_tex_matrix, to_tex_vector

import Base: values

function values(array::Vector{Pair{T,TT}}) where {T<:Any,TT<:Any}
    values = Vector{TT}()
    for (_, value) in array
        push!(values, value)
    end

    return values
end

symbol_at(variable::Symbol, index::Integer; prefix=Symbol()) = Symbol(String(prefix) * String(variable), "__", index)

import DataDrivenDiffEq: DataDrivenSolution, Basis

import Base.show

function show(io::IO, whole_solution::Tuple{DataDrivenSolution,Basis,Vector})
    solution, system, parameters = whole_solution

    println(io, system)
    println(io, "")
    println(io, solution)
    println(io, "")
    println(io, "Parameters:")
    println(io, parameters)
end

ode_params(parameter_map::Vector{Pair{Symbol, T}}) where T <: Number = [Main.eval(first(pair)) => last(pair) for pair in parameter_map]

sprintf(format, values...) = Printf.format(Printf.Format(format), values...)

function to_string(strings::AbstractVector{String}; format_to="%s", join_by=',', do_join=true)
    formatted = [("\$" * sprintf(format_to, one_string) * "\$") for one_string in strings]
    return do_join ? (join(formatted, join_by)) : formatted
end

to_string(symbols::AbstractVector{Symbol}; kwargs...) = to_string([String(symbol) for symbol in symbols]; kwargs...)
to_string(symbol::Symbol; kwargs...) = to_string([symbol]; kwargs...)
to_string(one_string::String; kwargs...) = to_string([one_string]; kwargs...)
to_string(tuple::Tuple; kwargs...) = to_string(collect(tuple); kwargs...)
to_string(string::AbstractString; more...) = string
to_string(string::LaTeXString; more...) = string

remove_types(object, more...) = replace(string(object), r"\{.+?\}" => "", more...)
to_string(object, more...) = remove_types(object, more...)

is_ones(a::Number) = a == 1
is_ones(a::AbstractVector) = all(i -> i == 1, a)

function as_tex(basis::Basis;
    delimiter=",\\\\",
    remove_parantheses=true,
    add_cdot=true,
    digits=2,
    lhs="\\frac{\\mathrm{d}}{\\mathrm{d}\\,t}\\hat{%s}",
    format_var="\\hat{%s}",
    aligned=true,
    line_length=150,
    aligned_break="\\\\ \n\t&",
    aligned_equals="=&\\hphantom{+}"
)
    params = get_parameter_map(basis)
    params = map((pair -> (first(pair) => round(last(pair); digits))), params)
    vars = states(basis)

    operators = ['+', '-']

    tex_system = ""

    for (eq_index, eq) in enumerate(equations(basis))
        eq_subtituted = substitute(eq.rhs, params)
        eq_string = string(eq_subtituted)


        if remove_parantheses
            eq_string = replace(eq_string, '(' => "", ')' => "")
        end

        if add_cdot
            eq_string = replace(eq_string, '*' => "")
            format = " \\cdot $format_var "
        end
        for var in string.(vars)
            eq_string = replace(eq_string, var => sprintf(format, string(var)))
        end

        split_lines = false
        lines = ceil(Int64, length(eq_string) / line_length)
        while lines > 1
            split_lines = true
            char_index = (lines - 1) * line_length
            while !(eq_string[char_index] in operators) && char_index > 0
                char_index -= 1
            end

            if char_index == 0
                break
            end

            eq_string = eq_string[1:(char_index-1)] * aligned_break * eq_string[char_index:end]
            lines -= 1
            char_index = (lines - 1) * line_length
        end

        var = string(vars[eq_index])
        lhs_var = sprintf(lhs, string(var))

        equates = (split_lines || aligned) ? aligned_equals : "="
        tex_system *= "$lhs_var $equates $eq_string"

        if (eq_index != length(vars))
            tex_system *= delimiter * "\n"
        end
    end

    return tex_system
end

function calculate_ribbon(data::Array{T}) where {T<:Number}
    @assert length(size(data)) == 3
    ribbon = Array{T}(undef, (size(data)[1], size(data)[2], 3))
    for (row_index, row) in enumerate(eachslice(data, dims=1))
        for (σ_index, values_for_σ) in enumerate(eachslice(row, dims=1))
            ribbon[row_index, σ_index, 1] = mean(values_for_σ)
            ribbon[row_index, σ_index, 2] = minimum(values_for_σ)
            ribbon[row_index, σ_index, 3] = maximum(values_for_σ)
        end
    end

	return ribbon
end

function extract_data(filepath::AbstractString, data_variables::Tuple; kwargs...)
    dataframe = DataFrame(CSV.File(filepath))
    return extract_data(dataframe, data_variables; kwargs...)
end

function extract_data(dataframe::DataFrame, data_variables::Tuple; at=1, derivatives = true)
    variables_names = Symbol[]
    derivatives_names = Symbol[]
    for current_variable in data_variables
        push!(variables_names, symbol_at(current_variable, at))
        push!(derivatives_names, symbol_at(current_variable, at; prefix=:d))
    end

    dataframe_trajectory = dataframe[:, variables_names]
    dataframe_derivative = derivatives ? dataframe[:, derivatives_names] : nothing
    time = dataframe[:, symbol_at(:time, at)]

	trajectory = Matrix(dataframe_trajectory)'
	derivative = derivatives ? Matrix(dataframe_derivative)' : nothing

    return time, trajectory, derivative
end

function do_unless(key::String)
	return !(key in ARGS)
end

function to_tex_matrix(mtr::AbstractMatrix;
	env="pmatrix",
	decimals = 2,
	digits = 2
)
	stringify = val -> val == 0 ? "0" : sprintf("%$(digits + decimals).$(decimals)f", val)
	text = "\\begin{$env}"
	for (index, row) in enumerate(eachrow(mtr))
		if index != 1
			text *= " \\\\"
		end

		row_string = stringify.(row)
		text *= "\n\t" * join(row_string, " & ")
	end
	text *= "\n\\end{$env}"

	return text
end

function to_tex_vector(vec::AbstractVector;
    use_brackets=true,
	borders = ('(', ')'),
    decimals=2,
    digits=2
)
    stringify = val -> val == 0 ? "0" : sprintf("%$(digits + decimals).$(decimals)f", val)

    text = use_brackets ? "\\brackets{" : "\\left$(borders[1])"
    text *= join(stringify.(vec), ", ")
    text *= use_brackets ? "}" : "\\right$(borders[2])"

    return text
end