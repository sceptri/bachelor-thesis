export @varname, @varname_and_value, @varname_and_symbol, @later

"""
Returns string of the supplied expression
"""
macro varname(arg)
    string(arg)
end

"""
Returns given expression in an `Expr`, 
to be evaluated later using `eval` or assigned with `@assign`
"""
macro later(arg)
    Expr(:quote, arg)
end

"""
Return a tuple of expression cast into a string and the expression value itself
"""
macro varname_and_value(expression)
    quote
        $(string(expression)), identity(begin
            local value = $(esc(expression))
        end)
    end
end

"""
Return a tuple of expression cast into a string and the expression in the form of `Expr`, 
to be evaluated later using `eval` or assigned with `@assign`
"""
macro varname_and_symbol(expression)
    quote
        $(string(expression)), $(Expr(:quote, expression))
    end
end