using TotalVariation, NoiseRobustDifferentiation
import NoiseRobustDifferentiation: tvdiff

export denoise, tvdiff

# Variants with 1D parameter input |---------------------------------------
"""
	function denoise(input_data, λ; kwargs...)

Denoises data using **total variation** method
"""
function denoise(input_data, λ::Number; kwargs...)
    denoised = [tv(collect(row), Float64(λ); kwargs...) for row in eachrow(input_data)]
    return mapreduce(permutedims, vcat, denoised)
end

"""
	function denoise(input_data, k, λ; kwargs...)

Denoises data using **group sparse total** variation method
"""
function denoise(input_data, k::Integer, λ::Number; kwargs...)
    denoised = [gstv(collect(row), Int(k), Float64(λ); kwargs...) for row in eachrow(input_data)]
    return mapreduce(permutedims, vcat, denoised)
end

# Variants with parameter input of data dimension |-----------------------
"""
	function denoise(input_data, λ; kwargs...)

Denoises data using **total variation** method
"""
function denoise(input_data, λ::Vector{<:Number}; kwargs...)
    if length(λ) != size(input_data, 1)
        throw(ErrorException("Wrong length of λs"))
    end

    denoised = [tv(collect(row), Float64(λ[index]); kwargs...) for (index, row) in enumerate(eachrow(input_data))]
    return mapreduce(permutedims, vcat, denoised)
end

"""
	function denoise(input_data, k, λ; kwargs...)

Denoises data using **group sparse total** variation method
"""
function denoise(input_data, k::Vector{<:Integer}, λ::Vector{<:Number}; kwargs...)
    if length(λ) != size(input_data, 1) || length(k) != size(input_data, 1)
        throw(ErrorException("Wrong length of λs or ks"))
    end

    denoised = [gstv(collect(row), Int(k[index]), Float64(λ[index]); kwargs...) for (index, row) in enumerate(eachrow(input_data))]
    return mapreduce(permutedims, vcat, denoised)
end

function tvdiff(data::AbstractArray, iter::Integer, α::Real; kwargs...)
    diffs = [tvdiff(row, iter, α; kwargs...) for row in eachrow(data)]
    return mapreduce(permutedims, vcat, diffs)
end

function tvdiff(data::AbstractArray, iter::Vector{<:Integer}, α::Vector{<:Real}; kwargs...)
    if length(iter) != size(data, 1) || length(α) != size(data, 1)
        throw(ErrorException("Wrong length of vector of iterations or αs"))
    end

    diffs = [tvdiff(row, iter[index], α[index]; kwargs...) for (index, row) in enumerate(eachrow(data))]
    return mapreduce(permutedims, vcat, diffs)
end