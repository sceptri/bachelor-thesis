Base.@kwdef struct MultiplicativeNoise <: AbstractNoise
    distribution::Distribution = Normal(1, 0.2)

    MultiplicativeNoise(μ, σ) = new(Normal(μ, σ))
    MultiplicativeNoise(distribution::Distribution) = new(distribution)
end
(noise::MultiplicativeNoise)(data::AbstractArray) = data .* rand(noise.distribution, size(data))

#>--OPTIONAL
import Base.show
Base.show(io::IO, noise::MultiplicativeNoise) = print(io, "MultiplicativeNoise($(noise.distribution.μ), $(noise.distribution.σ))")
#>--END