export AdditiveNoise, MultiplicativeNoise
export AdditiveVarianceNoise, CompositeNoise

using Random, Distributions, Statistics

abstract type AbstractNoise end

# Split into different files, because we include those parts in the document
include("Noises/additive.jl")
include("Noises/multiplicative.jl")
include("Noises/additiveVariance.jl")


"""
Order of noises **matters**!
"""
Base.@kwdef struct CompositeNoise <: AbstractNoise
    noises::Vector{<:AbstractNoise} = []

    CompositeNoise(noises...) = new([noises...])
    CompositeNoise(noises::Vector{<:AbstractNoise}) = new(noises)
end
function (noise::CompositeNoise)(data::AbstractArray)
    for partial_noise in noise.noises
        data = partial_noise(data)
    end
    return data
end
