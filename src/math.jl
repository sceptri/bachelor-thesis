export fft_derivative, forward_diff, tvdiff, L₂


# TODO: Look at using Laplace transform, so the function does not have to be tending to 0 at both infinities
"""
**Deprecated**

	function fft_derivative(trajectory::AbstractArray, L::Real, n::Real)

Computes derivative using fast fourier transform
"""
function fft_derivative(trajectory::AbstractArray, L::Real, n::Real)
    f̂ = fft(trajectory)

    κ = (2 * pi / L) * (-n/2:n/2-1)
    κ = fftshift(κ)

    df̂ = im * κ .* f̂

    return ifft(df̂) |> real
end

"""
	function forward_diff(trajectory::AbstractArray{T}, step::Real) where {T<:Real}

Simple forward differentiation function, trajectory is assumed to be column vector of trajectories in each variable
"""
function forward_diff(trajectory::AbstractArray{T}, step::Real) where {T<:Real}
    (variables_count, trajectory_length) = size(trajectory)
    forward_diff = Array{T}(undef, variables_count, trajectory_length)

    for κ in 1:(trajectory_length-1)
        forward_diff[:, κ] = (trajectory[:, κ+1] .- trajectory[:, κ]) ./ step
    end

    forward_diff[:, end] = forward_diff[:, end-1]

    return forward_diff
end

L₂(truth::AbstractArray, compare::AbstractArray) = sqrt.(sum((compare .- truth) .^ 2; dims=2))