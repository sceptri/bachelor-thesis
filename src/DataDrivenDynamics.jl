module DataDrivenDynamics

# ProjectKeeper submodule
include("ProjectKeeper/ProjectKeeper.jl")
import .ProjectKeeper

# SINDy submodule
include("SINDy/SINDy.jl")
import .SINDy

include("Noiser/Noiser.jl")
import .Noiser

# Data generator utilities
include("generator.jl")
import .Generator

# General utility stuff
include("macros.jl")
include("utils.jl")

# Makie recipes for plotting
include("recipes.jl")

# Useful mathematics
include("math.jl")

include("Sparse/Sparse.jl")
import .Sparse

end # module DataDrivenDynSystems
