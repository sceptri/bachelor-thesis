import DataDrivenSparse: init_cache, step!, get_thresholds
using LinearAlgebra, Statistics

struct ARDLS{T<:Real,TT<:Union{Number,AbstractVector}} <: DataDrivenSparse.AbstractSparseRegressionAlgorithm
	a::T
	b::T
	"""Consider-zero threshold"""
    thresholds::TT


	function ARDLS(threshold::TT=1e-1, a::T = 1e-10, b::T = a) where {T<:Real, TT}
        @assert all(threshold .> zero(eltype(threshold))) "Threshold must be positive definite"
		return new{T,TT}(a,b,threshold)
	end
end

Base.summary(::ARDLS) = "ARDLS"
Base.show(io::IO, opt::ARDLS) = print(io, "ARDLS(a=$(opt.a),b=$(opt.b),$(opt.thresholds))")

struct ARDLSCache{C<:AbstractArray,A<:BitArray,AT,AST,BT,ST,CT,FT,NT,SXT,aT,bT,ATT,BTT} <: 
		DataDrivenSparse.AbstractSparseRegressionCache
	X::C
	X_prev::C
	active_set::A
	# Scaled input data
	A::AT
	Aₛ::AST
	B::BT
	# Only cached data
	scale::ST
	covariance::CT
	factor::FT
	noise_variance::NT
	Sx::SXT
	# Algorithm data
	a::aT
	b::bT
	# Original Data
    Ã::ATT
    B̃::BTT
end

function init_cache(alg::ARDLS, A::AbstractMatrix, b::AbstractVector)
	init_cache(alg, A, permutedims(b))
end

function init_cache(alg::ARDLS, A::AbstractMatrix, B::AbstractMatrix)
    @assert size(B, 1) == 1 "Caches only hold single targets!"

	Ã, B̃ = A,B

	Mₓ = maximum(abs.(A))
    A = A ./ Mₓ
    B = B ./ Mₓ

    scale = sqrt.(diag(A * A'))
    Aₛ = A' * diagm(1 ./ scale)

    covariance = Aₛ' * Aₛ
    factor = Aₛ' * B'

    X = permutedims((covariance + maximum(covariance) / 100 * I) \ factor)

    residue = B' - Aₛ * X'
    noise_variance = 1 / mean(residue .^ 2)
    Sx = diagm(abs.(view(X, :)) .* 0.1 .+ 0.01 .* mean(X))

	X ./= scale'
    X_prev = zeros(size(X))
    active_set = BitArray(undef, size(X))

    λ = minimum(DataDrivenSparse.get_thresholds(alg))
    active_set .= abs.(X) .> sqrt(2 * λ)
    X[[!i for i in active_set]] .= 0

	return ARDLSCache{typeof(X), typeof(active_set), typeof(A), typeof(Aₛ), typeof(B), typeof(scale), 
		typeof(covariance), typeof(factor), typeof(noise_variance), typeof(Sx), typeof(alg.a), typeof(alg.b), typeof(Ã), 
		typeof(B̃)}(X, X_prev, active_set, A, Aₛ, B, scale, covariance, factor, noise_variance, Sx, alg.a, alg.b, Ã, B̃)
end

function step!(cache::ARDLSCache, λ)
    @unpack X, X_prev, A, Aₛ, B, scale, covariance, factor, noise_variance, Sx, a, b, active_set = cache
	
    correction = diagm((a + 0.5) ./ (b .+ 0.5 * (view(X, :) .* scale .^ 2 .+ diag(Sx))))
    Sx = inv(noise_variance * covariance + correction)

	X_prev = copy(X)
    X .= noise_variance * factor' * Sx'

    residue = B' - Aₛ * X'
    noise_variance = size(Aₛ, 1) / (dot(residue, residue) + tr(Sx * covariance))

    X ./= scale'
	
    active_set .= abs.(X) .> sqrt(2 * λ)
    X[[!i for i in active_set]] .= 0
end