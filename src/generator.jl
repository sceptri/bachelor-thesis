module Generator

export SaveData, feedSaveData, prepareSaveData

import DifferentialEquations: ODESolution, EnsembleSolution

"""
	struct SaveData
		variables::Tuple
		times::Vector{Vector{Number}}
		trajectories::Vector{Matrix{Number}}
		derivatives::Vector{Matrix{Number}}
	end

Struct to hold all the data for saving.

**Contents**:
- `variables` holds names (as `Symbol`) to all variables present in data and in the **correct order**
- `times` is a vector of timestamps for each run
- `trajectories` is a vector of matrices representing the trajectory of each run
- `derivatives` is a vecotr of matrices representing the derivatives of each run

To get data in a format useable for saving, call the function `prepareSaveData` 
"""
struct SaveData
    variables::Tuple
    times::Vector{Vector{Number}}
    trajectories::Vector{Matrix{Number}}
    derivatives::Vector{Matrix{Number}}
end

"""
	function feedSaveData(
		solution::EnsembleSolution, 
		variables::Tuple, 
		derivative_function::Base.Callable=identity
	)::SaveData

creates SaveData object straight from the EnsembleSolution
"""
function feedSaveData(solution::EnsembleSolution, variables::Tuple, derivative_function::Base.Callable=identity)::SaveData
    data = SaveData(variables, [], [], [])

    # Transforming trajectories from an EnsembleProblem to an array of matrices representing the trajectories
    for singular_solution in solution
        trajectory = mapreduce(permutedims, vcat, singular_solution.u)
        push!(data.trajectories, trajectory)
        push!(data.times, singular_solution.t)
        derivatives = [derivative_function(coords, singular_solution.t[t_index]) for (t_index, coords) in enumerate(eachrow(trajectory))]
        push!(data.derivatives, mapreduce(permutedims, vcat, derivatives))
    end

    return data
end

"""
	function feedSaveData(
		solution::EnsembleSolution, 
		variables::Tuple, 
		derivative_function::Base.Callable=identity
	)::SaveData

creates SaveData object straight from the ODESolution
"""
function feedSaveData(solution::ODESolution, variables::Tuple, derivative_function::Base.Callable=identity)::SaveData
    data = SaveData(variables, [], [], [])

    trajectory = mapreduce(permutedims, vcat, solution.u)
    push!(data.trajectories, trajectory)
    push!(data.times, solution.t)

    derivatives = derivative_function.([row for row in eachrow(trajectory)], solution.t)
    derivatives_trajectory = mapreduce(permutedims, vcat, derivatives)
    push!(data.derivatives, derivatives_trajectory)

    return data
end

function feedSaveData(solution::AbstractMatrix, solution_times::AbstractArray, variables::Tuple)::SaveData
    data = SaveData(variables, [], [], [])

    push!(data.trajectories, solution)
    push!(data.times, solution_times)

    return data
end

"""
	function prepareSaveData(data::SaveData, with_derivatives=false)::Dict{Symbol,Any}

Prepares time and trajectory data to be easy to save and write to a CSV file
"""
function prepareSaveData(data::SaveData, with_derivatives=false)::Dict{Symbol,Any}
    if (length(data.times) != length(data.trajectories))
        throw(ArgumentError("Times & Trajectories fields correspond to a different number of runs!"))
    end

    run_count = length(data.times)

    data_to_save = Dict{Symbol,Any}()

    for index in 1:run_count
        suffix = "__" * string(index)
        for (variable_index, variable) in enumerate(data.variables)
            variable_name = Symbol(string(variable) * suffix)
            data_to_save[variable_name] = data.trajectories[index][:, variable_index]
        end

        if with_derivatives
            for (variable_index, variable) in enumerate(data.variables)
                variable_name = Symbol("d" * string(variable) * suffix)
                data_to_save[variable_name] = data.derivatives[index][:, variable_index]
            end
        end

        time_name = Symbol("time" * suffix)
        data_to_save[time_name] = data.times[index]
    end

    data_to_save
end

end # module Generator