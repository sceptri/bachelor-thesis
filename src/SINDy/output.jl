using Printf

export prettyprint

"""
	function prettyprint_singular(coefficient, power_vector, variables_names)

Pretty prints a single term of the result, given a vector of its powers

**Arguments**:
- `coefficient` - coefficient for the printed term
- `power_vector` - vector of powers (of variables) present in the printed term
- `variables_names` - names of variables
"""
function prettyprint_singular(coefficient, power_vector, variables_names)
    coef = @sprintf "%.2f" coefficient
    if all(power_vector .== 0)
        return coef
    end

    terms = ["$(variables_names[index])" * (power > 1 ? "^$(power)" : "")
             for (index, power) in enumerate(power_vector)
             if power > 0
    ]

    return join([coef, terms...], "*")
end

"""
	function prettyprint(Ξ, library::PredefinedLibrary; 
		vars=["x", "y", "z", "w"], lhs_vars = nothing,
		io = stdout, abstol = 1e-5
	)

Pretty prints the result of the SINDy method given a used library

**Arguments**:
- `Ξ` - coefficient matrix solution discovered by SINDy method
- `library` - library used with SINDy

**Keyword Arguments**:
- `vars` - vector of variables names used for pretty printing
- `io` - we can wish to write it to a different IO than stdout
- `abstol` - absolute tolerance for coefficients (smaller will not be printed)
- `lhs_vars` - left hand side variables' names, by default the `vars` are used
"""
function prettyprint(Ξ, library::PredefinedLibrary;
    vars=["x", "y", "z", "w"], lhs_vars=nothing,
    io=stdout, abstol=1e-5
)
    used_variables = variables(Ξ)
    if length(vars) < used_variables
        error("Not enough variables' names supplied")
    end

    if isnothing(lhs_vars)
        lhs_vars = vars .* Char(0x0307)
    end

    println(io, "Found equations for the system are:")
    for var in 1:used_variables
        indices = findall(Ξ[:, var] .!= 0)
        right_side_terms = [
            prettyprint_singular(Ξ[index, var], stateful(library)[index], vars)
            for index in indices
            if abs(Ξ[index, var]) > abstol
        ]

        right_side_terms = isempty(right_side_terms) ? ["0"] : right_side_terms
        right_side = join(right_side_terms, " + ")
        left_side = lhs_vars[var]

        println(io, "\t$(left_side) = $(right_side)")
    end
end