module SINDy

"""
	variables(X; iterate_by=eachrow)

Helper function to return what will typically represent 
a number of variables in data
"""
variables(X; iterate_by=eachrow) = length(iterate_by(X'))

"""
	states(X; iterate_by=eachrow)

Helper function to return what will typically represent 
a number of states (measurements) in data
"""
states(X; iterate_by=eachrow) = length(iterate_by(X))

include("Optimizers/Optimizers.jl")
using .Optimizers
export LASSO, STLS, DSTLS, ARDLS

include("Libraries/Libraries.jl")
using .Libraries
export MonomialLibrary, PolynomialLibrary, CustomLibrary

export discover
include("definition.jl")

# Output utilities
include("output.jl")

end # module SINDy