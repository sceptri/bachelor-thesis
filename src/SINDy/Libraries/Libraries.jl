module Libraries

export MonomialLibrary, PolynomialLibrary, CustomLibrary, Library, PredefinedLibrary
export stateful

abstract type AbstractLibrary end
abstract type Library <: AbstractLibrary end
abstract type PredefinedLibrary <: Library end

# Implement the support of `library[index]`
Base.getindex(library::AbstractLibrary, index::Integer) = first(Iterators.drop(library, index - 1))

# At the very least for pretty printing, will need to get the "power vectors" of the candidates
# In general, StatefulLibrary returns an iterator of states of the given library
struct StatefulLibrary{T <: PredefinedLibrary} <: AbstractLibrary
	library::T
end
stateful(library::PredefinedLibrary) = StatefulLibrary(library)
Base.length(state::StatefulLibrary; kwargs...) = Base.length(state.library; kwargs...)
Base.iterate(state::StatefulLibrary; kwargs...) = Base.iterate(state.library; stateful=true, kwargs...)
Base.iterate(state::StatefulLibrary, iteration_state; kwargs...) = Base.iterate(state.library, iteration_state; kwargs...)


# Libraries are split into multiple files to 
# ease pasting into the document of the thesis
include("definition.jl")
include("monomial.jl")
include("polynomial.jl")
include("custom.jl")

end