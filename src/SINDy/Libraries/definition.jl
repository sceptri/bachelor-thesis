function (library::Library)(X; iterate_by=eachrow, kwargs...)
    # `iterate_by(X)` is an iterator that returns temporal states of the system,
    # therefore `iterate_by(X')` returns all temporal data of each variable per iteration 
    Θ = reshape.([func(X) for func in library], length(iterate_by(X)), 1)
    return hcat(Θ...)
end

eachstate(func, X; iterate_by=eachrow) = [func(iteration) for iteration in iterate_by(X)]