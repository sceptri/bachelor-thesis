# Struct, that defines a custom library
# aka a library, that purely consists of user-supplied functions
# Disclaimer: Pretty printing is NOT implemented for this library
struct CustomLibrary{T<:AbstractArray} <: Library
    functions::T
end

Base.length(library::CustomLibrary) = length(library.functions)
Base.iterate(library::CustomLibrary; kwargs...) = Base.iterate(library, 0; kwargs...)

function Base.iterate(library::CustomLibrary, index; kwargs...)
    index += 1
    if index > length(library.functions)
        return nothing
    end

    func = X -> eachstate(system_state -> library.functions[index](system_state), X; kwargs...)
    return (func, index)
end