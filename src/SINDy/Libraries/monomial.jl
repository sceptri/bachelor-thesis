# Struct, that generates a monomial library
# aka a library consisting purely of monomial terms
struct MonomialLibrary{T<:Integer} <: PredefinedLibrary
    degree::T
    variables::T
end

Base.length(library::MonomialLibrary) = library.degree * library.variables + 1

function Base.iterate(library::MonomialLibrary; stateful=false, kwargs...)
    state = zeros(library.variables)
    func = X -> eachstate(system_state -> prod(system_state .^ state), X; kwargs...)
    return stateful ? (state, (0, library.variables, stateful)) : (func, (0, library.variables, stateful))
end

# Stateful decides whether a function or a state will be returned
function Base.iterate(library::MonomialLibrary, state; kwargs...)
    power, var, stateful = state
    power += 1
    if power > library.degree
        var -= 1
        power = 1
    end
    if var == 0
        return nothing
    end

    func = X -> eachstate(system_state -> system_state[var]^power, X; kwargs...)
    if !stateful
        return (func, (power, var, stateful))
    end

    power_vector = zeros(library.variables)
    power_vector[var] = power
    return (power_vector, (power, var, stateful))
end
