# Struct, that generates a polynomial library
# aka a library consisting purely of polynomial terms
# of given number of variables and up to a chosen degree
struct PolynomialLibrary{T<:Integer} <: PredefinedLibrary
    degree::T
    variables::T
end

is_malformed(state::Vector, library::PolynomialLibrary) = sum(state) > library.degree || any(state .> library.degree)

Base.length(library::PolynomialLibrary) = sum(binomial(k + library.variables - 1, library.variables - 1) for k in 0:(library.degree))
function Base.iterate(library::PolynomialLibrary; stateful=false, kwargs...)
    state = zeros(library.variables)
    func = X -> eachstate(system_state -> prod(system_state .^ state), X; kwargs...)
    return stateful ? (state, (state, stateful)) : (func, (state, stateful))
end

# Stateful decides whether a function or a state will be returned
function Base.iterate(library::PolynomialLibrary, state; kwargs...)
    state, stateful = state
    index = length(state)
    state[index] += 1
    while is_malformed(state, library)
        state[index] = 0
        index -= 1
        if index == 0
            return nothing
        end
        state[index] += 1
    end

    func = X -> eachstate(system_state -> prod(system_state .^ state), X; kwargs...)
    return stateful ? (state, (state, stateful)) : (func, (state, stateful))
end