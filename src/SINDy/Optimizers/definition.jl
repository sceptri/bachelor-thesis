abstract type Optimizer end

"""
Optimizer is a functor, which when called, tries to regress the library
of candidate terms onto the derivative

Arguments:
- Θ is our data transformed by our selected library of candidate functions
- Ẋ is the derivative in our data
"""
(::Optimizer)(θ, Ẋ; kwargs...) = nothing