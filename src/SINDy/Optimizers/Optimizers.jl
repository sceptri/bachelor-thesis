module Optimizers

using LinearAlgebra, Statistics

import ..SINDy: variables, states

# Optimizers are split into multiple files to 
# ease pasting into the document of the thesis
include("definition.jl")
include("STLS.jl")
include("DSTLS.jl")
include("LASSO.jl")
include("ARDLS.jl")

export Optimizer, STLS, DSTLS, LASSO, ARDLS

end # module Optimizers