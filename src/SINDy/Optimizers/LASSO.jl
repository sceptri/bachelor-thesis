"""
	Base.@kwdef struct LASSO <: Optimizer
		μ = 5
		ρ = 10
	end

Defintion of LASSO optimizer with all the necessary parameters
"""
Base.@kwdef struct LASSO <: Optimizer
    μ = 5
    ρ = 10
end

"""
Soft thresholding operator
"""
S(z, η) = max(z - η, 0) - max(-z - η, 0)

# Solving LASSO problem using ADMM algorithm
function (optimizer::LASSO)(Θ, Ẋ; max_iter=100, kwargs...)
    λ, Q = eigen(Θ' * Θ)
    Q_inv = Matrix(Q') # Q is orthonormal
    ρ, μ = optimizer.ρ, optimizer.μ

    Ξ = zeros(variables(Θ), variables(Ẋ))

    for variable in 1:variables(Ẋ)
        # Initialize all variables as zero vectors
        ξ, u, z = [zeros(variables(Θ)) for _ in 1:3]

        for _ in 1:max_iter
            ξ = Q * ((Diagonal(1 ./ (λ .+ ρ)) * (Q_inv * (Θ' * Ẋ[:, variable] + ρ * (z - u)))))
            z = S.(ξ + u, μ / ρ)
            u = u + ξ - z
        end

        Ξ[:, variable] = ξ
    end

    return Ξ
end