Base.@kwdef struct ARDLS <: Optimizer
end

using Statistics, LinearAlgebra

"""
Solve the following problem
```
Ax = y + ε
```
for sparse `x` where
- `A` is the matrix of coefficient values (AKA linear combinations of regression variables),
- `y` is the vector of outputs

and AWGN, i.e. `ε ~ N(0, ω .* I) `. Should the noise be relative to the measurements, the input matrix `A`
can be scaled by a matrix `S` in the following matter
```
S*A*x = S*y + S*ε,
```
where `S` it the square root of the inverse covariance matrix of the noise
"""
function (optimizer::ARDLS)(Θ::AbstractMatrix, Ẋ::AbstractVector; max_iter=100000)
    # Scale outputs y and matrix A such that norm(y) ≤ 1 - then the stopping constants will be meaningful
    Mₒ = maximum(abs.(Ẋ))
    Θ = Θ ./ Mₒ
    Ẋ = Ẋ ./ Mₒ
	@show size(Ẋ), size(Θ)

    scale = sqrt.(diag(Θ' * Θ))
    Θₛ = Θ * diagm(1 ./ scale)
	@show size(scale), size(Θₛ)

    covariance = Θₛ' * Θₛ
    factor = Θₛ' * Ẋ
	@show size(covariance), size(factor)

    x = (covariance + maximum(covariance) / 100 * I) \ factor
	@show size(x)

    residue = Ẋ - Θₛ * x
    noise_variance = 1 / mean(residue .^ 2)
    Sx = diagm(abs.(x) .* 0.1 .+ 0.01 .* mean(x))
    @show size(residue), typeof(noise_variance), size(Sx)

    a = 1e-10
    b = copy(a)

    iteration = 1
    old_variance = copy(noise_variance)

    while iteration < max_iter
        xₙ = diagm((a + 0.5) ./ (b .+ 0.5 * (x .^ 2 .+ diag(Sx))))

        Sx = inv(noise_variance * covariance + xₙ)
        x = noise_variance * Sx * factor

        res = Ẋ - Θₛ * x
        noise_variance = size(Θₛ, 1) / (dot(res, res) + tr(Sx * covariance))

        iteration += 1
        old_variance = noise_variance
    end

	x = x ./ scale

    return x
end

(optimizer::ARDLS)(Θ::AbstractMatrix, Ẋ::AbstractMatrix; kwargs...) = hcat((optimizer(Θ, ẋ) for ẋ in eachcol(Ẋ))...)