"""
	Base.@kwdef struct DSTLS <: Optimizer
		τ = 0.1
	end

**Dynamic STLS (DSTLS)**

Fields:
- τ - coefficients under τ times the highest coefficient are zeroed out (in each variable separately)
"""
Base.@kwdef struct DSTLS <: Optimizer
    τ = 0.1
end

function (optimizer::DSTLS)(Θ, Ẋ; max_iter=10, kwargs...)
    # Get the number variables in the "outcome data"
    vars = variables(Ẋ; kwargs...)
    # Compute OLS as the initial guess of our system
    Ξ = Θ \ Ẋ

    for _ in 1:max_iter
        # For each variable, do the thresholded OLS
        for variable in 1:vars
            ξₕ = maximum(abs.(Ξ[:, variable]))
            small_indices = @. abs(Ξ[:, variable]) < (optimizer.τ * ξₕ)
            Ξ[small_indices, variable] .= 0

            big_indices_in = @. !small_indices
            Ξ[big_indices_in, variable] .= Θ[:, big_indices_in] \ Ẋ[:, variable]
        end
    end

    return Ξ
end