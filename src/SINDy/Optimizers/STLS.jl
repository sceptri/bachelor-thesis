"""
	Base.@kwdef struct STLS <: Optimizer
		τ = 0.025
	end

Fields:
- τ determines, how small can a value be before being zeroed
"""
Base.@kwdef struct STLS <: Optimizer
    τ = 0.025
end

function (optimizer::STLS)(Θ, Ẋ; max_iter=10, kwargs...)
    # Get the number variables in the "outcome data"
    vars = variables(Ẋ; kwargs...)
    # Compute OLS as the initial guess of our system
    Ξ = Θ \ Ẋ

    for _ in 1:max_iter
        small_indices = @. abs(Ξ) < optimizer.τ
        Ξ[small_indices] .= 0

        # For each variable, do the thresholded OLS
        for variable in 1:vars
            big_indices_in = @. !small_indices[:, variable]
            Ξ[big_indices_in, variable] .= Θ[:, big_indices_in] \ Ẋ[:, variable]
        end
    end

    return Ξ
end