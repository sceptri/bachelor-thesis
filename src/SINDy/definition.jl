"""
	discover(X, Ẋ, library::T, optimizer::TT; kwargs...) where {T<:Library,TT<:Optimizer}

Function to try to discover dynamics of an autonomous system given by `X` and `Ẋ`. 
	
Only functions from the `library` of functions are used and the problem is solved by the `optimizer`.
"""
function discover(X, Ẋ, library::T, optimizer::TT; kwargs...) where {T<:Library,TT<:Optimizer}
	# First we need to transform our raw data into candidate functions
    Θ = library(X; kwargs...)
	# Then we try to solve the SINDy problem with the given optimizer
    Ξ = optimizer(Θ, Ẋ; kwargs...)
    return Ξ
end