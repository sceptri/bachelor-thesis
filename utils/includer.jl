#############################################################################
# This is an includer script - it is run *before* the TeX compilation of 	#
# thesis and it replaces certain strings with given files					#
#																			#
# For example, writing														#
# 	%INSERT[juliaconsole]{document/snippets/list_comprehension.jl}END%		#
# will include list_comprehension.jl file into the juliaconsole environment,#
# replacing the "includer line" 											#
#																			#
# Most of this functionality relies on a heavy use of regular expressions	#
#############################################################################

INSERT_STATEMENT = r"%INSERT\[+?(.*)\]{+?(.*)(\.jl|\.txt)}(\(.*\))?END%"
INSERT_NO_BLOCK_STATEMENT = r"#INSERT{+?(.*)(\.jl|\.txt)}END#"
INSERT_NO_BLOCK_STATEMENT_ALT = r"%INSERTONLY{+?(.*)(\.jl|\.txt)}END%"
INCLUDE_STATEMENT = r"%INCLUDE\[\[.*\]\]END%"

FILENAME_REGEX = r"{((.*?|\n*?)+?)}"
TEXBLOCK_REGEX = r"\[((.*?|\n*?)+?)\]"
INCLUDED_REGEX = r"\[\[((.*?|\n*?)+?)\]\]"
ARGUMENTS_REGEX = r"}\(((.*?|\n*?)+?)\)"

NO_DOCS_ARG = "NODOCS"
NO_DOCS_REGEX = r"\"\"\"(.|\n)+?\"\"\""

NO_COMMENTS_ARG = "NOCOMMENTS"
NO_COMMENTS_REGEX = r".*#.*\n"

NO_OPTIONAL_ARG = "NOOPTIONAL"
NO_OPTIONAL_REGEX = r"#>--OPTIONAL(.|\n)*?#>--END"

ONLY_STEP_N_ARG = r"STEP_\d{1,}_"
ONLY_STEP_N_CAPTURE = r"_+?(.*)_"
ONLY_STEP_N_REGEX = r"#>--STEP\n((.*?|\n*?)+?)\n#>--END"

TOO_BIG_SPACES_REGEX = r"\n\n\n"

COMPILED_FLAG = "_compiled"
FILETYPE = ".tex"

if !isnothing(ARGS[1])
    default_dir = ARGS[1]
else
    default_dir = "document"
end

in_document = endswith(default_dir)
in_root = endswith("Bachelor Thesis")
in_chapters = endswith("text_prace")

is_texfile = endswith(FILETYPE)
is_compiled = endswith(COMPILED_FLAG * FILETYPE)

directory = pwd()
base_directory = (@__DIR__) * "/../"

function pasteFile(dir, filename, other=nothing)
    contents = read(dir * filename, String)
    if isnothing(other) || !(typeof(other) != String)
        return contents
    end

    arguments = split(other, ',')

    if NO_OPTIONAL_ARG in arguments
        contents = replace(contents, NO_OPTIONAL_REGEX => "")
    end

    if occursin(ONLY_STEP_N_ARG, other)
        arg_match = match(ONLY_STEP_N_CAPTURE, other)
        index = arg_match.captures[1]

        content_match = collect(eachmatch(ONLY_STEP_N_REGEX, contents))
        contents = content_match[parse(Int32, index)].captures[1]
    end

    if NO_COMMENTS_ARG in arguments
        contents = replace(contents, NO_COMMENTS_REGEX => "")
    end

    if NO_DOCS_ARG in arguments
        while occursin(NO_DOCS_REGEX, contents)
            contents = replace(contents, NO_DOCS_REGEX => "")
        end
    end

    while occursin(TOO_BIG_SPACES_REGEX, contents)
        contents = replace(contents, TOO_BIG_SPACES_REGEX => "\n\n")
    end

    return contents
end

function matchInsertStatement(line)
    if occursin(INSERT_STATEMENT, line)
        filename_match = match(FILENAME_REGEX, line)
        texblock_match = match(TEXBLOCK_REGEX, line)
        arguments_match = match(ARGUMENTS_REGEX, line)

        arguments = !isnothing(arguments_match) ? arguments_match.captures[1] : nothing

        new_line = "% !!! WARNING !!! This is an automatically generated part. Please do not modify."
        new_line *= "\n\\begin{" * texblock_match.captures[1] * "}\n"
        new_line *= pasteFile(base_directory, filename_match.captures[1], arguments)
        if new_line[end] != '\n'
            new_line *= '\n'
        end
        new_line *= "\\end{" * texblock_match.captures[1] * "}"

        return new_line
    elseif occursin(INSERT_NO_BLOCK_STATEMENT, line) || occursin(INSERT_NO_BLOCK_STATEMENT_ALT, line)
        filename_match = match(FILENAME_REGEX, line)

        # Don't include a comment by defualt - it would be visible 
        # new_line = "# !!! WARNING !!! This is an automatically generated part. Please do not modify.\n"
        new_line = pasteFile(base_directory, filename_match.captures[1])
        if new_line[end] == '\n'
            new_line = chop(new_line)
        end

        return new_line
    end

    return line
end

function matchIncludeStatement(line)
    if occursin(INCLUDE_STATEMENT, line)
        included_code_match = match(INCLUDED_REGEX, line)

        new_line = "% !!! WARNING !!! This is an automatically generated part. Please do not modify.\n"
        new_line *= included_code_match.captures[1]

        return new_line
    end

    return line
end

if (in_chapters(directory))
    println("Moving down to document base dir")
    cd("..")
    directory = pwd()
end

if (in_root(directory))
    println("Moving up to document base dir")
    cd("document")
    directory = pwd()
end

if (!in_document(directory))
    println("ERROR: Script must be run in document folder of the thesis")
    exit()
end

function replaceFiles()
    base_files = filter(t -> (is_texfile(t) && !is_compiled(t)), readdir())

    for file in base_files
        new_file_contents = ""

        open(directory * "/" * file) do file
            line_number = 0

            # read till end of file
            while !eof(file)
                # read a new / next line for every iteration          
                oldline = line = readline(file)
                line = matchInsertStatement(line)
                if line == oldline
                    line = matchIncludeStatement(line)
                end

                new_file_contents *= (line_number > 0 ? "\n" : "") * line
                line_number += 1
            end

        end

        compiled_dir = in_chapters(directory) ? (directory * COMPILED_FLAG) : directory
        compiled_filename = in_chapters(directory) ? file : replace(file, FILETYPE => (COMPILED_FLAG * FILETYPE))

        compiled_file = compiled_dir * "/" * compiled_filename

        write(compiled_file, new_file_contents)
        println("File $file compiled to $compiled_file")
    end
end

replaceFiles()
if isdir("text_prace")
    cd("text_prace")
    directory = pwd()
    replaceFiles()
end