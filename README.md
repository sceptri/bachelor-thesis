# Data-Driven Dynamical Systems
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/sceptri/bachelor-thesis/-/blob/main/LICENSE)

This is a Git repository for my Bachelor's thesis on Data-driven dynamical systems under the supervision of doc. RNDr. Lenka Přibylová, Ph.D.

The Bachelor's thesis's purpose is to explore and describe modern trends in the discovering of dynamics of nonlinear systems from data. Main focus should be put on models describing the behavior of neurons.

## Introduction

In the current age, we have a lot of data describing many systems. This data can help us understand the behavior of the system in its raw form - but for some systems, we may want to try and learn the equations that govern the system.

In this thesis, we try to explain the theory and practice of mining the equations from data. We try to give many examples and provide whole code needed to run the the example/experiment.

Below we provide a little sneak-peak into some figures of the thesis

![Sampling rate effect](assets/sindy_sampling_rate.png)
![Discovered attractor](assets/attractor.png)

# Project Description
## Structure
- `document` - folder containing all the files for the document itself
  - `thesis.tex` - main TeX file of the document (the thesis itself)
  - `svoc_thesis.tex` - main TeX file for the SVOC version of the thesis
  - `text_prace` - folder, that contains chapters of the main document 
  - `text_prace_compiled` and `thesis_compiled.tex` - compiled versions of the above mentioned files
  - `fig` - folder containing figures generated on the fly during compilation
  - `snippets` - folder containing mostly little snippets insert into document compilation
  - `borrowed_figures` - folder containing figures we have NOT created (and they are cited in the document itself)
- `presentation` - folder containing all the files for the defense (presentation) of the thesis
  - `presentation.tex` - main TeX file of the presentation
- `experiments` - folder containing all the experiments (simulation studies)
  - Experiment folders (almost) always contain:
    - `generator.jl` - file that generates data and writes it to CSV
    - `learner.jl` - file that learns the equations from `data.csv`
    - `runner.jl` - **USE THIS FILE FOR RUNNING THE EXPERIMENT** (if it exists for a given experiment and you do not want to tinker with it)
    - `tester.jl` - version of `runner.jl` used only for testing purposes
- `utils` - folder containing Julia scripts that are used outside of experiments (e.g. for compilation, cleaning, etc.)
- `src` - folder containing Julia code that is used with experiments (e.g. definitions of structures, functions, etc.) 
- `test` - folder containing all the test suites
- `assets` - folder containing images used in this `README.md`

## Project packages

Package `DataDrivenDynamics` was created for the purpose of this thesis. It is comprised of multiple submodules:
- `Noiser` - submodule containing code for noising and denoising data (denoise with the help of `TotalVaritation.jl` package)
  - available noises are `AdditiveNoise`, `AdditiveVarianceNoise`, `MultiplicativeNoise` and `CompositeNoise`
  - look at `Noise-Comparisons` experiment to see them in action
- `ProjectKeeper` - submodule containing a custom naming scheme functionality (also enables setting a log level)
  - it allows a deterministic naming of experiment data and figures
  - user can create folder, define transformations of variables into parts of filenames, etc.
- `Sparse` - small submodule containing DSTLS optimizer code compatible with `DataDrivenSparse` package
- `SINDy` - submodule with custom implementation of SINDy method
  - full and simple implementation of SINDy method
  - implemented optimizers: `LASSO`, `STLS`, `DSTLS`
  - implemented libraries: `polynomial_library`, `monomial_library` and a `custom_library`

# Running the project
Here we try to list all the packages necessary to run/compile this project. Also we strive to provide tips and tricks, if we have any.

## Prerequisites

*Disclaimer: I do NOT own a Windows device, so running on Windows is not as heavily tested*

> Mainly, I haven't tried `make` (and corresponding `makefiles`) on Windows. Please contact the author in case of trouble

You need to have Julia installed. For the installation process help, please refer [here](https://juliateachingctu.github.io/Julia-for-Optimization-and-Learning/stable/installation/vscode/) or [here](https://julialang.org/downloads/)

> To test, that Julia is installed correctly, try running `julia` in the terminal
> Julia **must** be added to the PATH

Whilst it is not mandatory I **strongly recommend** install `make`, as it will improve your experience by orders of magnitude - The entirety of this project heavily relies on `make` and *makefiles* for ease-of-use. **Please make sure you have `make` installed.**
> For help with installing `make` on Windows, please refer [here](https://stackoverflow.com/questions/32127524/how-to-install-and-use-make-in-windows)
> 
> Using chocolatey is probably the easiest way 

> On Windows you can (probably) also use the cygwin version

## Compiling the document

We use a [makefile](https://gitlab.com/sceptri/bachelor-thesis/-/blob/main/document/makefile) to compile the document.

> We employ PythonTeX package among others, which rules out the use of `latexmk` as the compilation manager

**Compilation order**:
1. symlink fonts and BibTeX file to `document` folder
2. "compile" the TeX files with `utils/includers.jl`
3. compile the document with LuaLaTeX for the first time
4. run PythonTex, Biber and Xindex
5. run LuaLaTeX one more time
6. cleaning of temporary files, etc.


For the document compilation you'll need `make`, `lualatex`, `biber`, `xindex`, `julia` and `latexmk` (for cleaning). The necessary Julia packages are listed in the `document/Project.toml` and TeX packages are found in the `thesis.tex`, but namely you will need
- `texlive-biblatex-iso690`, `biblatex`
- `texlive-nicematrix`, `texlive-luavna`
- `texlive-pythontex`
- `texlive-lcg`, `texlive-hf-tikz`

**Useful commands**
1. Before the first compilation, please initialize the Julia project necessary for document using `make doc-init`.
2. After that, compilation can be started simply with `make compile` or `make doc-compile` if you surely want to compile just the document
3. If a need arises to clean intermediate files created in document compilation process, please use `make doc-clean`
4. If at any point you feel stuck with the compilation and don't know which command to use, please try `make help` or `make doc-help`

> Please, do **NOT** edit the project over on Overleaf. Then, one needs to manually sync from the Overleaf and merge it locally, before pushing to Gitlab.

When using Overleaf, please use `main.tex` or `main_presentation.tex` as the main compile file

## Running the experiments
Please, use the main directory as a working directory for all operations.
Otherwise I cannot guarantee, that everything will work as intended.

### Useful commands
*You must be in the root directory, if you want to use* `make` *command shortcuts* 

1. Before running any Julia code related to experiments, etc., please use `make code-init` to initialize the project, download packages and precompile them
	> Alternatively run in the terminal \
	> `julia --project -e 'using Pkg; Pkg.activate("experiments"); Pkg.develop(path="."); Pkg.instantiate()'`
	
	> Or in Julia shell \
	> `using Pkg; Pkg.activate("experiments"); Pkg.develop(path="."); Pkg.instantiate()` \
	> or
	>	```julia
	>	] activate experiments
	>	] dev .
	>	] instantiate
	>	```

	> If any error is thrown, please try running in Julia shell \
	`] registry up`
2. If you want to run a given experiment, say "*FitzHugh-Nagumo*", please consider using a runner: `make run experiment=FitzHugh-Nagumo`. This will give you a streamlined experience with this project
   > Otherwise, go to `experiments/FitzHugh-Nagumo` folder and run `julia runner.jl`

	> Or run `include("runner.jl")` in the Julia shell in the experiment directory
    1. If you want to run only the generator part of the experiment, use e.g. `make run-gen experiment=FitzHugh-Nagumo`
		> Alternative run `julia runner.jl nolearn` in the folder of the chosen experiment
	2. If you want to run only the learber part of the experiment, use e.g. `make run-learn experiment=FitzHugh-Nagumo`
		> Alternative run `julia runner.jl nogen` in the folder of the chosen experiment


In the `runner.jl` file, you will typically find all the parameters you may want to tweak, along with a description of what they do. 

> If an experiment does NOT have a `runner.jl` file, it is probably deprecated & not used in the final version of the thesis or simply not of a great interest to an ordinary user
> 
> The deprecated experiments are: `1D-Neuron-Multiple`, `1D-Neuron-Simple`, `3D-Lorenz-Time_Delay_Embedding`
> 
> Experiments `FitzHugh-Nagumo-Bare` (barebones version of `FitzHugh-Nagumo` experiment) and `FitzHugh-Nagumo-Custom` (barebones version of said experiment, implemented using our own package) do NOT provide anything over `FitzHugh-Nagumo` and as such, this experiment should be used instead 

> Experiment `Hodgkin-Huxley` is similar to `FitzHugh-Nagumo` and `3D-Lorenz-Simple` in structure, but it was NOT used in the thesis itself

> Experiment `3D-Lorenz-Sampling_Rate` contains only `generator_and_runner.jl`, so `run-learn` and `run-gen` options do NOT make sense in this case

# DevOps & Organzation

## Gitlab CI/CD Pipeline
When pushing to the repository, using [CI/CD pipeline](https://gitlab.com/sceptri/bachelor-thesis/-/blob/main/.gitlab-ci.yml) we push the code to the Overleaf remote - this eases collaboration.

> (At least) due to the use PythonTex, Overleaf **cannot** compile the document correctly (for security reasons, execution of arbitrary code is forbidden) - what we do is version control the PythonTex intermediate files, that then can be used by Overleaf in TeX compilation

## Organization
You can monitor, watch and influence the progress and development of this bachelor thesis on a [kanban board](https://vikunja.zapadlo.name/share/SQRwHrZLRTQQLqxJEfkFUZRIeuPBcgfZNJPSNIOo/auth).

### Useful Links
Notes for my Bachelor's Thesis, which (for some reason) may not be in the main document, can be found here: [Knowledge Base](https://bookstack.zapadlo.name/books/bachelors-thesis)

As mentioned earlier, the progress tracker and the kanban board for this project lies here: [Kanban Board](https://vikunja.zapadlo.name/share/SQRwHrZLRTQQLqxJEfkFUZRIeuPBcgfZNJPSNIOo/auth)  