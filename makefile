ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

# --- GENERAL SECTION ---
# Command that pushes current HEAD to overleaf:master
sync:
	git push overleaf HEAD:master
# Makefile conditional based on:
# https://stackoverflow.com/questions/52245838/reading-user-input-in-a-makefile-script
compile:
	@echo "Compile presentation? [N/y]"
	@read target; if [ $$target == "y" ]; \
	then $(MAKE) pres-compile; \
	else $(MAKE) doc-compile; \
	fi
# To be used with VSCode and LaTeX external build system
auto-compile:
	@if [ $(ARGS) == presentation ] || [ $(ARGS) == svoc_presentation ]; \
	then $(MAKE) pres-auto-compile ARGS=$(ARGS); \
	else $(MAKE) doc-auto-compile ARGS=$(ARGS); \
	fi

# --- DOCUMENT SECTION ---
# For more info, refer to document/makefile
# When changing the folder, we need to actually use the make from the folder itself 

doc-compile:
	@cd document && \
	$(MAKE) compile
doc-auto-compile:
	@cd document && \
	$(MAKE) auto-compile ARGS=$(ARGS)
doc-clear-fonts:
	@cd document && \
	$(MAKE) clear-fonts
doc-clean:
	@cd document && \
	$(MAKE) clean
doc-init:
	@cd document && \
	$(MAKE) init

# --- PRESENTATION SECTION ---
# For more info, refer to presentation/makefile
# When changing the folder, we need to actually use the make from the folder itself 

pres-compile:
	@cd presentation && \
	$(MAKE) compile
pres-auto-compile:
	@cd presentation && \
	$(MAKE) auto-compile ARGS=$(ARGS)
pres-clear-fonts:
	@cd presentation && \
	$(MAKE) clear-fonts
pres-clean:
	@cd presentation && \
	$(MAKE) clean
pres-init:
	@cd presentation && \
	$(MAKE) init


# --- CODE SECTION ---

code-init:
	julia --project -e 'using Pkg; Pkg.activate("experiments"); Pkg.develop(path="."); Pkg.instantiate()'
code-test:
	julia -e 'using Pkg; Pkg.activate("."); Pkg.build("DataDrivenDynamics"); Pkg.test("DataDrivenDynamics"; coverage = true)'
code-coverage:
	julia -e 'using Pkg; Pkg.add("Coverage"); import DataDrivenDynamics; cd(joinpath(dirname(pathof(DataDrivenDynamics)), "..")); using Coverage; cl, tl = get_summary(process_folder()); println("(", cl/tl*100, "%) covered")'
code-unit-test:
	$(MAKE) run-test experiment=3D-Lorenz-Simple
	$(MAKE) run-test experiment=3D-Lorenz-Noise
	$(MAKE) run-test experiment=3D-Lorenz-Sampling_Rate

	$(MAKE) run-test experiment=FitzHugh-Nagumo
	$(MAKE) run-test experiment=FitzHugh-Nagumo-Noise

	$(MAKE) run-test experiment=Hodgkin-Huxley
	$(MAKE) run-test experiment=Hodgkin-Huxley-to-FitzHugh-Nagumo

	$(MAKE) run-test experiment=Noise-Comparisons

# --- RUNNER SECTION ---

run:
	@echo "Running the experiment $(experiment)"
	@cd "experiments/$(experiment)" && \
	julia runner.jl
run-learn:
	@echo "Running the learner experiment $(experiment)"
	@cd "experiments/$(experiment)" && \
	julia runner.jl nogen
run-gen:
	@echo "Running the generator experiment $(experiment)"
	@cd "experiments/$(experiment)" && \
	julia runner.jl nolearn
run-test:
	@echo "Running the tester of the experiment: $(experiment)"
	@cd "experiments/$(experiment)" && \
	julia tester.jl


# --- HELP SECTION ---

help:
	@echo "Help page for my bachelor's thesis"
	@echo "Common commands: (run any of them if 'make <command>' if not stated otherwise)"
	@echo "  'compile' - compiles the thesis (and asks you, if you want compile the document or the presentation)"
	@echo "  'run experiment=<experiment-name>' - runs both generator and learner of the supplied experiment"
	@echo "  'code-init' - initialises Julia project for the experiments"
	@echo "  'doc-init' - initialises Julia project for the document"
	@echo "  'doc-clean' - cleans auxiliary TeX files used for compilation"
	@echo "  'sync' - synchronizes local main branch to Overleaf remote"
	@echo "Or try any of the following help pages:"
	@echo "  'run-help'  'code-help'  'doc-help'  'pres-help'"
run-help:
	@echo "Wrapper around Julia runners to ease using the code part of the bachelor's thesis"
	@echo "This feature is described in README.md along with its usage with experiments"
	@echo "  'run experiment=<experiment-name>' - runs both generator and learner of the supplied experiment"
	@echo "  'run-learn experiment=<experiment-name>' - runs the learner part of the supplied experiment"
	@echo "  'run-gen experiment=<experiment-name>' - runs the generator part of the supplied experiment"
code-help:
	@echo "Available commands are:"
	@echo "  'code-init' - initialises Julia project for the experiments"
	@echo "  'code-test' - runs the test suite of the project"
	@echo "  'code-unit-test' - runs test runners of all available experiments"
doc-help:
	@cd document && \
	$(MAKE) help
pres-help:
	@cd presentation && \
	$(MAKE) help