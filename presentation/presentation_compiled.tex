\documentclass[
]{beamer}

\usepackage[no-math]{fontspec}
\defaultfontfeatures{Mapping=tex-text}
\usepackage[czech]{babel}
\usepackage{csquotes}
\usepackage{expl3,biblatex}
\addbibresource{thesis.bib}
\usepackage{booktabs}
\usetheme[
  workplace=sci,
]{MU}

\newcommand*{\preambule}{preambule.tex}
% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\renewcommand*{\preambule}{preambule_compiled.tex}
\input{\preambule}

\title[Dynamické systémy řízené daty]{Dynamické systémy řízené daty}
\subtitle[Řídká Identifikace Nelineární Dynamiky (SINDy)]{Řídká Identifikace Nelineární Dynamiky (SINDy)}
\author[Š.\,Zapadlo]{Štěpán Zapadlo\texorpdfstring{\\}{, }505579@mail.muni.cz}
\institute[PřF MU]{Přírodovědecká fakulta Masarykovy univerzity}
\date{\today}
\subject{Dynamické systémy řízené daty}
\keywords{dynamické systémy, strojové učení, SINDy, matematická optimalizace, řídká regrese}

\begin{document}

\begin{frame}[plain]
\maketitle
\end{frame}

\section[Úvod]{Úvod}
\subsection[Motivace]{Motivace}

\begin{frame}{Motivace}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\includegraphics[width=\textwidth]{fig/neuron_motivation.png}
		\end{column}

		\begin{column}{0.5\textwidth}
			{
				\huge\boldmath
				$\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont $?$}}}{\longrightarrow}}\quad\dot{\vv{x}} = \vv f (\vv x)$
			}
		\end{column}
	\end{columns}
\end{frame}

\subsection[Výsledky]{Výsledky}

\begin{frame}{Výsledky}
	\begin{enumerate}
		\item implementace SINDy v jazyce \Julia
		\item formulace SINDy pomocí řídké regrese
		\item odstranění šumu
		\item numerický odhad derivace (robustní vůči šumu)
		\item nová optimalizační metoda DSTLS
		\item chování metody SINDy na zašuměných datech
		\item srovnání optimalizačních metod
		\item simulační studie
		\begin{enumerate}
			\item FitzHughův--Nagumův model
			\item Lorenzův model
			\item Transformace Hodgkinova--Huxleyho modelu na FitzHughův--Nagumův model
		\end{enumerate}
	\end{enumerate}
\end{frame}

\section[SINDy]{Řídká identifikace nelineární dynamicky (SINDy)}
\subsection[Teoretická formulace]{Teoretická formulace}

\begin{frame}{SINDy}
	Zkoumaný dynamický systém hledáme ve tvaru
	\begin{equation*}
		\dot{\vv{x}} = \vv f (\vv x), \quad \vv f : \R^l \to \R^l.
	\end{equation*}
	Můžeme jej aproximovat pomocí kandidátních funkcí $\vvb \Theta(\vv x)$ jako
	\begin{equation*}
		\vv f(\vv x) \approx \mtr{
			\theta_1(\vv x) \xi_{1,1} + \dots + \theta_p(\vv x)\xi_{p, 1} \\
			\vdots \\
			\theta_1(\vv x) \xi_{1,l} + \dots + \theta_p(\vv x)\xi_{p, l}
		}^T = \vvb \Theta(\vv x) \vvb \Xi,
	\end{equation*}
	což interpretujeme jako $l$-\textit{rozměrný lineární model}, ve kterém se snažíme minimalizovat počet nenulových členů v $\vvb \Xi$, kde $\theta_i : \R^l \to \R$ a také
	\begin{equation*}
		\vvb \Theta(\vv x) = \mtr{\theta_1(\vv x), \dots,\theta_p(\vv x)}, \qquad
		\vvb \Xi = \mtr{\vvb \xi_1, \dots, \vvb \xi_l}, \qquad 
		\vvb \xi_i \in \R^p.
	\end{equation*}
\end{frame}

\begin{frame}{SINDy}{Data}
	Mějme data z časové řady příslušné hledanému dynamickému systému
	\begin{equation*}
		\vv X = \bmtr{\vv x(t_1) & \vv x(t_2) & \dots & \vv x(t_k)}^T
	\end{equation*}
	a derivace
	\begin{equation*}
		\vvb{\dot X} = \bmtr{\dot{\vv{x}}(t_1) & \dot{\vv{x}}(t_2) & \dots & \dot{\vv{x}}(t_k)}^T.
	\end{equation*}

	Potom vytvoříme knihovnu kandidátů 
	\begin{equation*}
		\vvb \Theta(\vv X) = \bmtr{\vvb 1 & \vv X & P_2(\vv X) & \dots & P_d(\vv X) & \dots & \sin (\vv X) & \dots & \vv g(\vv X)},
	\end{equation*}

	a celkem dostáváme \textit{multilineární model}
	\begin{equation*}
		\vvb{\dot{X}} = \vvb \Theta (\vv X) \vvb \Xi + \vvb \epsilon.
	\end{equation*}
\end{frame}

\begin{frame}{SINDy}{Optimalizační problém}
	Vhodná matice koeficientů $\vvb \Xi$ řeší úlohu
	\begin{equation*}
		\min_{\vvb \Xi} \frac 1 2 \norm{\vvb{\dot{X}} - \vvb \Theta(\vv X) \vvb \Xi}^2_2 + \mu R(\vvb \Xi),
	\end{equation*}
	kde $R(\cdot)$ je regularizující funkce vynucující řídkost -- nejčastěji $l_1$-norma $\norm{\cdot}_1$ (a přidružená maticová norma), což lze řešit např. přes LASSO regresi (a ADMM algoritmus) nebo pomocí SR3 metody.\\

	Alternativně můžeme řídkost vynutit přes omezení optimalizace, jako např. u metody STLS, resp. nově navržené DSTLS
	\begin{equation*}
		\begin{gathered}
			\min_{\vvb \Xi} \frac 1 2 \norm{\vv Y - \vvb \Theta(\vv X) \vvb \Xi}_2^2 \\
			\text{s.t. } \forall i \in \set{1, \dots, l}, j \in \set{1, \dots, p}: \\
			\absval{\xi_{j,i}} > \tau_i,
		\end{gathered}
		\qquad
		\begin{gathered}
			\min_{\vvb \Xi} \frac 1 2 \norm{\vv Y - \vvb \Theta(\vv X) \vvb \Xi}_2^2 \\
			\text{s.t. } \forall i \in \set{1, \dots, l}, j \in \set{1, \dots, p}: \\
			\absval{\xi_{j,i}} > \tau_i \cdot \max \set{\absval{\xi} : \xi \in \vvb \xi_i}.
		\end{gathered}
	\end{equation*}
\end{frame}

\subsection[Implementace]{Implementace}

\begin{frame}[fragile=singleslide]{Implementace}{FitzHughův--Nagumův model}
	Ukažme si správnost implementace na FitzHughově--Nagumově zjednodušeném modelu neuronu
	\begin{align*}
		\dot V &= 0.8 - W + V - \frac 1 3 V^3, \\
		\dot W &= 0.056 - 0.064 \cdot W + 0.08 \cdot V.
	\end{align*}

% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaconcode}
using Revise
using ModelingToolkit, DifferentialEquations
using DataFrames, CSV, Random

# My custom package
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator
using DataDrivenDynamics.SINDy

# Setting up independent variable namely time t and dependent variable V(t), W(t)
@variables t V(t) W(t)
@parameters a b c d iₑ

D = Differential(t)

# Governing equations for our system
V̇(V, W, a, b, c, d, iₑ) = V - V^3 / 3 - W + iₑ
Ẇ(V, W, a, b, c, d, iₑ) = a * (b * V - c * W + d)

params = [
    a => 0.08, b => 1.0,
    c => 0.8, d => 0.7,
    iₑ => 0.8
]
starting_values = [3.3, -2.0]
time_range = (0.0, 100.0)

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(V) ~ V̇(V, W, a, b, c, d, iₑ),
    D(W) ~ Ẇ(V, W, a, b, c, d, iₑ)
])
ode_problem = ODEProblem(ode_system, starting_values, time_range, params)

# The ODE problem may be stiff, so use a stiff solver
solution = solve(ode_problem, Rosenbrock23(); saveat=0.1)

# Compute the derivatives from the data
dict = Dict(params)
derivatives = [[
    V̇(V, W, dict[a], dict[b], dict[c], dict[d], dict[iₑ]),
    Ẇ(V, W, dict[a], dict[b], dict[c], dict[d], dict[iₑ]),
] for (V, W) in solution.u]

# transform vectors of states and derivatives into matrices
X = vcat(reshape.(solution.u, 1, 2)...)
Ẋ = vcat(reshape.(derivatives, 1, 2)...)
\end{juliaconcode}

	Prvně určíme knihovnu a optimalizační metodu, pak spustíme SINDy
	\begin{juliaconsole}
basis = PolynomialLibrary(3, 2);
optimizer = STLS();
Ξ = discover(X, Ẋ, basis, optimizer; max_iter=100);
prettyprint(Ξ, basis; vars=["V" "W"])
	\end{juliaconsole}
\end{frame}

\subsection[Chování SINDy]{Chování SINDy metody}

\begin{frame}{Chování SINDy na Lorenzově modelu}{Počet snímků a jejich frekvence}
	\begin{figure}
		\centering
		\tryshow{../experiments/3D-Lorenz-Sampling_Rate/figures/sampling_rate_and_duration/[-20, -20, 25]_30.0_10_5_0.01_0.1:0.05:2.5_4.0:0.25:15.0.pdf}{width=\textwidth}
		\caption{Vliv doby mezi jednotlivými pozorováními a počtu pozorování na chování SINDy metody na Lorenzově modelu}
		\label{fig:sindy_sampling_rate_and_duration}
	\end{figure}
\end{frame}

\begin{frame}{Chování SINDy}{Vliv šumu na SINDy na FitzHughově--Nagumově modelu}
	\begin{figure}
		\centering
		\tryshow{../experiments/FitzHugh-Nagumo-Noise/figures/[STLSQ(0.1), SR3(0.03), ADMM(0.025), DSTLS(0.1)]/pres-quality-of-fit-for-sigma/k=[4, 5]_noise_range=0.001:0.005:0.096_reg=[0.08, 0.9]_trajectory_count=10_AdditiveVarianceNoise_λ=[0.3, 0.3].pdf}{height=0.7\textheight}
		\caption{Vliv velikosti rozptylu šumu na chování SINDy metody na FitzHughově--Nagumově modelu}
		\label{fig:sindy_fhn_noise_comparison}
	\end{figure}
\end{frame}

\section[Simulační studie]{Simulační studie}

\begin{frame}{Simulační studie}{Zkoumané modely}
	\begin{columns}[t]
		\begin{column}{0.475\textwidth}
			FitzHughův--Nagumův model neuronu má tvar
			\begin{align*}
				\dot V &= V - \frac {V^3} 3 - W + i_e, \\
				\dot W &= a \cdot \brackets{b \cdot V - c \cdot W + d},
			\end{align*}
			přičemž parametry volíme jako
			\begin{gather*}
				a = 0.08, b = 1, c = 0.8, d = 0.7,\\
				i_e = 0.8.
			\end{gather*}
		\end{column}%
		\begin{column}{0.475\textwidth}
			Lorenzův zjednodušený model konvekčního proudění v atmosféře má tvar
			\begin{equation*}
				\begin{aligned}
					\dot x &= \sigma (y - x), \\
					\dot y &= x (\rho - z) - y, \\
					\dot z &= x y - \beta z,
				\end{aligned}
			\end{equation*}
			přičemž parametry volíme jako
			\begin{equation*}
				\sigma = 10, \quad \rho = 28, \quad \beta = \frac 8 3.
			\end{equation*}
		\end{column}
	\end{columns}
\end{frame}

\subsection[FitzHughův--Nagumův model]{FitzHughův--Nagumův model}

\begin{frame}{FitzHughův--Nagumův model}{SINDy}
	Pro SINDy algoritmus použijeme v tomto případě metodu \alert{DSTLS} s $\tau = 0.15$ a \alert{polynomiální knihovnu nejvýše 4. stupně}.

 	Nalezený model má tvar
 	\begin{align*}
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{V} =&\hphantom{+} 0.7 + 0.83 \cdot \hat{V}  - 0.85 \cdot \hat{W}  - 0.29 \cdot \hat{V} ^3,\\
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{W} =&\hphantom{+} 0.05 + 0.08 \cdot \hat{V}  - 0.05 \cdot \hat{W} 
		,
	\end{align*}
	což můžeme srovnat s původním modelem
	\begin{align*}
		\frac{\d}{\d t} V &= 0.8 + V - W - \frac 1 3 V^3, \\
		\frac{\d}{\d t} W &= 0.056 + 0.08 \cdot V - 0.064 \cdot W.
	\end{align*}
\end{frame}

\begin{frame}{FitzHughův--Nagumův model}{Predikovaná trajektorie}
	\begin{figure}
		\centering
		\tryshow{../experiments/FitzHugh-Nagumo/figures/predicted-trajectory-comparison/k:[4, 5]_DSTLS(0.15)_reg:[0.08, 0.9]_tvdiff_df_polynomial_basis(variables, 4)_AdditiveNoise(0.0, 0.1)_denoised_trajectory_λ:[0.3, 0.3].pdf}{height=0.65\textheight}
		\caption{Srovnání původní a nově simulované trajektorie}
		\label{fig:fhn_predicted_trajectory}
	\end{figure}
\end{frame}


\subsection[Lorenzův model]{Lorenzův model}

\begin{frame}[fragile=singleslide]{Lorenzův model}{SINDy}
	\begin{columns}
		\begin{column}{0.6\textwidth}
			Pro SINDy algoritmus použijeme \alert{polynomiální knihovnu nejvýše 3. stupně}, metodu \alert{DSTLS} s $\tau = 0.02$ a srovnáme ji s metodou \alert{STLS} s $\tau = 0.4$.
		\end{column}
		\begin{column}{0.35\textwidth}
			Lorenzův model:
			\scriptsize
			\begin{align*}
				\frac{\d}{\d t} x &= 10 \cdot y - 10 \cdot x, \\
				\frac{\d}{\d t} y &=  - y + 28 \cdot x - xz, \\
				\frac{\d}{\d t} z &= xy - \frac 8 3 \cdot z
			\end{align*}
		\end{column}
	\end{columns}

	\hfill

 	Nalezené modely mají tvar: \\
	\begin{columns}
		\begin{column}{0.46\textwidth}
		   	STLS:
			\scriptsize
			\begin{align*}
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{x} =&\hphantom{+} 0.86 + 9.3 \cdot \hat{y}  - 9.08 \cdot \hat{x} ,\\
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{y} =&\hphantom{+} 18.27 \cdot \hat{y}  - 7.14 \cdot \hat{x}  - 0.49 \cdot \hat{y}  \cdot \hat{z} ,\\
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{z} =&\hphantom{+} 0.93 \cdot \hat{x}  \cdot \hat{y}  - 11.4 - 2.02 \cdot \hat{z} 
			\end{align*}
		\end{column}
		\begin{column}{0.46\textwidth}
			DSTLS:
			\scriptsize
			\begin{align*}
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{x} =&\hphantom{+} 0.86 + 9.3 \cdot \hat{y}  - 9.08 \cdot \hat{x} ,\\
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{y} =&\hphantom{+} 23.01 \cdot \hat{x}  - 0.86 \cdot \hat{x}  \cdot \hat{z} ,\\
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{z} =&\hphantom{+} 0.14 \cdot \hat{x} ^2 + 0.84 \cdot \hat{x}  \cdot \hat{y}  - 2.51 - 2.51 \cdot \hat{z} 
			\end{align*}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Lorenzův model}{Atraktory}
	\begin{figure}[htb]
		\centering
		 \begin{subfigure}[t]{0.49\textwidth}
			\centering
			\tryshow{../experiments/3D-Lorenz-Simple/figures/polynomial_basis(variables, 3)/pres-attractor-plot/k=[4, 5, 5]_STLSQ(0.4)_reg=[0.08, 0.9, 0.6]_tvdiff_df_AdditiveNoise(0.0, 1.0)_denoised_trajectory_λ=[0.3, 0.3, 0.3].pdf}{width=0.9\textwidth}
			\caption{Srovnání atraktoru pro původní a nalezený systém pro STLS}
			\label{fig:lorenz_attractor_stls}
		 \end{subfigure}
		 \hfill
		 \begin{subfigure}[t]{0.49\textwidth}
			\centering
			\tryshow{../experiments/3D-Lorenz-Simple/figures/polynomial_basis(variables, 3)/pres-attractor-plot/k=[4, 5, 5]_DSTLS(0.02)_reg=[0.08, 0.9, 0.6]_tvdiff_df_AdditiveNoise(0.0, 1.0)_denoised_trajectory_λ=[0.3, 0.3, 0.3].pdf}{width=0.9\textwidth}
			\caption{Srovnání atraktoru pro původní a nalezený systém pro DSTLS}
			\label{fig:lorenz_attractor_dstls}
		 \end{subfigure}
		 \caption{Atraktory trajektorií naučených modelů}
		 \label{fig:lorenz_attractors_all}
	\end{figure}
\end{frame}

\section[Transformace HH modelu na FHN model]{Transformace HH modelu na FHN model}

% \subsection[FitzHughův přístup]{FitzHughův přístup}

% \begin{frame}[fragile=singleslide]{FitzHughova transformace HH modelu na FHN model}
% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliacode}
using CairoMakie, LinearAlgebra, Random, LaTeXStrings
import ColorSchemes

resolution = (400, 350)
labels = (
	:xlabel => L"V",
	:ylabel => L"W"
)
params = (
	:stepsize => 0.05,
	:arrow_size => 8,
	:maxsteps => 40,
	:colormap => :Nizami,
)

a = 0.082; b = 1.0; c = 0.8; d = 0.7
iₑ = 1.425

V̇(V, W) = @. V - V^3 / 3 - W + iₑ
Ẇ(V, W) = @. a * (b * V - c * W + d)

fig = Figure(; resolution)
ax = Axis(fig[1, 1]; labels..., palette = (color = cgrad(:Nizami, rev = true),))

let f(V,W) = Point2f(V̇(V, W),Ẇ(V, W))
	streamplot!(ax, f, -2.5..2.5, -0.5..2.5; params...)
end

converge_steps = 300
draw_steps = 1000

make_step(V, W; Δ = 0.1) = @. (V,W) + Δ * (V̇(V, W), Ẇ(V, W))

V, W = 0.0, 0.0
for _ in 1:converge_steps
	global V,W
	V, W = make_step(V, W)
end

cycle_V, cycle_W = Float64[], Float64[]
for _ in 1:draw_steps
	global V,W
	V, W = make_step(V,W)
	push!(cycle_V, V)
	push!(cycle_W, W)
end
lines!(ax, cycle_V, cycle_W; linewidth = 3, linestyle = :dash, label = L"\text{limit cycle}")
axislegend(position = :lb)
fig
\end{juliacode}

% 	\begin{figure}[htb]
% 		\centering
% 		 \begin{subfigure}[t]{0.49\textwidth}
% 			\centering
% 			\SaveAndPlot{fig}{fhn-phase-portrait}{fig}{width=\\textwidth}
% 			\caption{Fázový portrét FHN modelu}
% 			\label{fig:hh_to_fhn_base:fhn_phase_portrat}
% 		 \end{subfigure}
% 		 \hfill
% 		 \begin{subfigure}[t]{0.49\textwidth}
% 			\centering
% 			\tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/original_trajectory_transform/both_pres.pdf}{width=\textwidth}
% 			\caption{SVD a originální FitzHughova transformace}
% 			\label{fig:hh_to_fhn_base:svd_and_fitzhugh}
% 		 \end{subfigure}
% 		 \label{fig:hh_to_fhn_base}
% 	\end{figure}
% \end{frame}

\subsection[Optimalizační přístup]{Optimalizační přístup}

\begin{frame}{Formulace optimalizační úlohy}
	Nechť $\vvb H \in \R^{k \times 4}$ je trajektorie HH modelu, kde $k$ je počet pozorování, a FHN model je daný vztahem
	$$
		\dot{\vv u} = \vv f(\vv u; \vv p) \quad \text{s řešením } \vv u(t; \vv p, \vv u_0) \text{ a parametry } \vv p = (i_e, a, b, c, d),
	$$
	přičemž časy měření HH modelu označme $T$. 
	
	Vyřešení úlohy vyžaduje zároveň nalezení vhodné transformace trajektorie $\vvb H$, ale také i vhodných parametrů $\vv p$ a počáteční podmínky $\vv u_0$ FHN modelu, tj. se jedná o úlohu
	\begin{equation*}\label{eq:linear_optim_inf}
		\min_{\vvb \Lambda, \vv p, \vv u_0, \vv q} \brackets{\loss \brackets{\vv u(T; \vv p, \vv u_0), \vvb \Theta(\vv H; \vv q) \vvb \Lambda} + \mu \norm{\vv p}_2},
	\end{equation*}
	přičemž ztrátovou funkci volíme jako (experimentálně určeno)
	\begin{equation*}\label{eq:full_transform_loss}
		\loss (\vv x, \vv y) = \frac 1 k \norm{\vv x - \vv y}_2^2 + \alpha \absval{1 - \frac {\stdev(\vv x)} {\stdev(\vv y)}} + \beta \absval{1 - \frac {\stdev (\vv x_a)}{\stdev (\vv x_b)}}.
	\end{equation*}
\end{frame}

\begin{frame}{Transformace}{Lineární transformace}
	V případě lineární transformace volíme $\vvb \Theta(\vv H) = \vv H$.
	\begin{figure}[H]
		\centering
		\begin{subfigure}[t]{0.49\textwidth}
		   \centering
		   \tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/linear_transformation_comparison/infinite_train_α=50.0_β=1.0_μ=0.1_pres.pdf}{width=0.7\textwidth}
		   \label{fig:lin_transform_comparison_inf}
		\end{subfigure}
		\begin{subfigure}[t]{0.49\textwidth}
			\centering
			\tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/linear_transformation_phase_space/infinite_train_α=50.0_β=1.0_μ=0.1_pres.pdf}{width=0.7\textwidth}
			\label{fig:lin_phase_space:inf}
		\end{subfigure}
		\begin{subfigure}[t]{0.49\textwidth}
			\centering
			\tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/linear_transformation_comparison/finite_train_α=1.0_β=1.0_μ=0.1_pres.pdf}{width=0.7\textwidth}
			\label{fig:lin_transform_comparison_fin}
		\end{subfigure}
		\begin{subfigure}[t]{0.49\textwidth}
			\centering
			\tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/linear_transformation_phase_space/finite_train_α=1.0_β=1.0_μ=0.1_pres.pdf}{width=0.7\textwidth}
			\label{fig:lin_phase_space:fin}
		\end{subfigure}
		\label{fig:lin_phase_space}
	\end{figure}
\end{frame}

\begin{frame}{Transformace}{Alternativní transformace}
	Proměnné $m,h,n \in [0,1]$ z HH modelu jsou asociované s pravděpodobnostmi, ale proměnné FHN modelu mají (teoreticky) libovolný rozsah. Proto použijeme inverzní sigmoidu $S^{-1} : [0,1] \to \R$ a tedy použitá knihovna kovariátů má tvar
	$$
		\vvb \Theta_s(\vv H; \vv q, \vv r) = \mtr{
			\vdots	&  \vdots	& \vdots	& \vdots \\
			\vv H_{i, \cdot}		& S^{-1}(m; \vv q_1, \vv r_1) & S^{-1}(h; \vv q_2, \vv r_2) & S^{-1}(n; \vv q_3, \vv r_3) \\
			\vdots	&  \vdots	& \vdots	& \vdots
		}
	$$

	Transformovaná trajektorie HH modelu má tendenci být \uv{\textit{skoro všude}} konstantní v průměru nalezené trajektorie FHN modelu bez silné penalizace. Z tohoto důvodu můžeme zkusit tuto \uv{\textit{skoro konstantu}} zešikmit pomocí exponenciály, tj.
	\begin{equation*}
		\vvb \Theta_e(\vv H; \vv q) = \mtr{
			\vdots	& \vdots \\	
			\vv H_{i, \cdot}	& e^{\scal{\vv H_{i, \cdot}}{\vv q}} \\
			\vdots	& \vdots
		}.
	\end{equation*}
\end{frame}

\begin{frame}{Transformace}{Alternativní transformace -- pokrač.}
	\begin{figure}[H]
		\centering
		\begin{subfigure}[t]{0.32\textwidth}
			\centering
			\tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/linear_transformation_comparison/sigmoid_infinite_train_α=50.0_β=1.0_μ=0.1_pres.pdf}{width=\textwidth}
			\label{fig:sigmoid_transform_comparison_inf}
		\end{subfigure}
		\begin{subfigure}[t]{0.32\textwidth}
			\centering
			\tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/linear_transformation_phase_space/sigmoid_infinite_train_α=50.0_β=1.0_μ=0.1_pres.pdf}{width=\textwidth}
			\label{fig:sigmoid_phase_space:inf}
		\end{subfigure}
		\begin{subfigure}[t]{0.32\textwidth}
			\centering
			\tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/linear_transformation_comparison/sigmoid_finite_train_α=1.0_β=1.0_μ=0.1_pres.pdf}{width=\textwidth}
			\label{fig:sigmoid_transform_comparison_fin}
		\end{subfigure}
		\begin{subfigure}[t]{0.32\textwidth}
			\centering
			\tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/linear_transformation_comparison/exponential_infinite_train_α=50.0_β=10.0_μ=0.1_pres.pdf}{width=\textwidth}
			\label{fig:exp_transform_comparison_inf}
		\end{subfigure}
		\begin{subfigure}[t]{0.32\textwidth}
			\centering
			\tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/linear_transformation_phase_space/exponential_infinite_train_α=50.0_β=10.0_μ=0.1_pres.pdf}{width=\textwidth}
			\label{fig:exp_phase_space:inf}
		\end{subfigure}
		\begin{subfigure}[t]{0.32\textwidth}
			\centering
			\tryshow{../experiments/Hodgkin-Huxley-to-FitzHugh-Nagumo/figures/linear_transformation_comparison/exponential_finite_train_α=1.0_β=10.0_μ=0.1_pres.pdf}{width=\textwidth}
			\label{fig:exp_transform_comparison_fin}
		\end{subfigure}
		\label{fig:alt_phase_space}
	\end{figure}
\end{frame}

\begin{frame}[plain]
\vfill
\centerline{Děkuji vám za pozornost!}
\vfill
\end{frame}

\section[Dotazy z posudku]{Dotazy z posudku}

\begin{frame}{Dotazy z posudku}{Značení, metody odstraňování šumu}
	\alert{Značení} -- Pro proměnné (a jejich derivace) jsem použil v SINDy metodě stejné značení jako u příslušného dynamického systému. Takto je jednoduší posléze porovnávat nalezený a původní model. \\

	\alert{Metody odstraňování šumu} -- Použitá knihovna \href{https://docs.sciml.ai/DataDrivenDiffEq/stable/libs/datadrivensparse/sparse_regression/}{DataDrivenSparse.jl} (resp. \href{https://docs.sciml.ai/DataDrivenDiffEq/stable/}{DataDrivenDiffEq.jl}) implementuje tzv. \textit{collocation techniques}, čímž označuje metody jádrového vyhlazování a interpolace. Pomocí těchto metod lze taktéž vyhladit data a odhadnout derivace. Z mého testování však v práci popsaná metoda dávala lepší výsledky.
\end{frame}

\begin{frame}{Dotazy z posudku}{Koeficient determinace}
	\alert{Koeficient determinace} -- Koeficient determinace 
	$$
		R^2 = 1 - \frac {RSS} {TSS} = 1 - \frac {\sum_{i = 1}^k \norm{\vv x_i - \hat{\vv x}_i}^2_2} {\sum_{i = 1}^k \norm{\vv x_i - \overline{\vv x}}^2_2}
	$$
	udává jakou část variability (rozptylu) původních dat model vystihuje. V případě, že má model větší rozptyl, než původní data (což se u standardního lineárního modelu stát nemůže), bude koeficient determinace záporný, tj. $R^2 < 0$. Jinak řečeno tento model aproximuje data hůře než průměr.
\end{frame}

\begin{frame}{Dotazy z posudku}{Schodovitý tvar odhadu derivace}
	\alert{Schodovitý tvar odhadu derivace} -- Odstranění šumu pomocí totální variace původně pochází ze zpracování signálů. Přesněji je to metoda, jak zbavit \alert{po částech konstatní} signál šumu. To můžeme vidět i na minimalizované funkci
	$$
		\min_{\vv x \in \R^n} \eta \underbrace{\sum_{i = 1}^{n-1} \absval{x_{i+1} - x_i}}_{V(\vv x)} + \frac 1 2 \norm{\vv x - \vv y}_2^2,
	$$
	kde $\vv y \in \R^n$ jsou zašuměná data. Zde jsou přes funkci $V(\vv x)$ penalizovány změny (\uv{skoky}) odšuměných dat $\vv x$, což zapříčiňuje po částech konstantní výsledný odhad.
\end{frame}

\makeoutro

\end{document}