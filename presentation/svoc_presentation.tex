\documentclass[
]{beamer}

\usepackage[no-math]{fontspec}
\defaultfontfeatures{Mapping=tex-text}
\usepackage[czech]{babel}
\usepackage{csquotes}
\usepackage{expl3,biblatex}
\addbibresource{thesis.bib}
\usepackage{booktabs}
\usetheme[
  workplace=sci,
]{MU}

\newcommand*{\preambule}{preambule.tex}
%INCLUDE[[\renewcommand*{\preambule}{preambule_compiled.tex}]]END%
\input{\preambule}

\title[Dynamické systémy řízené daty]{Dynamické systémy řízené daty}
\subtitle[Řídká Identifikace Nelineární Dynamiky (SINDy)]{Řídká Identifikace Nelineární Dynamiky (SINDy)}
\author[Š.\,Zapadlo]{Štěpán Zapadlo\texorpdfstring{\\}{, }505579@mail.muni.cz}
\institute[PřF MU]{Přírodovědecká fakulta Masarykovy univerzity}
\date{\today}
\subject{Dynamické systémy řízené daty}
\keywords{dynamické systémy, strojové učení, SINDy, matematická optimalizace, řídká regrese}

\begin{document}

\begin{frame}[plain]
\maketitle
\end{frame}

\section[Úvod]{Úvod}
\subsection[Motivace]{Motivace}

\begin{frame}{Motivace}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\includegraphics[width=\textwidth]{fig/neuron_motivation.png}
		\end{column}

		\begin{column}{0.5\textwidth}
			{
				\huge\boldmath
				$\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont $?$}}}{\longrightarrow}}\quad\dot{\vv{x}} = \vv f (\vv x)$
			}
		\end{column}
	\end{columns}
\end{frame}

\subsection[Výsledky]{Výsledky}

\begin{frame}{Výsledky}
	\begin{enumerate}
		\item implementace SINDy v jazyce \Julia
		\item formulace SINDy pomocí řídké regrese
		\item odstranění šumu
		\item numerický odhad derivace (robustní vůči šumu)
		\item nová optimalizační metoda DSTLS
		\item chování metody SINDy na zašuměných datech
		\item srovnání optimalizačních metod
		\item simulační studie
		\begin{enumerate}
			\item FitzHughův--Nagumův model
			\item Lorenzův model
		\end{enumerate}
	\end{enumerate}
\end{frame}

\section[Základní pojmy]{Základní pojmy}
\subsection[Julia]{Julia}

\begin{frame}{Julia}{Představení}
	\Julia je:
	\begin{itemize}
		\item open-source a zaměřená na vědecké programování
		\item vysokoúrovňová, dynamicky typovaná a JIT kompilovaná
		\item \alert{\href{https://julialang.org/benchmarks/}{rychlá}}
		\item elegantní a dobře čitelná
		\item aktivní komunita a velký ekosystém balíčků
		\item správa balíčků integrovaná do jazyka samotného
	\end{itemize}
\end{frame}

\subsection[Řídká regrese]{Řídká regrese}

\begin{frame}{Řídká regrese}{Lineární regresní model}
	Lineární regresní model má tvar
	\scriptsize
	\begin{equation}\label{eq:linear_model}
		\underbrace{\bmtr{
			y_1 \\
			\vdots \\
			y_n
		}}_{\vv Y} = \underbrace{\bmtr{
			x_{1,1} & x_{1,2} & \dots & x_{1, k} \\
			\vdots & \vdots & \ddots & \vdots \\
			x_{n,1} & x_{n,2} & \dots & x_{n, k}
		}}_{\vvb X}
		\underbrace{\bmtr{
			\xi_1 \\
			\vdots \\
			\xi_k
		}}_{\vvb \xi}
		+ \underbrace{\bmtr{
			\ve_1 \\
			\vdots \\
			\ve_n
		}}_{\vvb \ve},
	\end{equation}
	\normalsize
	přičemž se pokoušíme najít \uv{nejvhodnější} $\vvb \xi$. V našem případě budeme často uvažovat obecnější variantu
	\scriptsize
	\begin{multline*}
		\underbrace{\bmtr{
			\dot x_{1,1} & \dots & \dot x_{1, l} \\
			\vdots & \ddots & \vdots \\
			\dot x_{n,1} & \dots & \dot x_{n, l}
		}}_{\vvb{\dot{X}}} 
		= 
		\underbrace{\bmtr{
			x_{1,1} & \dots & x_{1, k} & f_1(\vv x_{1, \cdot}) & \dots & f_r(\vv x_{1, \cdot}) \\
			\vdots & \ddots & \vdots & \vdots & \ddots & \vdots \\
			x_{n,1} & \dots & x_{n, k} & f_1(\vv x_{n, \cdot}) & \dots & f_r(x_{n, \cdot})
		}}_{\vvb \Theta (\vv X)} \cdot \\
		\cdot
		\underbrace{\bmtr{
			\xi_{1,1} & \dots & \xi_{1, l}\\
			\vdots  & \ddots & \vdots \\
			\xi_{p, 1} & \dots & \xi_{p, l}
		}}_{\vvb \Xi}
		+ \underbrace{\bmtr{
			\vvb \ve_1^T \\
			\vdots \\
			\vvb \ve_n^T
		}}_{\vvb \epsilon}
	\end{multline*}
	\begin{equation}\label{eq:sindy_model}
		\vvb{\dot{X}} = \vvb \Theta (\vv X) \vvb \Xi + \vvb \epsilon
	\end{equation}
\end{frame}

\begin{frame}{Řídká regrese}{Řešení lineární regrese metodou nejmenších čtverců}
	Vhodný lineární model úlohy \eqref{eq:linear_model} je řešení optimalizačního problému
	\begin{equation}\label{eq:ols_problem}
		\hat{\vvb \xi} = \argmin_{\vvb \xi} \norm{\vv Y - \vv X \vvb \xi}_2^2 = \brackets{\vv X^T \vv X}^{-1} (\vv X^T \vv Y)
	\end{equation}
	a schematicky
	$$
		\begin{bNiceMatrix}[first-row]
			\dot x_1 & \dot x_2 & \dot x_3 \\
			\tikzmarkin[ver=style orange]{lin mod deriv 1} & \tikzmarkin[ver=style green]{lin mod deriv 2} & \tikzmarkin[ver=style cyan]{lin mod deriv 3}\\
			& & \\
			& & \\
			& & \\
			& & \\
			& & \\
			& & \\
			& & \\
			\tikzmarkend{lin mod deriv 1} & \tikzmarkend{lin mod deriv 2}& \tikzmarkend{lin mod deriv 3} \\
		\end{bNiceMatrix} =
		\begin{bNiceMatrix}[first-row]
			x_1 & x_2 & x_3 & x_1^2 & x_2^2 & x_3^2 \\
			\tikzmarkin[ver=style combination]{lin mod x 1} & \tikzmarkin[ver=style combination]{lin mod x 2} & \tikzmarkin[ver=style combination]{lin mod x 3} & \tikzmarkin[ver=style combination]{lin mod x2 1} & \tikzmarkin[ver=style combination]{lin mod x2 2} & \tikzmarkin[ver=style combination]{lin mod x2 3}\\
			& & & & & \\
			& & & & & \\
			& & & & & \\
			& & & & & \\
			& & & & & \\
			& & & & & \\
			& & & & & \\
			\tikzmarkend{lin mod x 1} & \tikzmarkend{lin mod x 2}& \tikzmarkend{lin mod x 3} & \tikzmarkend{lin mod x2 1} & \tikzmarkend{lin mod x2 2}& \tikzmarkend{lin mod x2 3} \\
		\end{bNiceMatrix}
		\begin{bNiceMatrix}[first-row]
			\xi_1 & \xi_2 & \xi_3 \\
			\tikzmarkin[ver=style gray]{lin mod xi 1} \Circ{Orange!90}  & \tikzmarkin[ver=style gray]{lin mod xi 2} \Circ{green!90} & \tikzmarkin[ver=style gray]{lin mod xi 3} \Circ{Cyan!90}\\
			\Circ{Orange!90} & \Circ{Green!90} & \Circ{Cyan!90} \\
			\Circ{Orange!90} & \Circ{Green!90} & \Circ{Cyan!90} \\
			\Circ{Orange!90} & \Circ{Green!90} & \Circ{Cyan!90} \\
			\Circ{Orange!90} & \Circ{Green!90} & \Circ{Cyan!90} \\
			\Circ{Orange!90} \tikzmarkend{lin mod xi 1} & \Circ{green!90} \tikzmarkend{lin mod xi 2} & \Circ{Cyan!90} \tikzmarkend{lin mod xi 3} \\
		\end{bNiceMatrix}
		+ \vvb \epsilon
	$$
\end{frame}

\begin{frame}{Řídká regrese}{Řešení řídké regrese -- schematicky}
	$$
		\begin{bNiceMatrix}[first-row]
			\dot x_1 & \dot x_2 & \dot x_3 \\
			\tikzmarkin[ver=style orange]{sindy mod deriv 1} & \tikzmarkin[ver=style green]{sindy mod deriv 2} & \tikzmarkin[ver=style cyan]{sindy mod deriv 3}\\
			& & \\
			& & \\
			& & \\
			& & \\
			& & \\
			& & \\
			& & \\
			\tikzmarkend{sindy mod deriv 1} & \tikzmarkend{sindy mod deriv 2}& \tikzmarkend{sindy mod deriv 3} \\
		\end{bNiceMatrix} =
		\begin{bNiceMatrix}[first-row]
			x_1 & x_2 & x_3 & x_1^2 & x_2^2 & x_3^2 \\
			\tikzmarkin[ver=style cyanL]{sindy mod x 1} & \tikzmarkin[ver=style orangeL]{sindy mod x 2} & \tikzmarkin[ver=style gray]{sindy mod x 3} & \tikzmarkin[ver=style greenL]{sindy mod x2 1} & \tikzmarkin[ver=style greencyancomb]{sindy mod x2 2} & \tikzmarkin[ver=style orangeL]{sindy mod x2 3}\\
			& & & & & \\
			& & & & & \\
			& & & & & \\
			& & & & & \\
			& & & & & \\
			& & & & & \\
			& & & & & \\
			\tikzmarkend{sindy mod x 1} & \tikzmarkend{sindy mod x 2}& \tikzmarkend{sindy mod x 3} & \tikzmarkend{sindy mod x2 1} & \tikzmarkend{sindy mod x2 2}& \tikzmarkend{sindy mod x2 3} \\
		\end{bNiceMatrix}
		\begin{bNiceMatrix}[first-row]
			\xi_1 & \xi_2 & \xi_3 \\
			\tikzmarkin[ver=style gray]{sindy mod xi 1} & \tikzmarkin[ver=style gray]{sindy mod xi 2} & \tikzmarkin[ver=style gray]{sindy mod xi 3} \Circ{Cyan!90}\\
			\Circ{Orange!90} & & \\
			& &  \\
			& \Circ{Green!90} & \\
			& \Circ{Green!90} & \Circ{Cyan!90} \\
			\Circ{Orange!90} \tikzmarkend{sindy mod xi 1} & \tikzmarkend{sindy mod xi 2} & \tikzmarkend{sindy mod xi 3} \\
		\end{bNiceMatrix}
		+ \vvb \epsilon
	$$
\end{frame}

\begin{frame}{Řídká regrese}{LASSO regrese}
	Do účelové funkce OLS metody \eqref{eq:ols_problem} přidáme člen vynucující řídkost vektoru koeficientů
	\begin{equation}\label{eq:lasso_problem}
		\min_{\vvb \xi} \frac 1 2 \norm{\vv Y - \vv X \vvb \xi}_2^2 + \mu \norm{\vvb \xi}_1.
	\end{equation}

	LASSO regrese \alert{nemá} v obecné formě \alert{analytické řešení}, proto jej řešíme iterativní metodou založenou např. na proximálním operátoru. 
	
	Místo problému \eqref{eq:lasso_problem} uvažujme
	\begin{equation}\label{eq:admm_lasso_problem}
		\begin{gathered}
			\min_{\vvb \xi, \vv z} \brackets{\frac 1 2 \norm{\vv Y - \vv X \vvb \xi}_2^2 + \mu \norm{\vv z}_1} \\
			\text{za podmínky } \vvb \xi - \vv z = 0,
		\end{gathered}
	\end{equation}
	přičemž úloha \eqref{eq:admm_lasso_problem} je řešitelná \textit{ADMM} algoritmem.
\end{frame}

\begin{frame}{Řídká regrese}{STLS a DSTLS}
	Optimalizační problém STLS metody
	\begin{equation*}
		\begin{gathered}
			\min_{\vvb \xi} \frac 1 2 \norm{\vv Y - \vv X \vvb \xi}_2^2 \\
			\text{za podmínky } \forall j \in \set{1, \dots, n}: \quad \absval{\xi_j} > \tau.
		\end{gathered}
	\end{equation*}

	\vfill\vfill

	Optimalizační problém nově navržené DSTLS metody
	\begin{equation*}
		\begin{gathered}
			\min_{\vvb \xi} \frac 1 2 \norm{\vv Y - \vv X \vvb \xi}_2^2 \\
			\text{za podmínky } \forall j \in \set{1, \dots, n}: \; \absval{\xi_j} > \tau \cdot \max \set{\absval{\xi} : \xi \in \vvb \xi}.
		\end{gathered}
	\end{equation*}
\end{frame}

\subsection[Šum a jeho odstraňování]{Šum a jeho odstraňování}

\begin{frame}{Odstraňování šumu}{Totální variace}
	Nechť $\vv y$ jsou původní data s šumem. Pak odstranění šumu \textit{metodou totální variace} je nalezení vektoru $\vv x$ takového, že dobře aproximuje původní data, ale má menší \alert{totální variaci} $V(\vv x)$. Tedy se jedná o úlohu
	\begin{equation*}
		\min_{\vv x \in \R^n} \eta \underbrace{\sum_{i = 1}^{n-1} \absval{x_{i+1} - x_i}}_{V(\vv x)} + \frac 1 2 \norm{\vv x - \vv y}_2^2,
	\end{equation*}
	kde $\eta$ je parametr regularizace.
\end{frame}

\begin{frame}[fragile=singleslide]{Odstraňování šumu}{Totální variace}
	%INSERT[juliacode]{document/snippets/total_variation_denoise.jl}END%

	\begin{figure}
		\centering
		\SaveAndPlot{fig}{total-variation-denoise}{figure}{width=0.9\\textwidth}
		\caption{Ukázka odstranění šumu pomocí metody totální variace}
		\label{fig:total_variation_denoise}
	\end{figure}
\end{frame}

\begin{frame}{Odstraňování šumu}{Numerické derivování regularizované totální variací}
	Nalezení \uv{čisté} derivace z dat ovlivněných šumem můžeme matematicky vyjádřit jako řešení úlohy
	\begin{equation*}
		\min_{u} \eta R(u) + DF(A(u) - f),
	\end{equation*}
	kde $f$ je funkce, jejíž derivaci hledáme, $R$ regularizační člen, $A$ operátor antidiference a $DF$ člen zachycující věrohodnost k datům. Pro diskrétní případ dat $\vv f$ funkce $f$ řešíme
	\begin{equation*}
		\min_{\vv u \in \R^n} \eta V(\vv u) + \frac 1 2 \norm{A_d(\vv u) - \vv f}_2^2,
	\end{equation*}
	kde $A_d(\vv u)$ aproximuje integrál funkce $u(x)$ na bodech $\vv u$.
\end{frame}

\begin{frame}[fragile=singleslide]{Odstraňování šumu}{Numerické derivování regularizované totální variací}
	%INSERT[juliacode]{document/snippets/tvdiff_denoise.jl}END%

	\begin{figure}
		\centering
		\SaveAndPlot{fig}{tvdiff-denoise}{figure}{width=0.9\\textwidth}
		\caption{Ukázka numerického derivování regularizovaného metodou totální variace}
		\label{fig:tvdiff_denoise}
	\end{figure}
\end{frame}

\section[SINDy]{Řídká identifikace nelineární dynamicky (SINDy)}
\subsection[Teoretická formulace]{Teoretická formulace}

\begin{frame}{SINDy}
	Zkoumaný dynamický systém hledáme ve tvaru
	\begin{equation*}
		\dot{\vv{x}} = \vv f (\vv x).
	\end{equation*}
	Můžeme jej aproximovat pomocí kandidátních funkcí $\vvb \Theta(\vv x)$ jako
	\begin{equation*}
		\vv f(\vv x) \approx \sum_{i = 1}^p \vvb \theta_i(\vv x) \xi_i = \vvb \Theta(\vv x) \vvb \xi,
	\end{equation*}
	což interpretujeme jako \textit{zobecněný lineární model}, ve kterém se snažíme minimalizovat počet nenulových členů v $\vvb \xi$, kde $\vvb \theta_i : \R^n \to \R^n$ a také
	\begin{equation*}
		\vvb \Theta(\vv x) = \mtr{
			\vvb \theta_1(\vv x) \\
			\vdots \\
			\vvb \theta_p(\vv x)
		}^T, \qquad
		\vvb \xi = \mtr{
			\xi_1 \\
			\vdots \\
			\xi_p
		}.
	\end{equation*}
\end{frame}

\begin{frame}{SINDy}{Data}
	Mějme data z časové řady příslušné hledanému dynamickému systému
	\begin{equation*}
		\vv X = \bmtr{\vv x(t_1) & \vv x(t_2) & \dots & \vv x(t_n)}^T
	\end{equation*}
	a derivace
	\begin{equation*}
		\vvb{\dot X} = \bmtr{\dot{\vv{x}}(t_1) & \dot{\vv{x}}(t_2) & \dots & \dot{\vv{x}}(t_n)}^T.
	\end{equation*}

	Potom vytvoříme knihovnu kandidátů 
	\begin{equation*}
		\vvb \Theta(\vv X) = \bmtr{\vvb 1 & \vv X & P_2(\vv X) & \dots & P_d(\vv X) & \dots & \sin (\vv X) & \dots & \vv f(\vv X)},
	\end{equation*}

	a celkem dostáváme \textit{zobecněný lineární model}
	\begin{equation*}
		\vvb{\dot{X}} = \vvb \Theta (\vv X) \vvb \Xi + \vvb \epsilon.
	\end{equation*}
\end{frame}

\begin{frame}{SINDy}{Optimalizační problém}
	\begin{equation*}
		\min_{\vvb \Xi} \frac 1 2 \norm{\vvb{\dot{X}} - \vvb \Theta(\vv X) \vvb \Xi}^2_2 + \mu R(\vvb \Xi),
	\end{equation*}
	\hfill{}kde $R(\cdot)$ je regularizující funkce vynucující řídkost\hfill{}
\end{frame}

\subsection[Implementace]{Implementace}

\begin{frame}[fragile=singleslide]{Implementace}{FitzHughův--Nagumův model}
	Ukažme si správnost implementace na FitzHughově--Nagumově zjednodušeném modelu neuronu
	\begin{align*}
		\dot V &= 0.8 - W + V - \frac 1 3 V^3, \\
		\dot W &= 0.056 - 0.064 \cdot W + 0.08 \cdot V.
	\end{align*}

	%INSERT[juliaconcode]{experiments/FitzHugh-Nagumo-Custom/generator.jl}END%

	Prvně určíme knihovnu a optimalizační metodu, pak spustíme SINDy
	\begin{juliaconsole}
basis = PolynomialLibrary(3, 2);
optimizer = STLS();
Ξ = discover(X, Ẋ, basis, optimizer; max_iter=100);
prettyprint(Ξ, basis; vars=["V" "W"])
	\end{juliaconsole}
\end{frame}

\subsection[Chování SINDy]{Chování SINDy metody}

\begin{frame}{Chování SINDy na Lorenzově modelu}{Počet snímků a jejich frekvence}
	\begin{figure}
		\centering
		\tryshow{../experiments/3D-Lorenz-Sampling_Rate/figures/sampling_rate_and_duration/[-20, -20, 25]_30.0_10_5_0.01_0.1:0.05:2.5_4.0:0.25:15.0.pdf}{width=\textwidth}
		\caption{Vliv doby mezi jednotlivými pozorováními a počtu pozorování na chování SINDy metody na Lorenzově modelu}
		\label{fig:sindy_sampling_rate_and_duration}
	\end{figure}
\end{frame}


\begin{frame}{Chování SINDy}{Vliv šumu na SINDy na Lorenzově modelu}
	\begin{figure}
		\centering
		\tryshow{../experiments/3D-Lorenz-Noise/figures/[STLSQ(0.1), SR3(0.4), ADMM(0.1), ADMM(2.0), DSTLS(0.1)]/pres-quality-of-fit-for-sigma/k=[4, 5, 5]_noise_range=0.001:0.005:0.096_reg=[0.08, 0.9, 0.6]_trajectory_count=10_AdditiveVarianceNoise_λ=[0.3, 0.3, 0.3].pdf}{height=0.7\textheight}
		\caption{Vliv velikosti rozptylu šumu na chování SINDy metody na Lorenzově modelu}
		\label{fig:sindy_lorenz_noise_comparison}
	\end{figure}
\end{frame}

\begin{frame}{Chování SINDy}{Vliv šumu na SINDy na FitzHughově--Nagumově modelu}
	\begin{figure}
		\centering
		\tryshow{../experiments/FitzHugh-Nagumo-Noise/figures/[STLSQ(0.1), SR3(0.03), ADMM(0.025), DSTLS(0.1)]/pres-quality-of-fit-for-sigma/k=[4, 5]_noise_range=0.001:0.005:0.096_reg=[0.08, 0.9]_trajectory_count=10_AdditiveVarianceNoise_λ=[0.3, 0.3].pdf}{height=0.7\textheight}
		\caption{Vliv velikosti rozptylu šumu na chování SINDy metody na FitzHughově--Nagumově modelu}
		\label{fig:sindy_fhn_noise_comparison}
	\end{figure}
\end{frame}

\section[Simulační studie]{Simulační studie}

\begin{frame}{Simulační studie}{Zkoumané modely}
	\begin{columns}[t]
		\begin{column}{0.475\textwidth}
			FitzHughův--Nagumův model neuronu má tvar
			\begin{align*}
				\dot V &= V - \frac {V^3} 3 - W + i_e, \\
				\dot W &= a \cdot \brackets{b \cdot V - c \cdot W + d},
			\end{align*}
			přičemž parametry volíme jako
			\begin{gather*}
				a = 0.08, b = 1, c = 0.8, d = 0.7,\\
				i_e = 0.8.
			\end{gather*}
		\end{column}%
		\begin{column}{0.475\textwidth}
			Lorenzův zjednodušený model konvekčního proudění v atmosféře má tvar
			\begin{equation*}
				\begin{aligned}
					\dot x &= \sigma (y - x), \\
					\dot y &= x (\rho - z) - y, \\
					\dot z &= x y - \beta z,
				\end{aligned}
			\end{equation*}
			přičemž parametry volíme jako
			\begin{equation*}
				\sigma = 10, \quad \rho = 28, \quad \beta = \frac 8 3.
			\end{equation*}
		\end{column}
	\end{columns}
\end{frame}

\subsection[FitzHughův--Nagumův model]{FitzHughův--Nagumův model}

\begin{frame}{FitzHughův--Nagumův model}{Použitá trajektorie a derivace}
	\begin{figure}[htb]
		\centering
		 \begin{subfigure}[c]{0.49\textwidth}
			\centering
			\tryshow{../experiments/FitzHugh-Nagumo/figures/pres-true-and-noisy-trajectory/k=[4, 5]_AdditiveNoise(0, 0.1)_denoised_trajectory_λ=[0.3, 0.3].pdf}{width=0.95\textwidth}
			\caption{Srovnání trajektorií po procesu odstranění šumu}
			\label{fig:fhn_denoise_trajectory}
		 \end{subfigure}
		 \hfill
		 \begin{subfigure}[c]{0.49\textwidth}
			\centering
			\tryshow{../experiments/FitzHugh-Nagumo/figures/tvdiff-and-true-derivative/AdditiveNoise(0.0, 0.1)_denoised_trajectory_regularization:[0.08, 0.9].pdf}{width=0.95\textwidth}
			\caption{Srovnání skutečné derivace a derivace získané ze zašuměných dat}
			\label{fig:fhn_tvdiff}
		 \end{subfigure}
		 \caption{Diagnostické grafy naučených modelů}
	\end{figure}
\end{frame}

\begin{frame}{FitzHughův--Nagumův model}{SINDy}
	Pro SINDy algoritmus použijeme v tomto případě metodu \alert{DSTLS} s $\tau = 0.15$ a \alert{polynomiální knihovnu nejvýše 4. stupně}.

 	Nalezený model má tvar
 	\begin{align*}
		%INSERTONLY{experiments/FitzHugh-Nagumo/figures/tex-result/k:[4, 5]_DSTLS(0.15)_reg:[0.08, 0.9]_tvdiff_df_polynomial_basis(variables, 4)_AdditiveNoise(0.0, 0.1)_denoised_trajectory_λ:[0.3, 0.3].txt}END%
		,
	\end{align*}
	což můžeme srovnat s původním modelem
	\begin{align*}
		\frac{\d}{\d t} V &= 0.8 + V - W - \frac 1 3 V^3, \\
		\frac{\d}{\d t} W &= 0.056 + 0.08 \cdot V - 0.064 \cdot W.
	\end{align*}
\end{frame}

\begin{frame}{FitzHughův--Nagumův model}{Diagnostické grafy}
	\begin{figure}
		\centering
		\tryshow{../experiments/FitzHugh-Nagumo/figures/pres-simple-fitted-comparison/k=[4, 5]_DSTLS(0.15)_reg=[0.08, 0.9]_tvdiff_df_polynomial_basis(variables, 4)_AdditiveNoise(0, 0.1)_denoised_trajectory_λ=[0.3, 0.3].pdf}{height=0.7\textheight}
		\caption{Diagnostické grafy naučeného modelu}
		\label{fig:fhn_fitted_summary}
	\end{figure}
\end{frame}

\begin{frame}{FitzHughův--Nagumův model}{Predikovaná trajektorie}
	\begin{figure}
		\centering
		\tryshow{../experiments/FitzHugh-Nagumo/figures/predicted-trajectory-comparison/k:[4, 5]_DSTLS(0.15)_reg:[0.08, 0.9]_tvdiff_df_polynomial_basis(variables, 4)_AdditiveNoise(0.0, 0.1)_denoised_trajectory_λ:[0.3, 0.3].pdf}{height=0.65\textheight}
		\caption{Srovnání původní a nově simulované trajektorie}
		\label{fig:fhn_predicted_trajectory}
	\end{figure}
\end{frame}


\subsection[Lorenzův model]{Lorenzův model}

\begin{frame}[fragile=singleslide]{Lorenzův model}{SINDy}
	\begin{columns}
		\begin{column}{0.6\textwidth}
			Pro SINDy algoritmus použijeme \alert{polynomiální knihovnu nejvýše 3. stupně}, metodu \alert{DSTLS} s $\tau = 0.02$ a srovnáme ji s metodou \alert{STLS} s $\tau = 0.4$.
		\end{column}
		\begin{column}{0.35\textwidth}
			Lorenzův model:
			\scriptsize
			\begin{align*}
				\frac{\d}{\d t} x &= 10 \cdot y - 10 \cdot x, \\
				\frac{\d}{\d t} y &=  - y + 28 \cdot x - xz, \\
				\frac{\d}{\d t} z &= xy - \frac 8 3 \cdot z
			\end{align*}
		\end{column}
	\end{columns}

	\hfill

 	Nalezené modely mají tvar: \\
	\begin{columns}
		\begin{column}{0.46\textwidth}
		   	STLS:
			\scriptsize
			\begin{align*}
				%INSERTONLY{experiments/3D-Lorenz-Simple/figures/polynomial_basis(variables, 3)/tex-result/k:[4, 5, 5]_STLSQ(0.4)_reg:[0.08, 0.9, 0.6]_tvdiff_df_AdditiveNoise(0.0, 1.0)_denoised_trajectory_λ:[0.3, 0.3, 0.3].txt}END%
			\end{align*}
		\end{column}
		\begin{column}{0.46\textwidth}
			DSTLS:
			\scriptsize
			\begin{align*}
				%INSERTONLY{experiments/3D-Lorenz-Simple/figures/polynomial_basis(variables, 3)/tex-result/k:[4, 5, 5]_DSTLS(0.02)_reg:[0.08, 0.9, 0.6]_tvdiff_df_AdditiveNoise(0.0, 1.0)_denoised_trajectory_λ:[0.3, 0.3, 0.3].txt}END%
			\end{align*}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Lorenzův model}{Atraktory}
	\begin{figure}[htb]
		\centering
		 \begin{subfigure}[t]{0.49\textwidth}
			\centering
			\tryshow{../experiments/3D-Lorenz-Simple/figures/polynomial_basis(variables, 3)/pres-attractor-plot/k=[4, 5, 5]_STLSQ(0.4)_reg=[0.08, 0.9, 0.6]_tvdiff_df_AdditiveNoise(0.0, 1.0)_denoised_trajectory_λ=[0.3, 0.3, 0.3].pdf}{width=0.9\textwidth}
			\caption{Srovnání atraktoru pro původní a nalezený systém pro STLS}
			\label{fig:lorenz_attractor_stls}
		 \end{subfigure}
		 \hfill
		 \begin{subfigure}[t]{0.49\textwidth}
			\centering
			\tryshow{../experiments/3D-Lorenz-Simple/figures/polynomial_basis(variables, 3)/pres-attractor-plot/k=[4, 5, 5]_DSTLS(0.02)_reg=[0.08, 0.9, 0.6]_tvdiff_df_AdditiveNoise(0.0, 1.0)_denoised_trajectory_λ=[0.3, 0.3, 0.3].pdf}{width=0.9\textwidth}
			\caption{Srovnání atraktoru pro původní a nalezený systém pro DSTLS}
			\label{fig:lorenz_attractor_dstls}
		 \end{subfigure}
		 \caption{Atraktory trajektorií naučených modelů}
		 \label{fig:lorenz_attractors_all}
	\end{figure}
\end{frame}

\begin{frame}[plain]
\vfill
\centerline{Děkuji vám za pozornost!}
\vfill
\end{frame}

\makeoutro

\end{document}
