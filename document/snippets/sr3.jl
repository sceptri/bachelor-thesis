#>--OPTIONAL
# Do NOT run this code - some functions here DO NOT EXIST
#>--END
function SR3(X, Ẋ, Θ; ϵ, W, R, ν, μ)
	k = 0
	ε = 2 * ϵ
	Ξ = similar(W)
	while ε > ϵ
		k += 1
		# Find the minimizing Ξₖ (usually via OLS or similar)
		Ξₖ = argmin(1/2 * norm(Ẋ - Θ(X) * Ξ)^2 + 1/(2*ν) * norm(Ξ - W)^2 for Ξ in ℝ(size(W)))
		# Update the auxiliary variable
		Wₖ = prox(Ξₖ, μ*ν*R)
		# Recalculate the error ε
		ε = norm(Wₖ - W) / ν
		W = Wₖ; Ξ = Ξₖ
	end
	return Ξ
end