a = [x^2 for x in 1:5] # list comprehension
b = (x^2 for x in 1:5) # generator syntax
collect(b)