using CairoMakie, LaTeXStrings, DataDrivenDynamics

x = range(-7, 7; length=500)

resolution = (540, 150)
labels = (
    :xlabel => L"x",
    :ylabel => L"y"
)
fig = Figure(; resolution)


# logistic function, see https://en.wikipedia.org/wiki/Logistic_function
sigmoid(x) = 1 / (1 + 2 * exp(- (x - 0.5)))
y = sigmoid.(x)

ax = Axis(fig[1, 1];
    labels...
)
lines!(ax, x, y; 
	color=fetch_colors(:Nizami, 1; palette=false)[1]
)