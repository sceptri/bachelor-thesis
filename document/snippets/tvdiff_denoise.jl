using CairoMakie, LaTeXStrings, Random
using DataDrivenDynamics, DataDrivenDynamics.Noiser
Random.seed!(12345)

colors = fetch_colors(:Nizami, 3; palette=false)
resolution = (550, 280)

x_range = range(-10, 10, length=1000)
F(x) = sin(x) + abs(x)
df(x) = cos(x) + sign(x)
y_values = F.(x_range)
df_values = df.(x_range)

μₐ, σₐ = 0, 0.2
basic_additive_noise = AdditiveNoise(μₐ, σₐ)
noisy_y = basic_additive_noise(y_values)
denoised_df = tvdiff(noisy_y', 30, 0.55; dx = 20/1000, scale="large", ε =1e-10)

figure = Figure(; resolution)
axis = Axis(figure[2, 1]; xlabel=L"x", ylabel=L"y")
noisy_lines = lines!(axis, x_range, noisy_y; color=colors[1])

axis = Axis(figure[2, 2]; xlabel=L"x", ylabel=L"y")
true_lines = lines!(axis, x_range, df_values; color=colors[2])

axis = Axis(figure[2, 3]; xlabel=L"x", ylabel=L"y")
denoised_lines = lines!(axis, x_range, [denoised_df...]; color=colors[3])
legend = Legend(figure[1, :], [
		noisy_lines, 
		true_lines, 
		denoised_lines
	], [
		L"\text{noisy } \sin(x) + |x|", 
		L"\frac {\mathrm{d}}{\mathrm{d}x} \left(\sin (x) + |x|\right)", 
		L"\text{denoised derivative}"
	]; orientation=:horizontal)