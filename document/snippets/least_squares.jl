using CairoMakie, LinearAlgebra, Random, LaTeXStrings
Random.seed!(12345)

x_points = 1:10
y_points = x_points + 4 * (rand(10) .- 0.5)

ξ = (x_points' * x_points) \ (x_points' * y_points)

resolution = (300, 300)
labels = (
    :xlabel => L"x",
    :ylabel => L"y"
)

colors = cgrad(:Nizami, range(0.0, 1.0; length = 4); categorical = true, rev = true)
fig = Figure(; resolution)
ax = Axis(fig[1, 1]; labels..., palette = (color = colors, patchcolor = colors))

for i in 1:10
	side = ξ * x_points[i] - y_points[i]
    above = side > 0 ? 1 : -1
    side = abs(side)

    rectangle = Point2f[[x_points[i], y_points[i]], 
	 					[x_points[i] + above * side, y_points[i]], 
	 					[x_points[i] + above * side, y_points[i] + above * side], 
	 					[x_points[i], y_points[i] + above * side]]

	poly!(ax, rectangle; color = Cycled(2))
end

lines!(ax, 0:11, ξ .* (0:11); color = Cycled(3))
scatter!(ax, x_points, y_points; color=Cycled(1))
fig