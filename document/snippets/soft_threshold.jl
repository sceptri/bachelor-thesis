using CairoMakie, LaTeXStrings, DataDrivenDynamics

# Define the soft thresholding operator
S(x, λ) = max(x - λ, 0) - max(-x - λ, 0)

x = range(-6, 6; length=100)
λ = 2

resolution = (300, 250)
labels = (
    :xlabel => L"x",
    :ylabel => L"S_{\lambda}(x)"
)

fig = Figure(; resolution)
ax = Axis(fig[1, 1]; labels..., palette = fetch_colors(:Nizami, 2))

lines!(ax, x, [S(xᵢ, λ) for xᵢ in x])
fig