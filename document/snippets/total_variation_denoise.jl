using CairoMakie, LaTeXStrings, Random
using DataDrivenDynamics, DataDrivenDynamics.Noiser
Random.seed!(12345)

colors = fetch_colors(:Nizami, 3; palette=false)
resolution = (550, 280)

x_range = range(-10, 10, length=1000)
F(x) = sin(x) + abs(x)
y_values = F.(x_range)

μₐ, σₐ = 0, 0.5
basic_additive_noise = AdditiveNoise(μₐ, σₐ)
noisy_y = basic_additive_noise(y_values)
denoised_y = denoise(noisy_y', 10, 0.25)

figure = Figure(; resolution)
axis = Axis(figure[2, 1]; xlabel=L"x", ylabel=L"y")
true_lines = lines!(axis, x_range, y_values;color=colors[1])

axis = Axis(figure[2, 2]; xlabel=L"x", yticklabelsvisible=false)
noisy_lines = lines!(axis, x_range, noisy_y; color=colors[2])

axis = Axis(figure[2, 3]; xlabel=L"x", yticklabelsvisible=false)
denoised_lines = lines!(axis, x_range, [denoised_y...]; color=colors[3])


legend = Legend(figure[1, :], [
		true_lines,
		noisy_lines,
        denoised_lines
    ], [
        L"\sin (x) + |x|",
		L"\text{noisy data}",
        L"\text{denoised data}"
    ]; orientation=:horizontal)
