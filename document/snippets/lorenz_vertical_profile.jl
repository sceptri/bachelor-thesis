using CairoMakie, LaTeXStrings

x = range(0, 1; length=100)

resolution = (570, 250)
labels = (
    :xlabel => L"x",
    :ylabel => L"y"
)

fig = Figure(; resolution)

y = x
ax = Axis(fig[1, 1];
    labels...,
    title=L"\text{\textbf{Linear profile}}"
)

lines!(ax, x, y; color=y, colormap=:Nizami)

# logistic function, see https://en.wikipedia.org/wiki/Logistic_function
logistic(x) = 1 / (1 + exp(- 12 * (x - 0.5)))
y = logistic.(x)

ax = Axis(fig[1, 2];
    labels...,
    title=L"\text{\textbf{Nonlinear profile}}"
)
lines!(ax, x, y; color=y, colormap=:Nizami)
