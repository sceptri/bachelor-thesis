using CairoMakie, LinearAlgebra, Random, LaTeXStrings
import ColorSchemes

resolution = (500, 350)
labels = (
	:xlabel => L"V",
	:ylabel => L"W"
)
params = (
	:stepsize => 0.05,
	:arrow_size => 8,
	:maxsteps => 40,
	:colormap => :Nizami,
)

a = 0.08; b = 1.0; c = 0.8; d = 0.7
iₑ = 0.8

V̇(V, W) = @. V - V^3 / 3 - W + iₑ
Ẇ(V, W) = @. a * (b * V - c * W + d)

fig = Figure(; resolution)
ax = Axis(fig[1, 1]; labels..., palette = (color = cgrad(:Nizami, rev = true),))

let f(V,W) = Point2f(V̇(V, W),Ẇ(V, W))
	streamplot!(ax, f, -2.5..2.5, -1..2.5; params...)
end

converge_steps = 300
draw_steps = 1000

make_step(V, W; Δ = 0.1) = @. (V,W) + Δ * (V̇(V, W), Ẇ(V, W))

V, W = 0.0, 0.0
for _ in 1:converge_steps
	global V,W
	V, W = make_step(V, W)
end

cycle_V, cycle_W = Float64[], Float64[]
for _ in 1:draw_steps
	global V,W
	V, W = make_step(V,W)
	push!(cycle_V, V)
	push!(cycle_W, W)
end
lines!(ax, cycle_V, cycle_W; linewidth = 3, linestyle = :dash, label = L"\text{limit cycle}")
axislegend()
fig