\chapter{Řídká identifikace nelineární dynamiky (SINDy)}\label{ch:sindy}

Jak již bylo nastíněno v kapitole \ref{ch:basics}, hlavním tématem této práce jsou metody nalézání rovnic dynamických systémů z dat. V této kapitole se podíváme na  \textit{řídkou identifikaci nelineární dynamiky} (\textbf{SINDy}) (z anglického \textit{Sparse Identification of Nonlinear Dynamics}), se kterou přišel prof.\,Brunton et al. v roce 2016 \cite{BruntonSINDyCore, brunton_kutz_2019}. Tato kapitola je z velké části založena právě na jeho publikacích \cite{brunton_kutz_2019, brunton_video_basics}.

Ačkoliv zde budeme používat výhradně SINDy, patří se uvést alternativní metody \cite{Daniels2015, brunton_kutz_2019}. Mezi ně patří například \textit{symbolická regrese} \cite{Schmidt2009}, \textit{automatická inference dynamiky} \cite{Daniels2015} nebo nově i pomocí \textit{aritmetických neuronových sítí} \cite{HeimANN2020}.

I když se zde budeme zabývat pouze identifikací \textit{obyčejných diferenciálních rovnic}, SINDy algoritmus lze formulovat i pro \textit{parciální diferenciální rovnice} \cite{brunton_kutz_2019, rudy2016datadriven}.

\section{Teoretický úvod}

Předpokládejme, že zkoumaný dynamický systém
\begin{equation}\label{eq:sindy_system}
	\vv{\dot{x}} = \vv f (\vv x)
\end{equation}
má pouze malé množství \uv{aktivních členů} v prostoru všech možných členů pravé strany této rovnice. Například FitzHughův--Nagumův model \eqref{eq:fitzhugh_nagumo} obsahuje pouze lineární členy a třetí mocniny v každé rovnici. 

Jinak řečeno se snažíme aproximovat neznámou funkci $\vv f(\vv x)$ \textit{zobecněným lineárním modelem} 
\begin{equation} \label{eq:sindy_approximation}
	\vv f(\vv x) \approx \sum_{i = 1}^p \vvb \theta_i(\vv x) \xi_i = \vvb \Theta(\vv x) \vvb \xi,
\end{equation}
přičemž se snažíme minimalizovat počet nenulových členů v $\vvb \xi$, kde $p \in \N, \vvb \theta_i : \R^n \to \R^n$ a také
\begin{equation}\label{eq:sindy_theta_function}
	\vvb \Theta(\vv x) = \mtr{
		\vvb \theta_1(\vv x) \\
		\vdots \\
		\vvb \theta_p(\vv x)
	}^T, \qquad
	\vvb \xi = \mtr{
		\xi_1 \\
		\vdots \\
		\xi_p
	}.
\end{equation}

Funkce $\vvb \theta_k(\vv x)$ pro $k = 1, \dots, p$ dohromady nazýváme \textbf{knihovnou kandidátů} (či \textit{bází kandidátních prvků}). Pomocí řídké regrese \ref{ch:sparse_regression} jsme schopni najít požadované $\vvb \xi$ pro \eqref{eq:sindy_approximation} takové, že bude z většiny tvořeno nulovými členy, tj. bude \textit{řídké}.

Nechť máme data z časové řady příslušné systému \eqref{eq:sindy_system}, které uspořádáme do matice
\begin{equation*}
	\vv X = \bmtr{\vv x(t_1) & \vv x(t_2) & \dots & \vv x(t_n)}^T
\end{equation*}
a obdobně vytvoříme matici derivací
\begin{equation*}
	\vv{\dot X} = \bmtr{\vv{\dot x}(t_1) & \vv{\dot x}(t_2) & \dots & \vv{\dot x}(t_n)}^T.
\end{equation*}

V praxi se často potkáváme se situací, kdy máme matici $\vv X$ ovlivněnou šumem, a nezřídka matici $\vv{\dot X}$ vůbec neznáme a musíme ji odvodit z dat. Tyto problémy řešíme pomocí metod \textit{totální variace} představených v sekci \ref{ch:tv_denoise}, respektive v sekci \ref{ch:tvdiff} \cite{brunton_kutz_2019}.

Knihovnu kandidátních funkcí konstruujeme z dat uložených v matici $\vv X$ jako
\begin{equation}\label{eq:sindy_library}
	\vvb \Theta(\vv X) = \bmtr{\vvb 1 & \vv X & P_2(\vv X) & \dots & P_d(\vv X) & \dots & \sin (\vv X) & \dots & \vv f(\vv X)},
\end{equation}
kde $P_d(\vv X) = \mtr{p_d(\vv X_{1, \cdot}) & \dots & p_d(\vv X_{n, \cdot})}^T$, přičemž $p_d : \R^k \to \R$ značí polynom $k$-proměnných $d$-tého stupně, $\sin(\vv X)$ je myšlen po prvcích (stejně jako obdobné funkce) a nakonec $\vv f : \R^{n \times k} \to \R^n$ libovolná námi zvolená funkce -- obecně mohou být součástí knihovny kandidátů libovolné funkce, o kterých předpokládáme, že by se v systému \eqref{eq:sindy_system} mohly nacházet.

Celkem tedy SINDy aproximuje systém \eqref{eq:sindy_system} pomocí \textit{zobecněného lineárního modelu} \eqref{eq:sindy_model}
\begin{equation*}
	\vv{\dot{X}} = \vvb \Theta (\vv X) \vvb \Xi + \vvb \epsilon,
\end{equation*}
kde $i$-tý sloupec $\vvb \xi_i$ matice regresních koeficientů $\vvb \Xi$ odpovídá koeficientům aproximace $i$-té složky systému \eqref{eq:sindy_system} ve smyslu \eqref{eq:sindy_approximation}. \textit{Šetrným} modelem (z anglického \textit{parsimonious model}) rozumíme takový model, který bude dobře aproximovat původní systém a zároveň obsahovat pouze malé množství nenulových koeficientů v $\vvb \Xi$. Obecně můžeme tento problém formulovat jako
\begin{equation}\label{eq:sindy_optimization_problem}
	\min_{\vvb \Xi} \brackets{\frac 1 2 \norm{\dot{\vv X} - \vvb \Theta(\vv X) \vvb \Xi}^2_2 + \mu R(\vvb \Xi)},
\end{equation}
kde $R(\cdot)$ je regularizující funkce vynucující řídkost (viz také \eqref{eq:lasso_estimator} speciálně pro LASSO regresi). Šetrný model můžeme získat například pomocí metody \textit{STLS}, viz \ref{ch:STLS}, nebo pomocí LASSO regrese, viz \ref{ch:lasso_regression}, přičemž prof.\,Brunton doporučuje metodu \textit{STLS} \cite{brunton_kutz_2019}. Dále pak v \cite{champion2020unified} je ukázáno použití metody \textit{SR3}, jakožto alternativy k \textit{STLS}, viz \ref{ch:optimization_algorithms}, a v sekci \ref{ch:dstls} navrhujeme novou metodu \textit{DSTLS} taktéž upravující původní metodu \textit{STLS}.

Dohromady aproximaci $i$-té složky systému \eqref{eq:sindy_system} dostaneme jako
\begin{equation*}
	\dot x_i = \vvb \Theta(\vv x) \vvb \xi_i,
\end{equation*}
kde $\vvb \Theta(\vv x)$ vnímáme jako vektor \textit{symbolických funkcí} ve smyslu \eqref{eq:sindy_theta_function}.

\section{Ukázková implementace}

Ukažme si nyní ukázkovou implementaci SINDy v jazyce \Julia. Jako optimalizační algoritmy využijeme \textit{STLS}, algoritmus \ref{alg:stls}, a LASSO regresi, algoritmus \ref{alg:lasso_admm}, z kapitoly \ref{ch:sparse_regression} -- tyto metody budeme souhrnně v kódu, viz algoritmus \ref{alg:sindy_main}, označovat abstraktním typem \juliav|Optimizer|, přičemž volba konkrétní metody bude na uživateli.

Hlavní myšlenkou SINDy je první vytvořit knihovnu možných funkcí, pro kterou budeme používat abstraktní typ \juliav|Library|, a potom se pokusit najít takové koeficienty pro funkce z knihovny, že co nejlépe vystihují data ze systému \eqref{eq:sindy_system} a přitom jsou řídké. 

Tento postup můžeme algoritmicky zapsat pomocí funkce \juliav|discover|, viz algoritmus \ref{alg:sindy_main}, která na vstupu potřebuje matici $\vv X$, maticí derivací $\vv{\dot X}$, knihovnu kandidátů a optimalizační metodu.

\begin{algorithm}[H]
% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaverbatim}

function discover(X, Ẋ, library::T, optimizer::TT; kwargs...) where {T<:Library,TT<:Optimizer}
    Θ = library(X; kwargs...)
    Ξ = optimizer(Θ, Ẋ; kwargs...)
    return Ξ
end
\end{juliaverbatim}
	\caption{Ústřední funkce SINDy metody}
	\label{alg:sindy_main}
\end{algorithm}

Knihovna funkcí, která je v algoritmu \ref{alg:sindy_main} vyjádřena abstraktním typem \juliav|Library|, může mít mnoho různých podob a tím se bude lišit, jak ji budeme v kódu vytvářet.

Pro začátek si definujme funkci, která pro každý řádek matice $\vv X$ vyhodnotí knihovní funkce na tomto pozorování.

\begin{algorithm}[H]
% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaverbatim}
function (library::Library)(X; iterate_by=eachrow, kwargs...)
    Θ = reshape.([func(X) for func in library], length(iterate_by(X)), 1)
    return hcat(Θ...)
end

eachstate(func, X; iterate_by=eachrow) = [func(iteration) for iteration in iterate_by(X)]
\end{juliaverbatim}
	\caption{Ústřední funkce pro práci s knihovnami kandidátů}
	\label{alg:library_main}
\end{algorithm}

Vlastnoručně implementujeme pouze několik vybraných typů knihoven, a to monomiální knihovnu (viz Přílohy/algoritmus \ref{alg:library_monomial}), např. funkce $(1, x, y, x^2, y^2)$, polynomiální knihovnu (viz Přílohy/algoritmus \ref{alg:library_polynomial}), např. funkce $(1, y, yx, x)$, a knihovnu z libovolných funkcí zvolených uživatelem (viz Přílohy/algoritmus \ref{alg:library_custom}). Dalšími knihovnami, které bychom mohli použít, by byly například trigonometrické funkce.

Tímto jsme poskládali dohromady všechny stavební bloky metody SINDy a můžeme si ji ukázat na příkladu systému daném FitzHughovým--Nagumovým modelem
\begin{equation}\label{eq:fhn_model}
	\begin{aligned}
		\dot V &= 0.8 - W + V - \frac 1 3 V^3, \\
		\dot W &= 0.056 - 0.064 \cdot W + 0.08 \cdot V.
	\end{aligned}
\end{equation}


Pro jednoduchost předpokládejme, že už máme data o tomto systému, přesněji matici měření stavových proměnných $\vv X$ a matici derivací $\vv{\dot X}$. Dále určeme, že budeme používat knihovnu polynomů 2 proměnných stupně nejvýše 3 a jakožto optimalizační metodu využijeme \textit{STLS} \addLearnerLink{FitzHugh-Nagumo-Custom}.

% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaconcode}
using Revise
using ModelingToolkit, DifferentialEquations
using DataFrames, CSV, Random

# My custom package
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator
using DataDrivenDynamics.SINDy

# Setting up independent variable namely time t and dependent variable V(t), W(t)
@variables t V(t) W(t)
@parameters a b c d iₑ

D = Differential(t)

# Governing equations for our system
V̇(V, W, a, b, c, d, iₑ) = V - V^3 / 3 - W + iₑ
Ẇ(V, W, a, b, c, d, iₑ) = a * (b * V - c * W + d)

params = [
    a => 0.08, b => 1.0,
    c => 0.8, d => 0.7,
    iₑ => 0.8
]
starting_values = [3.3, -2.0]
time_range = (0.0, 100.0)

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(V) ~ V̇(V, W, a, b, c, d, iₑ),
    D(W) ~ Ẇ(V, W, a, b, c, d, iₑ)
])
ode_problem = ODEProblem(ode_system, starting_values, time_range, params)

# The ODE problem may be stiff, so use a stiff solver
solution = solve(ode_problem, Rosenbrock23(); saveat=0.1)

# Compute the derivatives from the data
dict = Dict(params)
derivatives = [[
    V̇(V, W, dict[a], dict[b], dict[c], dict[d], dict[iₑ]),
    Ẇ(V, W, dict[a], dict[b], dict[c], dict[d], dict[iₑ]),
] for (V, W) in solution.u]

# transform vectors of states and derivatives into matrices
X = vcat(reshape.(solution.u, 1, 2)...)
Ẋ = vcat(reshape.(derivatives, 1, 2)...)
\end{juliaconcode}
% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaconsole}


basis = PolynomialLibrary(3, 2)
optimizer = STLS()
Ξ = discover(X, Ẋ, basis, optimizer; max_iter=100)
prettyprint(Ξ, basis; vars=["V" "W"])
\end{juliaconsole}

Z ukázky výše vidíme, že až na zaokrouhlovací chyby jsme v tomto, ačkoliv jednoduchém, případě nalezli systém správně.

\section{Knihovna DataDrivenDiffEq.jl}

Ačkoliv je pro vzdělávací účely vlastní implementace nutností, z pohledu dlouhodobé udržitelnosti, efektivity či funkcionality je vhodné použít volně dostupné knihovny. Knihovna \href{https://docs.sciml.ai/DataDrivenDiffEq/stable/}{DataDrivenDiffEq.jl} \cite{datadrivendiffeq}, přesněji \uv{podbalíček} \href{https://docs.sciml.ai/DataDrivenDiffEq/stable/libs/datadrivensparse/sparse_regression/}{DataDrivenSparse}, implementuje nejen metodu SINDy v jazyce \Julia. Udělejme proto nyní rychlé srovnání naší vlastní implementace a této knihovny.

Abychom mohli použít SINDy metodu v této knihovně, je nejdříve potřeba si zadefinovat tzv. \juliav|DataDrivenProblem|. V této práci budeme výhradně používat \juliav|ContinuousDataDrivenProblem|, ale z dokumentace můžeme zjistit, že je implementovaný např. i \juliav|DiscreteDataDrivenProblem| a další.

% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaconcode}

using ModelingToolkit, DifferentialEquations
using DataFrames, CSV, Random

# Setting up independent variable namely time t and dependent variable V(t), W(t)
@variables t V(t) W(t)
@parameters a b c d iₑ

D = Differential(t)

# Governing equations for our system
V̇(V, W, a, b, c, d, iₑ) = V - V^3 / 3 - W + iₑ
Ẇ(V, W, a, b, c, d, iₑ) = a * (b * V - c * W + d)

params = [
    a => 0.08, b => 1.0,
    c => 0.8, d => 0.7,
    iₑ => 0.8
]
starting_values = [3.3, -2.0]
time_range = (0.0, 100.0)

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(V) ~ V̇(V, W, a, b, c, d, iₑ),
    D(W) ~ Ẇ(V, W, a, b, c, d, iₑ)
])
ode_problem = ODEProblem(ode_system, starting_values, time_range, params)

# The ODE problem may be stiff, so use a stiff solver
solution = solve(ode_problem, Rosenbrock23(); saveat=0.1)

# Compute the derivatives from the data
dict = Dict(params)
derivatives = [[
    V̇(V, W, dict[a], dict[b], dict[c], dict[d], dict[iₑ]),
    Ẇ(V, W, dict[a], dict[b], dict[c], dict[d], dict[iₑ]),
] for (V, W) in solution.u]

# transform vectors of states and derivatives into matrices
X = vcat(reshape.(solution.u, 1, 2)...)
Ẋ = vcat(reshape.(derivatives, 1, 2)...)
times = 0:0.1:100
\end{juliaconcode}
% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaconsole}
using DataDrivenDiffEq, DataDrivenSparse
@named data_driven_problem = ContinuousDataDrivenProblem(X', times, Ẋ')
\end{juliaconsole}

Dalším krokem je volba knihovny kandidátů, což zde probíhá zvolením proměnných přes makro \juliav|@variables|. V našem případě jsme dále s pomocí funkce \juliav|polynomial_basis| vytvořili pole obsahující funkce polynomiální báze v proměnných $V,W$ stupně nejvýše 3. V posledním kroku tuto bázi ještě převedeme na odpovídající typ \juliav|Basis|, přičemž v tomto místě bychom mohli upravit poslední detaily ohledně této knihovny kandidátů.

% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaconsole}
@variables V, W
term_library = polynomial_basis([V, W], 3)
@named model_basis = Basis(term_library, [V, W])
\end{juliaconsole}

V neposlední řadě potřebujeme na vyřešení SINDy problému \eqref{eq:sindy_model} optimalizační metodu. Knihovna DataDrivenDiffEq.jl poskytuje metodu \textit{STLS} (pod typem \juliav|STLSQ|), metodu LASSO regrese řešenou pomocí \textit{ADMM} algoritmu (pod typem \juliav|ADMM|) a také metodu \textit{SR3} (pod typem \juliav|SR3|), kterou dále rozebereme v sekci \ref{ch:optimization_algorithms}.

% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaconsole}
optimizer = STLSQ(0.025)
\end{juliaconsole}

V tuto chvíli nám již zbývá na vyřešení problému zavolat funkci \juliav|solve|, která odpovídá funkci \juliav|discover| z naší implementace. Pomocí funkce \juliav|get_basis| získáme nalezené rovnice, přičemž hodnoty parametrů řešení dostaneme funkcí \juliav|get_parameter_map|, která vrací jejich \textit{hash mapu} \addLearnerLink{FitzHugh-Nagumo-Bare}.

% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaconsole}
solution = solve(data_driven_problem, model_basis, optimizer); print(solution)
fitted_system = get_basis(solution); print(fitted_system)
fitted_parameters = get_parameter_map(fitted_system)
\end{juliaconsole}

Celkem si můžeme všimnout, že jsem dostali stejné řešení jako pomocí naší vlastní implementace.
\section{Výzvy při hledání modelů}

V této kapitole jsme zatím předváděli ukázkový případ použití SINDy metody na FitzHughově--Nagumově modelu. Předpokládali jsme, že známe matici stavových proměnných $\vv X$, matici derivací $\dot{\vv X}$, že jsou tyto hodnoty bez šumu a že jich známe dostatek. Navíc jsme mlčky uvažovali, že měříme \textit{ty správné} proměnné, a volbu kandidátů do knihovny jsme udělali na základě předchozí znalosti tohoto systému -- věděli jsme, že obsahuje nejvýše polynom 3. stupně, a tedy jsme jej do knihovny zahrnuli. Naopak jsme knihovnu kandidátů ochudili o funkce, o kterých jsme věděli, že se v systému nevyskytují. 

Avšak tyto předpoklady jistě nemusí být splněny (či alespoň část z nich). Proto se nyní budeme věnovat tomu, jaké mají zmíněné nedostatky dat vliv na chování SINDy metody a jak je řešit. 

\subsection{SINDy a data}
Jak již bylo nastíněno na začátku této sekce, pojďme se nyní zaměřit na možné problémy spojené s nedostatkem dat. Tedy nyní předpokládejme, že měříme správně proměnné a máme čistá data. 

Aby SINDy metoda fungovala dle očekávání, je potřeba, abychom měli \textit{relativně jemně} měřená data po \textit{dostatečně dlouhou} dobu \cite{brunton_video_data}. Co to ale znamená v praxi?

V článku \cite{Champion_2019} dr.\,Kathleen Champion a prof.\,Brunton ukázali, že pokud máme data měřená dostatečně často (vůči jedné periodě\footnote{Periodou se např. u Lorenzova systému rozumí doba, kterou trajektorie potřebuje na \uv{projití jednoho křídla} a odpovídá $t \approx 0.759$.} v datech), tak stačí i méně než jedna perioda na korektní identifikaci systému, podle tvaru systému a toho, zda je deterministicky chaotický\footnote{Systém nazveme \textit{deterministicky chaotický}, jestliže velmi malá změna v počátečních podmínkách způsobí velkou změnu v dynamice tohoto systému.}. Pro Lorenzův model (viz sekce \ref{ch:lorenz}) je to cca 70~--~110~\% periody, podle počáteční podmínky, viz obrázek \ref{fig:sindy_sampling_rate_and_duration}.

\begin{figure}[H]
	\centering
	\tryshow{../experiments/3D-Lorenz-Sampling_Rate/figures/sampling_rate_and_duration/[-20, -20, 25]_30.0_10_5_0.01_0.1:0.05:2.5_4.0:0.25:15.0.pdf}{}
	\caption{Vliv délky trajektorie a počtu pozorování na jednu periodu na chování SINDy metody na Lorenzově modelu s chaotickými parametry \addExperimentLink{3D-Lorenz-Sampling_Rate}{generator_and_learner}}
	\label{fig:sindy_sampling_rate_and_duration}
\end{figure}

% \begin{figure}
% 	\centering
% 	\tryshow{../experiments/3D-Lorenz-Sampling_Rate/figures/trajectory_duration/0.025:0.025:1.2_[-20, -20, 25]_0.0001:0.001:0.1091.pdf}{}
% 	\caption{Znázornění trajektorie v závislosti na době jejího trvání}
% 	\label{fig:trajectory_duration}
% \end{figure}

V opačném případě, tj. pokud \uv{snímkujeme} data po dlouhých časových okamžicích, potřebujeme \uv{vidět} celý systém i několikrát. Pro úplnost uveďme, že za úspěšně nalezený model jsme považovali situaci, kdy jsme nalezli model se 7 parametry a žádný parametr se od původního nelišil o více než 0.01. Každou dvojici délky trajektorie a počtu pozorování na jednu periodu jsme spustili pro 10 různých počátečních podmínek (ty byly náhodně vybrány z bodů na atraktoru před spuštěním programu). Při hledání modelu jsme použili polynomiální knihovnu až 5. stupně a optimalizační metodu \textit{STLS} s $\tau = 0.1$.

Avšak i s dostatkem dat se nevyhneme časté situaci, že jsou tato data zatížena šumem a navíc nemáme měření derivací stavových proměnných. V takovém případě musíme použít techniky odstranění šumu, abychom podpořili úspěšné nalezení dynamiky.

Na odstranění šumu z dat můžeme použít například metodu totální variace, definovanou v sekci \ref{ch:tv_denoise}. Na vypočtení derivací z dat zatížených šumem je výhodné použít některou \textit{regularizovanou metodu numerického derivování}, např. pomocí totální variace \cite{Champion_2019}, viz sekce \ref{ch:tvdiff}.

Ukažme si nyní vliv velikosti šumu na počet nalezených parametrů, $R^2$, AIC a BIC\footnote{Akaikeho a Bayesovské informační kritérium, více \href{https://en.wikipedia.org/wiki/Akaike_information_criterion}{zde} a \href{https://en.wikipedia.org/wiki/Bayesian_information_criterion}{zde}.} Lorenzova systému, viz obrázky \ref{fig:sindy_lorenz_noise_comparison} a \ref{fig:sindy_fhn_noise_comparison}. Ačkoliv jsme ještě nerozebrali zobrazené optimalizační metody (viz sekce \ref{ch:optimization_algorithms}), obrázky \ref{fig:sindy_lorenz_noise_comparison} a \ref{fig:sindy_fhn_noise_comparison} pěkně ilustrují vliv šumu a proto je uvádíme již nyní. Pro každou hodnotu $\hat {\sigma}$ velikosti aditivního šumu s rozptylem dat a každou optimalizační metodu byly všechny \uv{diagnostické metriky} spočteny pro 10 různých -- předem určených -- počátečních podmínek. Tedy na obrázcích \ref{fig:sindy_lorenz_noise_comparison} a \ref{fig:sindy_fhn_noise_comparison} tlusté čáry značí průměr z výsledků pro všechny počáteční podmínky a světleji zbarvené pásy znázorňují rozpětí minima a maxima těchto výsledků.

Další metodou, jak se vypořádat s šumem, je v jistém smyslu oslabení SINDy metody \cite{sindy_integral_terms}. Vzpomeňme si, že SINDy spočívá v nalezení řídkého vektoru $\vvb \xi$ tak, že knihovna kandidátů $\vvb \Theta(\vv x)$ dobře aproximuje původní systém \eqref{eq:sindy_system}, viz \eqref{eq:sindy_approximation}
\begin{equation*}
	\dot{\vv x} = \vv f(\vv x) \approx \sum_{j = 1}^p \vvb \theta_j(\vv x) \xi_j = \vvb \Theta(\vv x) \vvb \xi,
\end{equation*}
což pro $i$-tou složku znamená
\begin{equation*}
	\dot x_i = \brackets{\vvb \Theta(\vv x) \vvb \xi}_i = \sum_{j = 1}^p \xi_j \vvb \theta_{j, i} (\vv x),
\end{equation*}
kde $\vvb \theta_{j, i}(\vv x) = \brackets{\vvb \theta_j(\vv x)}_i$. Tuto formulaci ale můžeme relaxovat a místo ní řešit problém
\begin{equation*}
	x_i(t) = x_i(0) + \sum_{j = 1}^p \xi_j d_{j, i} (\vv x, t),
\end{equation*}
přičemž definujeme
\begin{equation*}
	d_{j, i}(\vv x, t) = \int_0^t \vvb \theta_{j, i} (\vv x(\tau)) \d \tau.
\end{equation*}

Tento přístup umožňuje SINDy tolerovat větší \textit{aditivní} šum, neboť jej \uv{rozprostře} přes všechna data \cite{sindy_integral_terms}.

\subsection{Souřadnicový systém}

Dosud jsme předpokládali, že měřené proměnné (sloupce matice $\vv X$) odpovídají proměnným hledaného systému \eqref{eq:sindy_system}
\begin{equation*}
	\dot{\vv x} = \vv f(\vv x)
\end{equation*}

Jak ale můžeme vědět, že měřené proměnné jsou \uv{ty správné}? V reálném světě můžeme narazit na různé problémy \cite{brunton_video_coordinates}, např.
\begin{itemize}
	\item měříme pouze některé proměnné systému \eqref{eq:sindy_system},
	\item měříme \uv{příliš mnoho proměnných} -- tedy mezi některými mohou být silné korelace,
	\item v měřených proměnných nemá systém \eqref{eq:sindy_system} řídkou reprezentaci.
\end{itemize}

Například na prostorově časových systémech, jako je třeba proudění za překážkou, na volbu souřadnicového systému dobře funguje SVD rozklad, který poskytuje ortogonální bázi dobře vystihující variabilitu dat \cite{Champion_2019, sindy_autoencoders}.

SVD navíc můžeme vnímat jako jednovrstvý lineární autoenkodér -- neuronovou síť, která mnohorozměrný vstup transformuje přes \uv{malou} vrstvu zpátky na mnohorozměrný výstup. Tento koncept můžeme zobecnit a místo jednovrstvého lineárního autoenkodéru použít vícevrstvou nelineární neuronovou síť, viz obrázek \ref{fig:sindy_autoencoder}.

\begin{figure}[H]
	\centering
	% Taken from https://tikz.net/neural_networks/
	% AUTOENCODER
	\begin{tikzpicture}[x=1.6cm,y=1.2cm]
		\large
		\message{^^JNeural network without arrows}
		\readlist\Nnod{6,5,4,3,2,3,4,5,6} % array of number of nodes per layer
		
		% TRAPEZIA
		\node[above,align=center,myorange!60!black] at (3,2.4) {encoder $\vf(\vv x)$};
		\node[above,align=center,myblue!60!black] at (7,2.4) {decoder $\psi(\vv z)$};
		\node[above,align=center,myblue!60!myorange] at (5,1) {$\vv z$};
		\draw[myorange!40,fill=myorange,fill opacity=0.02,rounded corners=2]
		(1.6,-2.7) --++ (0,5.4) --++ (2.8,-1.2) --++ (0,-3) -- cycle;
		\draw[myblue!40,fill=myblue,fill opacity=0.02,rounded corners=2]
		(8.4,-2.7) --++ (0,5.4) --++ (-2.8,-1.2) --++ (0,-3) -- cycle;
		
		\message{^^J  Layer}
		\foreachitem \N \in \Nnod{ % loop over layers
			\def\lay{\Ncnt} % alias of index of current layer
			\pgfmathsetmacro\prev{int(\Ncnt-1)} % number of previous layer
			\message{\lay,}
			\foreach \i [evaluate={\y=\N/2-\i+0.5; \x=\lay; \n=\nstyle;}] in {1,...,\N}{ % loop over nodes
				
				% NODES
				\node[node \n,outer sep=0.6] (N\lay-\i) at (\x,\y) {};
				
				% CONNECTIONS
				\ifnum\lay>1 % connect to previous layer
				\foreach \j in {1,...,\Nnod[\prev]}{ % loop over nodes in previous layer
					\draw[connect,white,line width=1.2] (N\prev-\j) -- (N\lay-\i);
					\draw[connect] (N\prev-\j) -- (N\lay-\i);
					%\draw[connect] (N\prev-\j.0) -- (N\lay-\i.180); % connect to left
				}
				\fi % else: nothing to connect first layer
				
			}
		}
		
		% LABELS
		\node[above=1,align=center,mygreen!60!black] at (N1-1) {input $\vv x$};
		\node[above=1,align=center,myred!60!black] at (N\Nnodlen-1) {output $\hat{\vv x}$};
		
	\end{tikzpicture}
	\caption{Schematické znázornění autoenkodéru pro použití se SINDy metodou}
	\label{fig:sindy_autoencoder}
\end{figure}

Takto lze zkombinovat schopnost autoenkodéru naučit se vhodný souřadnicový systém se SINDy hledající řídký systém na těchto nových souřadnicích. Dokonce je optimalizaci SINDy problému možné zařadit do ztrátové funkce neuronové sítě autoenkodéru \cite{sindy_autoencoders}.

Obdobný postup můžeme použít i na situaci, kdy vstupní data o systému $\vv X$ neobsahují některou z proměnných, pokud tento systém splňuje jisté požadavky -- mimo jiné předpoklady \href{https://en.wikipedia.org/wiki/Takens's_theorem}{Takensovy věty o vnoření}. Pomocí \textit{time-delay embedding} (volně přeloženo \textit{vnoření se zpožděním}) souřadnic, která transformujeme autoenkodérem, jsme schopni nalézt řídký model v \uv{originálních souřadnicích}\footnote{Ve vnořených souřadnicích často dostaneme lineární model, což souvisí s teorií Koopmanova operátoru.} \cite{sindy_autoencoders}.

\subsection{Výběr členů do knihovny}

Výběr členů do knihovny kandidátních funkcí z velké části ovlivňuje úspěch SINDy metody -- pokud knihovna nebude obsahovat členy, které byly v původním systému \eqref{eq:sindy_system}, pak se nám nejspíše nepodaří najít řídký model, který by tato data dobře aproximoval \cite{brunton_video_library}.

Určení těchto funkcí může být obtížné, nemáme-li žádné znalosti o příslušném fyzikálním (či jiném) systému, a jednoduše se můžeme dostat do situace, že nevíme, které členy by knihovna obsahovat měla a které ne. 

\noindent%
Obecně můžeme narazit na následující výzvy:

\subsubsection{Podmíněnost matice $\vvb \Theta(\vv X)$}
Knihovna by měla být navržena tak, aby zahrnovala členy, které jsou vzájemně nezávislé nebo jen málo korelované, a aby se minimalizovalo množství redundance a zbytečné komplexity v modelu. V opačném případě bude pravděpodobně matice $\vvb \Theta(\vv X)$ \textit{špatně podmíněná} a bude náročné najít minimalizující matici $\vvb \Xi$, zvlášť pokud budou data ovlivněna šumem. Například pokud knihovna obsahuje funkce $\sin \vv x, \vv x, \vv x^3, \vv x^5$, pak je velká šance, že matice $\vvb \Theta(\vv X)$ bude špatně podmíněná, neboť jsou tyto členy \textit{nezanedbatelně korelované}. S tímto se také váže předpoklad, že stavové proměnné systému jsou nezávislé -- v opačném případě bychom ze stejného problému museli některé vyřadit. 

Je vhodné zmínit, že pod podmíněností matice myslíme \textit{ERC podmíněnost} \cite{sparse_condition_number, brunton_video_library}, která kvantifikuje podmíněnost matice na jejích řídkých podprostorech.

\subsubsection{Velikost knihovny}
Pokud máme příliš rozsáhlou knihovnu, také se dostáváme ke problému zmíněnému výše. Nejen, že pro velké matice je výpočetně náročné spočítat pseudoinverzi, ale také je matice $\vvb \Theta(\vv X)$ příslušná příliš velké knihovně špatně podmíněná a dochází k numerickým chybám při výpočtu pseudoinverze.

\subsubsection{Nelinearita systému}
Metoda SINDy je navržena pro identifikaci lineárních modelů nelineárních systémů. Pokud je však systém příliš nelineární (např. pokud je zadán racionálními lomenými funkcemi), může být těžké najít vhodnou knihovnu členů pro identifikaci modelu.

\subsubsection{Vnější vlivy}
Pokud máme systém, který závisí nejen na stavových proměnných, ale i na vnějších vlivech, pak bychom měli možné formy tohoto vlivu zakomponovat do knihovny kandidátů. Příkladem může být model predátor--kořist, ve kterém kontrolujeme populaci např. kořisti, či epidemiologické modely, o kterých máme data z reálného světa, kde si nemůžeme dovolit přestat léčit lidi a nebo zrušit všechna bezpečnostní opatření, jen abychom mohli změřit dynamiku systému bez okolních vlivů.

\subsection{Optimalizační algoritmy} \label{ch:optimization_algorithms}

Vzpomeňme si, že podstatou SINDy metody je najít koeficienty $\vvb \Xi$ šetrného modelu řešícího úlohu \eqref{eq:sindy_model} 
\begin{equation*}
	\vv{\dot{X}} = \vvb \Theta (\vv X) \vvb \Xi + \vvb \epsilon.
\end{equation*}
Takový model můžeme dostat různými optimalizačními metodami, které si dále rozebereme. Jejich srovnání můžeme vidět i na obrázcích \ref{fig:sindy_lorenz_noise_comparison} a \ref{fig:sindy_fhn_noise_comparison}.

\subsubsection{LASSO regrese}
Ačkoliv by se intuitivně mohlo zdát, že LASSO regrese (viz \ref{ch:lasso_regression}) bude dávat správné výsledky (např. i v \cite{brunton_kutz_2019}), ve skutečnosti tomu tak většinou není. Matice koeficientů $\vvb \Xi$ podle LASSO regrese často obsahuje velké množství malých, ale nenulových koeficientů. I když zvýšíme parametr regularizace $\mu$, tak často neidentifikujeme správný model \cite{champion2020unified, brunton_video_optimization}.

\subsubsection{Sequentially Thresholded Least Squares (STLS)}
Tato optimalizační metoda, doporučená v \cite{brunton_kutz_2019}, je založena na opakování metody nejmenších čtverců při vyřazování příliš malých koeficientů, viz \ref{ch:STLS}. Pro \uv{standardní} formulaci SINDy problému \eqref{eq:sindy_model} funguje \textit{STLS} relativně dobře \cite{champion2020unified}. Nicméně jelikož se jedná o \uv{samostatnou metodu}, bez přesně vyjádřené ztrátové funkce, tak je ji potřeba přeformulovat, změníme-li tvar SINDy metody (například přidáním dalších omezení).

\subsubsection{Sparse Relaxed Regularized Regression (SR3)}
Jak již bylo zmíněno na začátku této kapitoly, dalším algoritmem, kterým lze nalézt šetrný model, je metoda \textit{SR3}. Tato metoda, se kterou přišla Dr.\,Champion et al. v \cite{champion2020unified}, se v jistém smyslu snaží být kompromisem mezi LASSO regresí a \textit{STLS}. 

\textit{SR3} upravuje optimalizační problém SINDy metody \eqref{eq:sindy_optimization_problem} tak, aby byl robustnější vůči odlehlým pozorováním a používal nelineární parametrický odhad \cite{champion2020unified, zheng2018unified}
\begin{equation} \label{eq:sindy_sr3}
	\min_{\vvb \Xi, \vv W} \frac 1 2 \norm{\dot{\vv X} - \vvb \Theta(\vv X)\vvb \Xi}_2^2 + \mu R(\vv W) + \frac 1 {2 \nu} \norm{\vvb \Xi - \vv W}^2_2,
\end{equation}
kde $\vv W$ je pomocná proměnná. Tento problém můžeme řešit pomocí příbuzné metody k \textit{ADMM}, viz algoritmus \ref{alg:sr3}.

\begin{algorithm}[H]
% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaverbatim}

function SR3(X, Ẋ, Θ; ϵ, W, R, ν, μ)
	k = 0
	ε = 2 * ϵ
	Ξ = similar(W)
	while ε > ϵ
		k += 1
		# Find the minimizing Ξₖ (usually via OLS or similar)
		Ξₖ = argmin(1/2 * norm(Ẋ - Θ(X) * Ξ)^2 + 1/(2*ν) * norm(Ξ - W)^2 for Ξ in ℝ(size(W)))
		# Update the auxiliary variable
		Wₖ = prox(Ξₖ, μ*ν*R)
		# Recalculate the error ε
		ε = norm(Wₖ - W) / ν
		W = Wₖ; Ξ = Ξₖ
	end
	return Ξ
end
\end{juliaverbatim}
	\caption{Schématické znázornění algoritmu řešícího \textit{SR3}}
	\label{alg:sr3}
\end{algorithm}

\subsubsection{Dynamic Sequentially Thresholded Least Squares (DSTLS)} \label{ch:dstls}
Poslední optimalizační metoda, kterou si představíme, je motivovaná problémem s rozdílnými velikostmi parametrů v jednotlivých rovnicích. Vezměme si například FitzHughův--Nagumův model \eqref{eq:fhn_model}
\begin{align*}
	\dot V(V,W) &= 0.8 - W + V - \frac 1 3 V^3, \\
	\dot W(V,W) &= 0.056 - 0.064 \cdot W + 0.08 \cdot V.
\end{align*}

Tento model má v první rovnici parametry v řádu desetin, ale v druhé rovnici v řádu setin. Pokud se tedy pokoušíme nalézt tento model pomocí SINDy a \textit{STLS} metody (viz \eqref{eq:stls_problem}), narazíme na problém s volbou parametru $\tau$. 

Zvolíme-li $\tau$ velké, např. $\tau \geq 0.1$, v druhé rovnici nenajdeme žádné vhodné členy (tedy $\dot W$ bude model aproximovat nulou). Naopak při volbě $\tau$ malého, např. $\tau \leq 0.03$, sice nalezneme správně dynamiku v proměnné $W$, ale v rovnici pro proměnnou $V$ zbude velké množství členů s malým koeficientem.

Proto navrhujeme upravit metodu \textit{STLS} tak, že v každé rovnici si metoda omezovací hranici upravuje podle již nalezených parametrů této rovnice. Jelikož se jedná o více \uv{proměnlivou} modifikaci \textit{STLS} metody, budeme ji dále nazývat \textit{Dynamic Sequentially Thresholded Least Squares (DSTLS)}. Mimo rámec této práce usilujeme o publikaci metody \textit{DSTLS}.

Jádrem naší úpravy je předpoklad, že se koeficienty v každé z rovnic od sebe výrazně neliší. Tedy místo vylučování koeficientů menších než hranice $\tau$ metoda \textit{DSTLS} vyloučí koeficienty menší než $\tau$-násobek největšího koeficientu. Matematicky zapsáno se jedná o úlohu
\begin{equation}\label{eq:dstls_problem}
	\begin{gathered}
		\min_{\vvb \xi} \frac 1 2 \norm{\vv Y - \vv X \vvb \xi}_2^2 \\
		\text{za podmínky } \forall j \in \set{1, \dots, n}: \; \absval{\xi_j} > \tau \cdot \max \set{\absval{\xi} : \xi \in \vvb \xi},
	\end{gathered}
\end{equation}
kde uvažujeme $\tau \in (0,1)$. Tato optimalizační metoda je ukázkově implementována v algoritmu \ref{alg:dstls}. Dále na obrázcích \ref{fig:sindy_lorenz_noise_comparison} a \ref{fig:sindy_fhn_noise_comparison} můžeme porovnat její chování oproti předešlým metodám.

\begin{algorithm}
% !!! WARNING !!! This is an automatically generated part. Please do not modify.
\begin{juliaverbatim}

Base.@kwdef struct DSTLS <: Optimizer
    τ = 0.1
end

function (optimizer::DSTLS)(Θ, Ẋ; max_iter=10, kwargs...)
    vars = variables(Ẋ; kwargs...)
    Ξ = Θ \ Ẋ

    for _ in 1:max_iter
        for variable in 1:vars
            ξₕ = maximum(abs.(Ξ[:, variable]))
            small_indices = @. abs(Ξ[:, variable]) < (optimizer.τ * ξₕ)
            Ξ[small_indices, variable] .= 0

            big_indices_in = @. !small_indices
            Ξ[big_indices_in, variable] .= Θ[:, big_indices_in] \ Ẋ[:, variable]
        end
    end

    return Ξ
end
\end{juliaverbatim}
	\caption{Ukázková implementace \textit{DSTLS} metody}
	\label{alg:dstls}
\end{algorithm}

\begin{figure}
	\centering
	\tryshow{../experiments/3D-Lorenz-Noise/figures/[STLSQ(0.1), SR3(0.4), ADMM(0.1), ADMM(2.0), DSTLS(0.1)]/quality-of-fit-for-sigma/k:[4, 5, 5]_noise_range:0.001:0.005:0.096_reg:[0.08, 0.9, 0.6]_trajectory_count:10_AdditiveVarianceNoise_λ:[0.3, 0.3, 0.3].pdf}{width=\textwidth}
	\caption{Vliv velikosti rozptylu šumu na chování SINDy metody na Lorenzově modelu \addLearnerLink{3D-Lorenz-Noise}}
	\label{fig:sindy_lorenz_noise_comparison}
\end{figure}

\begin{figure}
	\centering
	\tryshow{../experiments/FitzHugh-Nagumo-Noise/figures/[STLSQ(0.1, 0.0), SR3(0.00045, 1.0, HardThreshold()), ADMM(0.025, 1.0), DSTLS(0.1)]/quality-of-fit-for-sigma/k:[4, 5]_noise_range:0.001:0.005:0.096_reg:[0.08, 0.9]_trajectory_count:10_AdditiveVarianceNoise_λ:[0.3, 0.3].pdf}{width=\textwidth}
	\caption{Vliv velikosti rozptylu šumu na chování SINDy metody na FitzHughově--Nagumově modelu \addLearnerLink{FitzHugh-Nagumo-Noise}}
	\label{fig:sindy_fhn_noise_comparison}
\end{figure}