# For compatibility with Overleaf and it compiling via LaTeXMk we need to set
# some variables accordingly.
# Our custom Julia precompilation process doesn't help it either
jobname = output
outfile = thesis
bibfile = thesis

# thesis is a default value, if not set from the environment
infile ?= thesis

compile: infile = thesis_compiled
raw-compile: infile = thesis
compile-svoc: infile = svoc_thesis_compiled
compile-svoc: outfile = svoc_thesis
raw-compile-svoc: infile = svoc_thesis
raw-compile-svoc: outfile = svoc_thesis

# we export ALL variables
export

# --- COMPILATION SECTION ---
compile:
	@$(MAKE) precompile
	@$(MAKE) base-compile infile="$(infile)"
	
raw-compile:
	@$(MAKE) base-compile infile="$(infile)"

compile-svoc:
	@$(MAKE) compile infile="$(infile)" outfile="$(outfile)"

raw-compile-svoc:
	@$(MAKE) raw-compile infile="$(infile)" outfile="$(outfile)"

base-compile:
	@$(MAKE) clean

	@clear -x
	@printf "Compilation of $(infile)\n"

	@$(MAKE) load-package

	# make fonts & thesis.bib are copied from the root folder
	@$(MAKE) step-ensure-files

	# compile .tex files with LuaLaTeX for the first time
	@$(MAKE) step-lualatex compilation_count=1

	@$(MAKE) step-juliatex
	@$(MAKE) step-biber
	@$(MAKE) step-xindex
	
	# compile again (now we have JuliaTeX, Biber & Xindex intermediate files)
	@$(MAKE) step-lualatex compilation_count=2
	
	# rename generated pdf to the desired name
	mv $(jobname).pdf $(outfile).pdf

	@$(MAKE) clean

	@clear -x
	@printf "All okay!\n" 
	@printf "Go enjoy the magnificent PDF :3\n"

auto-compile:
	@if [ $(ARGS) == svoc_thesis ]; \
	then $(MAKE) compile-svoc; \
	else $(MAKE) compile; \
	fi

# --- MISC SECTION ---
load-package:
	julia --project -e 'using Pkg; Pkg.activate("."); Pkg.develop(path="../")'
init:
	julia --project -e 'using Pkg; Pkg.activate("."); Pkg.develop(path="../"); Pkg.instantiate()'
@clear-fonts:
	luaotfload-tool -vvv --update --force
clean:
	latexmk -C --jobname=$(jobname) thesis.tex
	latexmk -C --jobname=$(jobname) svoc_thesis.tex
	latexmk -C --jobname=$(jobname) thesis_compiled.tex
	latexmk -C --jobname=$(jobname) svoc_thesis_compiled.tex
	([ ! -e $(bibfile).bib ] || rm $(bibfile).bib)
	([ ! -e fonts ] || rm -rf fonts)


# --- PRECOMPILATION SECTION ---
precompile:
	@printf "Using main input file $(infile)\n"
	@printf "Running Julia includer.jl\n"

	julia ../utils/includer.jl document

	@clear -x
	@printf "Julia includer.jl okay\n"

remove-precompilation:
	@printf "Removing precompiled files\n"
	@printf "This is generally a BAD idea, as they are version controlled\n"

	([ ! -e thesis_compiled.tex ] || rm thesis_compiled.tex)
	([ ! -e svoc_thesis_compiled.tex ] || rm svoc_thesis_compiled.tex)
	([ ! -e text_prace_compiled ] || rm -rf text_prace_compiled)

# --- COMPILATION STEPS ---
step-ensure-files:
	@printf "Copying $(bibfile).bib and fonts directory here\n"

	([ ! -e $(bibfile).bib ] || rm $(bibfile).bib)
	([ ! -e fonts ] || rm -rf fonts)
	ln -s ../$(bibfile).bib $(bibfile).bib
	ln -s ../fonts fonts

	@clear -x
	@printf "Copy of $(bibfile).bib and fonts okay\n" 

step-lualatex:
	@printf "Compilation (#$(compilation_count)) with $(jobname) now\n"

	lualatex --jobname=$(jobname) $(infile).tex

	@clear -x 
	@printf "LuaLaTeX okay\n"

step-juliatex:
	@printf "Running PythonTex (JuliaTeX) now\n"

	pythontex $(jobname)
	
	@clear -x
	@printf "JuliaTeX okay\n"

step-biber:
	@printf "Running Biber now\n"

	biber $(jobname)

	@clear -x
	@printf "Biber okay\n"

step-xindex:
	@printf "Running Xindex now\n"

	xindex $(jobname)

	@clear -x
	@printf "Xindex okay\n"

# --- HELP SECTION ---
help:
	@echo "Available commands are:"
	@echo "  'compile' - compiles the thesis along with precompilation using Julia (which includes figures, Julia, etc.)"
	@echo "  'raw-compile'" - compiles the thesis WITHOUT precompilation
	@echo "  'init' -- initialises Julia project used with the document"