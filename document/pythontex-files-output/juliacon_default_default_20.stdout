\begin{Verbatim}[commandchars=\\\{\}]
\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{k}{using}\PYG{+w}{ }\PYG{n}{Revise}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{k}{using}\PYG{+w}{ }\PYG{n}{ModelingToolkit}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{DifferentialEquations}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{k}{using}\PYG{+w}{ }\PYG{n}{DataFrames}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{CSV}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{Random}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }
\PYG{g+go}{\PYGZsh{} My custom package}
\PYG{g+go}{using DataDrivenDynamics}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{k}{using}\PYG{+w}{ }\PYG{n}{DataDrivenDynamics}\PYG{o}{.}\PYG{n}{ProjectKeeper}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{DataDrivenDynamics}\PYG{o}{.}\PYG{n}{Generator}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{k}{using}\PYG{+w}{ }\PYG{n}{DataDrivenDynamics}\PYG{o}{.}\PYG{n}{SINDy}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }
\PYG{g+go}{\PYGZsh{} Setting up independent variable namely time t and dependent variable V(t), W(t)}
\PYG{g+go}{@variables t V(t) W(t)}
\PYG{g+go}{3\PYGZhy{}element Vector\PYGZob{}Symbolics.Num\PYGZcb{}:}
\PYG{g+go}{    t}
\PYG{g+go}{ V(t)}
\PYG{g+go}{ W(t)}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{n+nd}{@parameters}\PYG{+w}{ }\PYG{n}{a}\PYG{+w}{ }\PYG{n}{b}\PYG{+w}{ }\PYG{n}{c}\PYG{+w}{ }\PYG{n}{d}\PYG{+w}{ }\PYG{n}{iₑ}
\PYG{g+go}{5\PYGZhy{}element Vector\PYGZob{}Symbolics.Num\PYGZcb{}:}
\PYG{g+go}{  a}
\PYG{g+go}{  b}
\PYG{g+go}{  c}
\PYG{g+go}{  d}
\PYG{g+go}{ iₑ}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }
\PYG{g+go}{D = Differential(t)}
\PYG{g+go}{(::Symbolics.Differential) (generic function with 2 methods)}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }
\PYG{g+go}{\PYGZsh{} Governing equations for our system}
\PYG{g+go}{V̇(V, W, a, b, c, d, iₑ) = V \PYGZhy{} V\PYGZca{}3 / 3 \PYGZhy{} W + iₑ}
\PYG{g+go}{V̇ (generic function with 1 method)}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{n}{Ẇ}\PYG{p}{(}\PYG{n}{V}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{W}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{a}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{b}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{c}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{d}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{iₑ}\PYG{p}{)}\PYG{+w}{ }\PYG{o}{=}\PYG{+w}{ }\PYG{n}{a}\PYG{+w}{ }\PYG{o}{*}\PYG{+w}{ }\PYG{p}{(}\PYG{n}{b}\PYG{+w}{ }\PYG{o}{*}\PYG{+w}{ }\PYG{n}{V}\PYG{+w}{ }\PYG{o}{\PYGZhy{}}\PYG{+w}{ }\PYG{n}{c}\PYG{+w}{ }\PYG{o}{*}\PYG{+w}{ }\PYG{n}{W}\PYG{+w}{ }\PYG{o}{+}\PYG{+w}{ }\PYG{n}{d}\PYG{p}{)}
\PYG{g+go}{Ẇ (generic function with 1 method)}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }
\PYG{g+go}{params = [}
\PYG{g+go}{    a =\PYGZgt{} 0.08, b =\PYGZgt{} 1.0,}
\PYG{g+go}{    c =\PYGZgt{} 0.8, d =\PYGZgt{} 0.7,}
\PYG{g+go}{    iₑ =\PYGZgt{} 0.8}
\PYG{g+go}{]}
\PYG{g+go}{5\PYGZhy{}element Vector\PYGZob{}Pair\PYGZob{}Symbolics.Num, Float64\PYGZcb{}\PYGZcb{}:}
\PYG{g+go}{  a =\PYGZgt{} 0.08}
\PYG{g+go}{  b =\PYGZgt{} 1.0}
\PYG{g+go}{  c =\PYGZgt{} 0.8}
\PYG{g+go}{  d =\PYGZgt{} 0.7}
\PYG{g+go}{ iₑ =\PYGZgt{} 0.8}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{n}{starting\PYGZus{}values}\PYG{+w}{ }\PYG{o}{=}\PYG{+w}{ }\PYG{p}{[}\PYG{l+m+mf}{3.3}\PYG{p}{,}\PYG{+w}{ }\PYG{o}{\PYGZhy{}}\PYG{l+m+mf}{2.0}\PYG{p}{]}
\PYG{g+go}{2\PYGZhy{}element Vector\PYGZob{}Float64\PYGZcb{}:}
\PYG{g+go}{  3.3}
\PYG{g+go}{ \PYGZhy{}2.0}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{n}{time\PYGZus{}range}\PYG{+w}{ }\PYG{o}{=}\PYG{+w}{ }\PYG{p}{(}\PYG{l+m+mf}{0.0}\PYG{p}{,}\PYG{+w}{ }\PYG{l+m+mf}{100.0}\PYG{p}{)}
\PYG{g+go}{(0.0, 100.0)}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }
\PYG{g+go}{\PYGZsh{} Define ODESystem \PYGZhy{} more precisely the differential equation(s) that define it}
\PYG{g+go}{@named ode\PYGZus{}system = ODESystem([}
\PYG{g+go}{    D(V) \PYGZti{} V̇(V, W, a, b, c, d, iₑ),}
\PYG{g+go}{    D(W) \PYGZti{} Ẇ(V, W, a, b, c, d, iₑ)}
\PYG{g+go}{])}
\PYG{g+go}{Model ode\PYGZus{}system with 2 equations}
\PYG{g+go}{States (2):}
\PYG{g+go}{  V(t)}
\PYG{g+go}{  W(t)}
\PYG{g+go}{Parameters (5):}
\PYG{g+go}{  iₑ}
\PYG{g+go}{  a}
\PYG{g+go}{  d}
\PYG{g+go}{  c}
\PYG{g+go}{⋮}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{n}{ode\PYGZus{}problem}\PYG{+w}{ }\PYG{o}{=}\PYG{+w}{ }\PYG{n}{ODEProblem}\PYG{p}{(}\PYG{n}{ode\PYGZus{}system}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{starting\PYGZus{}values}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{time\PYGZus{}range}\PYG{p}{,}\PYG{+w}{ }\PYG{n}{params}\PYG{p}{)}
\PYG{g+go}{ODEProblem with uType Vector\PYGZob{}Float64\PYGZcb{} and tType Float64. In\PYGZhy{}place: true}
\PYG{g+go}{timespan: (0.0, 100.0)}
\PYG{g+go}{u0: 2\PYGZhy{}element Vector\PYGZob{}Float64\PYGZcb{}:}
\PYG{g+go}{  3.3}
\PYG{g+go}{ \PYGZhy{}2.0}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }
\PYG{g+go}{\PYGZsh{} The ODE problem may be stiff, so use a stiff solver}
\PYG{g+go}{solution = solve(ode\PYGZus{}problem, Rosenbrock23(); saveat=0.1)}
\PYG{g+go}{retcode: Success}
\PYG{g+go}{Interpolation: 1st order linear}
\PYG{g+go}{t: 1001\PYGZhy{}element Vector\PYGZob{}Float64\PYGZcb{}:}
\PYG{g+go}{   0.0}
\PYG{g+go}{   0.1}
\PYG{g+go}{   0.2}
\PYG{g+go}{   0.3}
\PYG{g+go}{   0.4}
\PYG{g+go}{   0.5}
\PYG{g+go}{   0.6}
\PYG{g+go}{   0.7}
\PYG{g+go}{   0.8}
\PYG{g+go}{   0.9}
\PYG{g+go}{   ⋮}
\PYG{g+go}{  99.2}
\PYG{g+go}{  99.3}
\PYG{g+go}{  99.4}
\PYG{g+go}{  99.5}
\PYG{g+go}{  99.6}
\PYG{g+go}{  99.7}
\PYG{g+go}{  99.8}
\PYG{g+go}{  99.9}
\PYG{g+go}{ 100.0}
\PYG{g+go}{u: 1001\PYGZhy{}element Vector\PYGZob{}Vector\PYGZob{}Float64\PYGZcb{}\PYGZcb{}:}
\PYG{g+go}{ [3.3, \PYGZhy{}2.0]}
\PYG{g+go}{ [2.9094425211825548, \PYGZhy{}1.95711016049548]}
\PYG{g+go}{ [2.724993133719877, \PYGZhy{}1.916647775249773]}
\PYG{g+go}{ [2.6261677277374877, \PYGZhy{}1.8775346442244059]}
\PYG{g+go}{ [2.5685016469679423, \PYGZhy{}1.8392747664709928]}
\PYG{g+go}{ [2.5328190736253067, \PYGZhy{}1.8016241802613777]}
\PYG{g+go}{ [2.50901755344256, \PYGZhy{}1.7644428813938213]}
\PYG{g+go}{ [2.492463146144573, \PYGZhy{}1.7276629870471278]}
\PYG{g+go}{ [2.479755297562603, \PYGZhy{}1.691232720755322]}
\PYG{g+go}{ [2.4693682069793006, \PYGZhy{}1.655126365899217]}
\PYG{g+go}{ ⋮}
\PYG{g+go}{ [\PYGZhy{}1.9314924083195961, 1.2406000674798037]}
\PYG{g+go}{ [\PYGZhy{}1.9280746457670834, 1.2228784362638332]}
\PYG{g+go}{ [\PYGZhy{}1.9239746697928601, 1.20530083259782]}
\PYG{g+go}{ [\PYGZhy{}1.9194156513839518, 1.1878723802240516]}
\PYG{g+go}{ [\PYGZhy{}1.9143975905403576, 1.1705930791425252]}
\PYG{g+go}{ [\PYGZhy{}1.9089260501456538, 1.1534632771784905]}
\PYG{g+go}{ [\PYGZhy{}1.9032204549076956, 1.1364869070114587]}
\PYG{g+go}{ [\PYGZhy{}1.897365233815438, 1.1196650574880203]}
\PYG{g+go}{ [\PYGZhy{}1.8913603868688826, 1.1029977286081798]}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }
\PYG{g+go}{\PYGZsh{} Compute the derivatives from the data}
\PYG{g+go}{dict = Dict(params)}
\PYG{g+go}{Dict\PYGZob{}Symbolics.Num, Float64\PYGZcb{} with 5 entries:}
\PYG{g+go}{  iₑ =\PYGZgt{} 0.8}
\PYG{g+go}{  a  =\PYGZgt{} 0.08}
\PYG{g+go}{  d  =\PYGZgt{} 0.7}
\PYG{g+go}{  c  =\PYGZgt{} 0.8}
\PYG{g+go}{  b  =\PYGZgt{} 1.0}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{n}{derivatives}\PYG{+w}{ }\PYG{o}{=}\PYG{+w}{ }\PYG{p}{[}\PYG{p}{[}
\PYG{g+go}{    V̇(V, W, dict[a], dict[b], dict[c], dict[d], dict[iₑ]),}
\PYG{g+go}{    Ẇ(V, W, dict[a], dict[b], dict[c], dict[d], dict[iₑ]),}
\PYG{g+go}{] for (V, W) in solution.u]}
\PYG{g+go}{1001\PYGZhy{}element Vector\PYGZob{}Vector\PYGZob{}Float64\PYGZcb{}\PYGZcb{}:}
\PYG{g+go}{ [\PYGZhy{}5.878999999999999, 0.44800000000000006]}
\PYG{g+go}{ [\PYGZhy{}2.542784436267664, 0.4140104519663151]}
\PYG{g+go}{ [\PYGZhy{}1.303250813070817, 0.39666490831357565]}
\PYG{g+go}{ [\PYGZhy{}0.7336444569286797, 0.386255635449361]}
\PYG{g+go}{ [\PYGZhy{}0.44053054997384367, 0.37919371681157893]}
\PYG{g+go}{ [\PYGZhy{}0.28171380163831095, 0.3739294734267527]}
\PYG{g+go}{ [\PYGZhy{}0.19143614261277864, 0.36964574868460937]}
\PYG{g+go}{ [\PYGZhy{}0.14124373125207734, 0.36596748286258207]}
\PYG{g+go}{ [\PYGZhy{}0.1118377789735121, 0.3626193179333489]}
\PYG{g+go}{ [\PYGZhy{}0.0947262402619482, 0.35947754397589393]}
\PYG{g+go}{ ⋮}
\PYG{g+go}{ [0.02982322904700574, \PYGZhy{}0.17791779698427512]}
\PYG{g+go}{ [0.03823465142975668, \PYGZhy{}0.17651019158225204]}
\PYG{g+go}{ [0.04470307342425839, \PYGZhy{}0.1750572268696893]}
\PYG{g+go}{ [0.04985448119683089, \PYGZhy{}0.17357708444505549]}
\PYG{g+go}{ [0.05371281249499815, \PYGZhy{}0.1720697643083502]}
\PYG{g+go}{ [0.05631866539942876, \PYGZhy{}0.1705357337510757]}
\PYG{g+go}{ [0.05827153029107057, \PYGZhy{}0.16899279844134904]}
\PYG{g+go}{ [0.05980471979317392, \PYGZhy{}0.16744778238446834]}
\PYG{g+go}{ [0.060927821029572726, \PYGZhy{}0.16590068558043414]}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }
\PYG{g+go}{\PYGZsh{} transform vectors of states and derivatives into matrices}
\PYG{g+go}{X = vcat(reshape.(solution.u, 1, 2)...)}
\PYG{g+go}{1001×2 Matrix\PYGZob{}Float64\PYGZcb{}:}
\PYG{g+go}{  3.3      \PYGZhy{}2.0}
\PYG{g+go}{  2.90944  \PYGZhy{}1.95711}
\PYG{g+go}{  2.72499  \PYGZhy{}1.91665}
\PYG{g+go}{  2.62617  \PYGZhy{}1.87753}
\PYG{g+go}{  2.5685   \PYGZhy{}1.83927}
\PYG{g+go}{  2.53282  \PYGZhy{}1.80162}
\PYG{g+go}{  2.50902  \PYGZhy{}1.76444}
\PYG{g+go}{  2.49246  \PYGZhy{}1.72766}
\PYG{g+go}{  2.47976  \PYGZhy{}1.69123}
\PYG{g+go}{  2.46937  \PYGZhy{}1.65513}
\PYG{g+go}{  ⋮        }
\PYG{g+go}{ \PYGZhy{}1.93149   1.2406}
\PYG{g+go}{ \PYGZhy{}1.92807   1.22288}
\PYG{g+go}{ \PYGZhy{}1.92397   1.2053}
\PYG{g+go}{ \PYGZhy{}1.91942   1.18787}
\PYG{g+go}{ \PYGZhy{}1.9144    1.17059}
\PYG{g+go}{ \PYGZhy{}1.90893   1.15346}
\PYG{g+go}{ \PYGZhy{}1.90322   1.13649}
\PYG{g+go}{ \PYGZhy{}1.89737   1.11967}
\PYG{g+go}{ \PYGZhy{}1.89136   1.103}

\PYG{g+gp}{julia\PYGZgt{}}\PYG{+w}{ }\PYG{n}{Ẋ}\PYG{+w}{ }\PYG{o}{=}\PYG{+w}{ }\PYG{n}{vcat}\PYG{p}{(}\PYG{n}{reshape}\PYG{o}{.}\PYG{p}{(}\PYG{n}{derivatives}\PYG{p}{,}\PYG{+w}{ }\PYG{l+m+mi}{1}\PYG{p}{,}\PYG{+w}{ }\PYG{l+m+mi}{2}\PYG{p}{)}\PYG{o}{...}\PYG{p}{)}
\PYG{g+go}{1001×2 Matrix\PYGZob{}Float64\PYGZcb{}:}
\PYG{g+go}{ \PYGZhy{}5.879       0.448}
\PYG{g+go}{ \PYGZhy{}2.54278     0.41401}
\PYG{g+go}{ \PYGZhy{}1.30325     0.396665}
\PYG{g+go}{ \PYGZhy{}0.733644    0.386256}
\PYG{g+go}{ \PYGZhy{}0.440531    0.379194}
\PYG{g+go}{ \PYGZhy{}0.281714    0.373929}
\PYG{g+go}{ \PYGZhy{}0.191436    0.369646}
\PYG{g+go}{ \PYGZhy{}0.141244    0.365967}
\PYG{g+go}{ \PYGZhy{}0.111838    0.362619}
\PYG{g+go}{ \PYGZhy{}0.0947262   0.359478}
\PYG{g+go}{  ⋮          }
\PYG{g+go}{  0.0298232  \PYGZhy{}0.177918}
\PYG{g+go}{  0.0382347  \PYGZhy{}0.17651}
\PYG{g+go}{  0.0447031  \PYGZhy{}0.175057}
\PYG{g+go}{  0.0498545  \PYGZhy{}0.173577}
\PYG{g+go}{  0.0537128  \PYGZhy{}0.17207}
\PYG{g+go}{  0.0563187  \PYGZhy{}0.170536}
\PYG{g+go}{  0.0582715  \PYGZhy{}0.168993}
\PYG{g+go}{  0.0598047  \PYGZhy{}0.167448}
\PYG{g+go}{  0.0609278  \PYGZhy{}0.165901}
\end{Verbatim}
