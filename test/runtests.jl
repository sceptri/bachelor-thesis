using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Noiser
using DataDrivenDynamics.SINDy
using Test, Random, LinearAlgebra

@testset "DataDrivenDynamics.jl" begin

    @testset "General" begin
		# Testing L₂ norm
		x = rand(100)
		y = copy(x)
		y[1] += 1
		@test L₂(x',y')[1] ≈ 1 atol=0.01

		# Testing symbol_at function
		@test symbol_at(:x, 1; prefix = :a) == :(ax__1)
    end

	@testset "Mathematics" begin
		# Testing is_ones function
		@test is_ones(1) == true
		@test is_ones(0) == false
		@test is_ones([1, 1]) == true
		@test is_ones([1, 0]) == false
	end

	@testset "Macros" begin
		testing_variable = 1

		name_by_varname = @varname testing_variable
		@test String(name_by_varname) == "testing_variable"

		name_by_macro, value_by_macro = @varname_and_value testing_variable
		@test String(name_by_macro) == "testing_variable"
		@test value_by_macro == testing_variable
	end

	# Tests about DataDrivenDynamics Makie plot recipes
	include("recipes.jl")

    # Tests about DataDrivenDynamics data utilities
    include("data_utils.jl")

    # Tests about DataDrivenDynamics.SINDy module
    include("SINDy.jl")

    # Tests about DataDrivenDynamics.ProjetKeeper module
    include("ProjectKeeper.jl")

    # Tests about DataDrivenDynamics.Noiser module
    include("Noiser.jl")
end