@testset "SINDy" begin
    @testset "Optimizers" begin
        Random.seed!(12345)
        X = rand(100, 10) # define the input data
        Y = 10 * X[:, 1] - 7 * X[:, 3] + 5 * X[:, 8] + rand(100)
        correct = [10, 0, -7, 0, 0, 0, 0, 5, 0, 0]

        stls = STLS(0.3)
        @test all(isapprox.(stls(X, Y), correct; atol=0.55))

        lasso = LASSO(2, 100)
        @test all(isapprox.(lasso(X, Y; max_iter=1000), correct; atol=0.7))

		dstls = DSTLS(0.3)
		@test all(isapprox.(dstls(X, Y), correct; atol=0.55))
    end

    @testset "Libraries" begin
        for vars in 1:10, degree in 1:5
            @testset "Polynomial library (variables = $vars, degree = $degree)" begin
                lib = PolynomialLibrary(degree, vars)

                functions = collect(poly for poly in lib)
                @test length(functions) == length(lib)
            end
        end

        @testset "Predefined polynomial library comparison test" begin
            lib = PolynomialLibrary(2, 2)
            Θ = lib([2 3])

            @test Θ == [1 3 9 2 6 4]
        end
    end

    @testset "Discovery" begin
        Random.seed!(12345)
        X = rand(100, 3) # define the input data
        Y = [(10 * X[:, 1] - 7 * X[:, 2] + 5 * X[:, 3]) (2 * X[:, 2]) zeros(100)]

        correct = [[0 5 -7 10]' [0 0 2 0]' [0 0 0 0]']

        @testset "Monomial library" begin
            basis = MonomialLibrary(1, 3)
            optimizer = STLS()
            Ξ = discover(X, Y, basis, optimizer)

            @test all(isapprox.(Ξ, correct; atol=0.1))

            io = IOBuffer()
            prettyprint(Ξ, basis; io)
            @test String(take!(io)) == "Found equations for the system are:\n\tẋ = 5.00*z + -7.00*y + 10.00*x\n\tẏ = 2.00*y\n\tż = 0\n"
        end

        @testset "Polynomial library" begin
            basis = PolynomialLibrary(1, 3)
            optimizer = STLS()
            Ξ = discover(X, Y, basis, optimizer)

            @test all(isapprox.(Ξ, correct; atol=0.1))

            io = IOBuffer()
            prettyprint(Ξ, basis; io)
            @test String(take!(io)) == "Found equations for the system are:\n\tẋ = 5.00*z + -7.00*y + 10.00*x\n\tẏ = 2.00*y\n\tż = 0\n"
        end

        @testset "Custom library" begin
            basis = CustomLibrary([x -> 1, x -> x[3], x -> x[2], x -> x[1]])
            optimizer = STLS()
            Ξ = discover(X, Y, basis, optimizer)

            @test all(isapprox.(Ξ, correct; atol=0.1))
        end
    end
end