local common = require "core.common"
local config = require "core.config"

config.plugins.build.targets = {
  { name = "doc-compile" }
}
config.plugins.build.type = "make"
