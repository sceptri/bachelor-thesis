############################################
# USE runner.jl INSTEAD - only for testing #
############################################

# Signals, that test runs should NOT save anything
TEST_DO_SAVES = false

# Only to be used when running tester.jl via console (aka in `julia tester.jl;`)
using Pkg;
Pkg.activate("..");

using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper
using DataDrivenDynamics.Noiser, DataDrivenDynamics.Sparse
using DataDrivenDiffEq, DataDrivenSparse
using LaTeXStrings

# Do you want to see Plots and/or console output?
# By default, all is shown
# Only console output (uncomment line below)⬇
# VERBOSE = OnlyConsole
# Only plots (uncomment line below)⬇
# VERBOSE = OnlyPlots
# Nothing (uncomment line below)⬇
# VERBOSE = NoOutput

# Set desired parameters
PARAMETERS = Dict(
    # --- Variables used in Generator ---
    # Parameters of FHN model
    "params" => [
        :a => 0.08,
		:b => 1.0,
		:c => 0.8,
		:d => 0.7,
		:iₑ => 0.8
    ],
    # Initial condition of the FHN model
    "starting_values" => [3.3, -2.0],
    # Time range on which to base trajectory of FHN,
	# which is then sampled for different ICs
    "full_time" => (0.0, 100.0),
    # Time intervals between each measuremnt of the trajectory
    "saveat" => 0.01,
	# Time range for each of the simulated trajectories
	"time_range" => (0.0, 100.0),
	# Number of different trajectories taken
	"ic_count" => 3,

    # --- Variables used in Learner ---
	# Number of trajectories to do an average on (taken from data)
	# Must be smaller or equal to `ic_count` setting above! 
	"trajectory_count" => 3,
	# Type of the noise that will effect the trajectory
	"noise_type" => AdditiveVarianceNoise,
	# Range of noise σ used for this experiment
	"noise_range" => 0.001:0.01:0.1,

	# Parameters of total variation denoising of the trajectory
	"λ" => [0.3, 0.3],
    "k" => [4, 5], 

	# Parameters of total variation reg. differentiation
	"number_of_iterations" => [30, 30],
    "regularization" => [0.08, 0.9],

	# Different optimizers to be used in this experiment 
	"optimizers" => [
		STLSQ(0.1), 
		SR3(0.03, 1.0), 
		ADMM(0.025, 1.0), 
		DSTLS(0.1)
	],
	# Labels for plotting legend given the selected optimizers
	"labels" => [
		"STLS",
		"SR3",
		L"\text{LASSO, } \mu = 0.025",
		"DSTLS"
	],

	# Macro @later makes it a expression, which is evaluated
	# once the variable `variables` is known (do not remove it)
	# Define the library to be used with SINDy
	"term_library" => variables -> polynomial_basis(variables, 3)
)

# Generate `data.csv` file in the current directory?
GENERATE_DIRECTLY = true
# If not, you have to supply "your own" `data.csv` in the same
# directory as the `learner.jl`
# >	Every time `generator.jl` runs, it creates CSV file in datasets folder
# > so you can take any CSV from there and simply rename it to `data.csv` 

# Comment out (with '#') files you do NOT want to run
include("generator.jl")
include("learner.jl")
