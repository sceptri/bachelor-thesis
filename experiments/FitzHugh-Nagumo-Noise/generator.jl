#########################################
#		Program intilizatization	 	#
#########################################

###
#	Data generator file for 2D FitzHugh-Nagumo for noise comparisons
#	The system is given by equations
#
#		V̇ = V - V³ / 3 - W + iₑ
#		Ẇ = a ⋅ (b ⋅ V - c ⋅ W + d)
#
#	Purpose of this is to generate an ensemble of trajectories
#	that are to be used to explore behavior of SINDy on different
#	noise levels
###

using Revise
using ModelingToolkit, DifferentialEquations
using CairoMakie, LaTeXStrings
using DataFrames, CSV, Random

# My own packages
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator

# Define the naming scheme used through out the script
naming = Naming()
push!(naming, NamingField(:params => :(parse_param_vector(params)); subdirectory_behavior=ProjectKeeper.make_subdirectory))
push!(naming, NamingField(:starting_values; include_key=true))
push!(naming, NamingField(:full_time; include_key=true))
push!(naming, NamingField(:time_range; include_key=true))
push!(naming, NamingField(:saveat; include_key=true))
push!(naming, NamingField(:ic_count; include_key=true))

Random.seed!(12345)

#########################################
#		Setting up the system	 		#
#########################################

# Setting up independent variable namely time t and dependent variable V(t), W(t)
@variables t V(t) W(t)
@parameters a b c d iₑ

# Our system is one ODE, so we only need differential with respect to time
D = Differential(t)

# Governing equation(s) for our system
V̇(V, W, a, b, c, d, iₑ) = V - V^3 / 3 - W + iₑ
Ẇ(V, W, a, b, c, d, iₑ) = a * (b * V - c * W + d)

@overrides params = [
    :a => 0.08,
    :b => 1.0,
    :c => 0.8,
    :d => 0.7,
    :iₑ => 0.8
]
@overrides starting_values = [3.3, -2.0]
@overrides full_time = (0.0, 100.0)

#########################################
#		Solving the system		 		#
#########################################

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(V) ~ V̇(V, W, a, b, c, d, iₑ),
    D(W) ~ Ẇ(V, W, a, b, c, d, iₑ)
])

# Solve the system for a long time to get a trajactory, from which we can sample
# desired initial conditions
@overrides saveat = 0.01
ode_problem = ODEProblem(ode_system, starting_values, full_time, ode_params(params))
solution = solve(ode_problem, Tsit5(); saveat)

@overrides time_range = (0.0, 100.0)
@overrides ic_count = 10
initial_conditions = rand(solution.u, ic_count)

ode_problem = ODEProblem(ode_system,
    initial_conditions[1],
    time_range,
    ode_params(params)
)

"""
EnsembleProblem function, which allows us to change starting value based on current index
"""
function problem_function(problem, index, repeat)
    @. problem.u0 = initial_conditions[index]
    problem
end

ensemble_problem = EnsembleProblem(ode_problem; prob_func=problem_function)
solution = solve(ensemble_problem; saveat, trajectories=length(initial_conditions))

#########################################
#	Plotting the solved trajectory	 	#
#########################################

# For ease of handling, we create a temporary object to hide away saving logic
data_to_save = feedSaveData(
    solution,
    (:V, :W), # names of variables
    (coords, t) -> [
        V̇(coords..., values(params)...),
        Ẇ(coords..., values(params)...)
    ]
)

#########################################
#	Saving the solved trajectories	 	#
#########################################

dataframe = DataFrame(prepareSaveData(data_to_save, true)...)
not_test_suite() && CSV.write(naming("data"; filetype="csv", directory="datasets", as_directory=false), dataframe)

if isdefined(Main, :GENERATE_DIRECTLY) && GENERATE_DIRECTLY && not_test_suite()
    CSV.write(here("data.csv"), dataframe)
end