############################################
# USE runner.jl INSTEAD - only for testing #
############################################

# Signals, that test runs should NOT save anything
TEST_DO_SAVES = false

# Only to be used when running tester.jl via console (aka in `julia tester.jl;`)
using Pkg;
Pkg.activate("..");

using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper
using DataDrivenDynamics.Noiser, DataDrivenDynamics.Sparse
using DataDrivenDiffEq, DataDrivenSparse
using LaTeXStrings

# Do you want to see Plots and/or console output?
# By default, all is shown
# Only console output (uncomment line below)⬇
# VERBOSE = OnlyConsole
# Only plots (uncomment line below)⬇
# VERBOSE = OnlyPlots
# Nothing (uncomment line below)⬇
# VERBOSE = NoOutput


#####################################################################
#			IT TAKES A LONG TIME TO RUN (approx. 3h)				#
#####################################################################


# Set desired parameters
PARAMETERS = Dict(
    # Period length for a given model (and parameters)
    # For Lorenz model with chaotic parameters, it is approx. 0.759
    "period_length" => 0.759,

    # Time, from which all trajectories are simulated
    "starting_time" => 0.0,

    # Range of how much of period (determined by a period length)
    # should be used for discovery
    "period_parts" => 0.1:0.05:2.5,

    # Range of how many samples should be taken per period
    # Number of samples per period is calculated as `2^(current_sample_count)`
    "samples" => 4:0.25:15,

    # Tolerance for parameter values of a found model
    # compared to the ground truth
    "parameter_tolerance" => 1e-2,

    # In this experiment, polynomial library for SINDy is used
    # This determines maximum order of used polynomials
    "library_power" => 5,

    # Referential parameters used for initial trajectory 
    # and the selection of initial conditions
    "params" => [
        :σ => 10,
        :ρ => 28,
        :β => 8 / 3
    ],

    # Maximum time, up to which simulate a given trajectory
    "full_time" => 30.0,

    # Initial condition used to generate a sampling trajectory 
    # for the random selection of initial conditions
    "base_initial_cond" => [-20, -20, 25],

    # For each pair of sampling rate and duration, how many 
    # trajectories to simulate and try to learn on
    "ic_count" => 10,

    # Determines optimizer to be used with SINDy
    "optimizer" => STLSQ(0.1)
)

# Should we save found heatmap in `probability_matrix.dat`?
SAVE_DATA_FOR_LATER = true

# Code generating the figure itself
include("generator_and_learner.jl")
