#########################################
#		Program intilizatization	 	#
#########################################

###
#	Learner file for 
# 				3D Lorenz Sampling rate and duration experiment
# 	Purpose of this is explore how sampling rate and duration affects the quality of SINDy fit
# 	and whether the model will or will not be found
###

using Revise
using ModelingToolkit, DifferentialEquations
using DataDrivenDiffEq, DataDrivenSparse
using CairoMakie, ColorSchemes, LaTeXStrings
using DataFrames, CSV, Random
using Printf
# Due the computing cost of creating the probability matrix
# we serialize it for use later
using Serialization

# My own packages
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Noiser

naming = Naming([:starting_time, :period_parts, :samples, :base_initial_cond, :full_time, :ic_count, :parameter_tolerance, :library_power])

# https://github.com/kpchamp/MultiscaleDiscovery/blob/master/01_sindy_uniscale.ipynb
# Predetermined period length -- time which it takes to travel around one of the lobes
@overrides period_length = 0.759

@overrides starting_time = 0.0
@overrides period_parts = 0.1:0.05:2.5
@overrides samples = 4:0.25:15

@overrides parameter_tolerance = 1e-2
@overrides library_power = 5

# Setting up independent variable namely time t and dependent variable V(t), W(t)
@variables t x(t) y(t) z(t)
@parameters σ, ρ, β

D = Differential(t)

# Governing equations for our system
ẋ(x, y, z, σ) = σ * (y - x)
ẏ(x, y, z, ρ) = x * (ρ - z) - y
ż(x, y, z, β) = x * y - β * z

@overrides params = [
    :σ => 10,
    :ρ => 28,
    :β => 8 / 3
]
dict = Dict(ode_params(params))

@overrides full_time = 30.0
@overrides base_initial_cond = [-20, -20, 25]
@overrides ic_count = 10

correct_params = [-dict[σ], dict[σ], dict[ρ], -1, -1, 1, -dict[β]]

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(x) ~ ẋ(x, y, z, σ),
    D(y) ~ ẏ(x, y, z, ρ),
    D(z) ~ ż(x, y, z, β)
])

# Calculate the full trajectory
ode_problem = ODEProblem(ode_system, base_initial_cond, (starting_time, full_time), ode_params(params))
sol = solve(ode_problem, Rosenbrock23(); saveat=0.01)
full_trajectory = vcat(reshape.(sol.u, 1, 3)...)

# then sample it for random selected initial conditions
initial_conditions = rand(sol.u, ic_count)

successfully_found = zeros(Int8, length(period_parts), length(samples))

@overrides optimizer = STLSQ(0.1)

# For each sampling rate, sampling duration and initial condition, simulate the Lorenz system, try to learn it and see, if the attempt was successful
for (time_index, period_part) in enumerate(period_parts), (Δt_index, sample_count) in enumerate(samples), (ic_index, initial_condition) in enumerate(initial_conditions)
	# Set end time of trajectory and sampling rate
	end_time = period_part * period_length
	saveat = period_length / (2^sample_count)

    @show time_index, Δt_index, ic_index
    time_range = (starting_time, end_time)
    ode_problem = ODEProblem(ode_system, initial_condition, time_range, ode_params(params))

    # The ODE problem may be stiff, so use a stiff solver
    ode_solution = solve(ode_problem, Rosenbrock23(); saveat)

    # Compute the derivatives from the data
    derivatives = [[
        ẋ(x, y, z, dict[σ]),
        ẏ(x, y, z, dict[ρ]),
        ż(x, y, z, dict[β])
    ] for (x, y, z) in ode_solution.u]

    # transform vectors of states and derivatives into matrices
    X = vcat(reshape.(ode_solution.u, 1, 3)...)
    Ẋ = vcat(reshape.(derivatives, 1, 3)...)
    times = ode_solution.t

    @named data_driven_problem = ContinuousDataDrivenProblem(X', times, Ẋ')
    @variables x, y, z

	term_library = polynomial_basis([x, y, z], library_power)

    @named model_basis = Basis(term_library, [x, y, z])

	# Checks, whether the found solution is indeed a correct one
    try
        solution = solve(data_driven_problem, model_basis, optimizer)

        fitted_system = get_basis(solution)
        fitted_parameters = get_parameter_map(fitted_system)

        converged = is_converged(solution)
        all_eqs = length(fitted_system) == 3
        all_params = dof(solution) == 7
        params_values = all_params ? all(isapprox.(get_parameter_values(fitted_system), correct_params; atol=parameter_tolerance)) : false

        successfully_found[time_index, Δt_index] += converged && all_eqs && all_params && params_values
    catch
        successfully_found[time_index, Δt_index] += false
    end
end

# Convert the matrix of absolute occurences of successfully found model into a "probability matrix"
probability_matrix = copy(successfully_found)
probability_matrix = @. probability_matrix / ic_count

"""
	function superscriptnumber(i::Int)

function that converts an integer `i` into a superscriptnumber (like 2 -> ²)
"""
function superscriptnumber(i::Int)
    if i < 0
        c = [Char(0x207B)]
    else
        c = []
    end
    for d in reverse(digits(abs(i)))
        if d == 0
            push!(c, Char(0x2070))
        end
        if d == 1
            push!(c, Char(0x00B9))
        end
        if d == 2
            push!(c, Char(0x00B2))
        end
        if d == 3
            push!(c, Char(0x00B3))
        end
        if d > 3
            push!(c, Char(0x2070 + d))
        end
    end
    return join(c)
end

# Bruteforce find, where the selected powers of 2 lie on the x-axis
x_indices = findall(x -> (x == 4 || x == 8 || x == 11 || x == 15), samples)

# Log the probability matrix as a heatmap
if log_plot()
	resolution = (500, 250)
	colormap = :Nizami

	figure = Figure(; resolution)
	axis = Axis(figure[1, 1];
		title = L"\text{\textbf{Sampling rate and duration effect on SINDy}}",
		palette=fetch_colors(colormap, 2),
		xlabel=L"\text{sampling rate (samples per period)}",
		ylabel=L"\text{trajectory duration (periods)}",
		yticks=(
			range(1, length(period_parts); length=4), 
			[sprintf("%2.1f", x) for x in range(collect(period_parts)[1], collect(period_parts)[end]; length=4)]
		),
		xticks=(
			x_indices, 
			["2" * superscriptnumber(Int64.(round(x))) for x in collect(samples)[x_indices]]
		)
	)
	Colorbar(figure[1, 2]; 
		label=L"P(\text{model identified})",
		width=15, 
		colormap=cgrad(:Nizami, ic_count; categorical = true), 
		ticks=([0.05, 0.95], ["0", "1"]), 
		ticksvisible=false, 
		ticklabelrotation=π / 2
	)
	hmap = heatmap!(axis, probability_matrix'; colormap)

	display(figure)

	filesave(naming("sampling_rate_and_duration"), figure)
end

# Save data incase we need to use them later
if isdefined(Main, :SAVE_DATA_FOR_LATER) && SAVE_DATA_FOR_LATER && not_test_suite()
	serialize(fix_name(naming("probability_matrix"; filetype = "dat", directory = "data", as_directory=false)), probability_matrix)
	# test that is was saved correctly
	@assert deserialize(fix_name(naming("probability_matrix"; filetype = "dat", directory = "data", as_directory=false))) == probability_matrix
end