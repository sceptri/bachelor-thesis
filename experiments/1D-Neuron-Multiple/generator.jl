#########################################
#		Program intilizatization	 	#
#########################################

###
#	Data generator file for 1D Neuron experiment
#	The system is given by equation
#
#		ẋ = k ⋅ x ⋅ (a - x) ⋅ (x - 1) + I
#
#	Purpose of this is to generate mutlitple trajectories
###

using Revise
using ModelingToolkit, DifferentialEquations
using CairoMakie, LaTeXStrings
using DataFrames, CSV, Random

# My own packages
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator

# Define the naming scheme used through out the script
naming = Naming([:params => :(parse_param_vector(params)),])
push!(naming, NamingField(:starting_values; include_key=true))
push!(naming, NamingField(:time_range; include_key=true))
push!(naming, NamingField(:saveat; include_key=true))

Random.seed!(12345)

#########################################
#		Setting up the system	 		#
#########################################

# Setting up independent variable namely time t and dependent variable x(t) 
@variables t x(t)
@parameters k a I

# Our system is one ODE, so we only need differential with respect to time
D = Differential(t)

# Governing equation(s) for our system
ẋ(x, k, a, I) = k * x * (a - x) * (x - 1) + I

params = [
    :k => 1.0,
    :a => 0.5,
    :I => 1.0
]

starting_values = [
    [-1.0],
    [4.0],
    [-10.0],
    [10.0],
    [11.0],
    [-11.0],
    [-30],
    [30]
]

time_range = (0.0, 2.0)

# For better illustration of the behaviour of a given system
# we draw square phase portrait
if log_plot()
    f(x, t) = Point2f(ẋ(x, values(params)...), 1)

    lower_limit = -10
    upper_limit = 10

    # Drawing the vector fieĺd, default arrow_size = 0.07
    streamplot(f, lower_limit .. upper_limit, lower_limit .. upper_limit, colormap=:Nizami, arrow_size=10)
end

#########################################
#		Solving the system		 		#
#########################################

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([D(x) ~ ẋ(x, k, a, I)])

# We put starting_values[1] to ODE problem temporarily, because we'll need to create an EnsembleProblem
ode_problem = ODEProblem(ode_system,
    starting_values[1],
    time_range,
    ode_params(params)
)

"""
EnsembleProblem function, which allows us to change starting value based on current index
"""
function problem_function(problem, index, repeat)
    @. problem.u0 = starting_values[index]
    problem
end

ensemble_problem = EnsembleProblem(ode_problem; prob_func=problem_function)

# Defines how much time passes between each save
saveat = 0.05

# TODO: Implement saving derivative as a callback
# saved_derivatives = SavedValues(Float64, Array{Float64})
# solver_callback = SavingCallback((u, t, integrator) -> integrator(t, Val{1}), saved_derivatives)

solution = solve(ensemble_problem; saveat, trajectories=length(starting_values))

#########################################
#	Plotting the solved trajectory	 	#
#########################################

# For ease of handling, we create a temporary object to hide away saving logic
data_to_save = feedSaveData(
    solution,
    (:x,), # names of variables
    (x, t) -> ẋ(x, values(params)...)
)

if log_plot()
    figure = generated_trajectory_plot(data_to_save; legend_outside=true, resolution=(450, 300))
    display(figure)

    filesave(naming("generated-trajectory"), figure)
end

#########################################
#	Saving the solved trajectories	 	#
#########################################

dataframe = DataFrame(prepareSaveData(data_to_save, true)...)
not_test_suite() && CSV.write(naming("data"; filetype="csv", directory="datasets", as_directory=false), dataframe)