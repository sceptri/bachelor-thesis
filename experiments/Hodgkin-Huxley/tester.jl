############################################
# USE runner.jl INSTEAD - only for testing #
############################################

# Signals, that test runs should NOT save anything
TEST_DO_SAVES = false

# Only to be used when running tester.jl via console (aka in `julia tester.jl;`)
using Pkg;
Pkg.activate("..");

using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper
using DataDrivenDynamics.Noiser, DataDrivenDynamics.Sparse
using DataDrivenDiffEq, DataDrivenSparse

# Do you want to see Plots and/or console output?
# By default, all is shown
# Only console output (uncomment line below)⬇
# VERBOSE = OnlyConsole
# Only plots (uncomment line below)⬇
# VERBOSE = OnlyPlots
# Nothing (uncomment line below)⬇
# VERBOSE = NoOutput

# Set desired parameters
PARAMETERS = Dict(
    # --- Variables used in Generator ---
    # Parameters of HH model
    "params" => [
        :Cₘ => 1, # Or Cₘ => 5
		:Iₑ => 80, # 0 or 80 or 170 or 9.78 or 154.527
		:gₙ => 120,
		:Vₙ => 50,
		:gₖ => 36,
		:Vₖ => -77,
		:gₗ => 0.3,
		:Vₗ => -54.402
    ],
    # Initial condition of the HH model
    "starting_values" => [15.0, 0.5, 0.5, 0.5],
    # Time range on which to simulate the HH model
    "time_range" => (0.0, 30),
    # Time intervals between each measuremnt of the trajectory
    "saveat" => 0.25,

    # --- Variables used in Learner ---
	# Noise that will effect the trajectory
	# First value is μ, second is σ (or σ̂)
    "noise" => AdditiveVarianceNoise(0, 0.05), 
	# Parameters of total variation denoising of the trajectory
	"λ" => [0.35, 0.9, 0.9, 0.9],
    "k" => [4, 15, 15, 15], 

	# Which trajectory do you wish to use? Options are
	# - :true_trajectory
	# - :noisy_trajectory
	# - :denoised_trajectory
	"used_trajectory" => :denoised_trajectory,

	# Parameters of total variation reg. differentiation
	"number_of_iterations" => [30, 30, 30, 30],
    "regularization" => [0.2, 0.8, 0.8, 0.8],

	# Which derivative do you wish to use? Options are
	# - :true_derivative		(derivative calculated from the original system)
	# - :tvdiff_df				(derivative calculated from used data with TVDIFF)
	# - :forward_df				(derivative calculated from used data with forward differences)
	# -	:smooth_forward_df		(derivative calculated from true data with forward differences)
	# - :collocation_df			(derivative calculated from used data by collocation kernel)
	"derivative_df" => :tvdiff_df,

	# Collocation kernel to be used with :collocation_df derivative option (otherwise it is NOT used)
	# ⬇ Uncomment line below (options are here:https://docs.sciml.ai/DataDrivenDiffEq/stable/utils/#DataDrivenDiffEq.collocate_data)
	#"collocation_kernel" => GaussianKernel(),

	# Macro @later makes it a expression, which is evaluated
	# once the variable `variables` is known (do not remove it)
	# Define the library to be used with SINDy
	"term_library" => @later(polynomial_basis(variables, 3)),
	# Choose and optimizer (choices are e.g. `STLSQ()`, `ADMM()`, `SR3()` or our custom `DSTLS()`)
	# More here: https://docs.sciml.ai/DataDrivenDiffEq/stable/libs/datadrivensparse/sparse_regression/#sparse_algorithms
	"optimizer" => DSTLS(0.25)
)

# Generate `data.csv` file in the current directory?
GENERATE_DIRECTLY = true
# If not, you have to supply "your own" `data.csv` in the same
# directory as the `learner.jl`
# >	Every time `generator.jl` runs, it creates CSV file in datasets folder
# > so you can take any CSV from there and simply rename it to `data.csv` 

# Comment out (with '#') files you do NOT want to run
# Alternatively, you can use `nogen` or `nolearn` command line options like so:
# `julia runner.jl nogen`
do_unless("nogen") && include("generator.jl")
do_unless("nolearn") && include("learner.jl") 
