#########################################
#		Program intilizatization	 	#
#########################################

###
#	Data generator file for 4D Hodgkin-Huxley
###

using Revise
using ModelingToolkit, DifferentialEquations
using CairoMakie, LaTeXStrings
using DataFrames, CSV, Random

# My own packages
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator

# Define the naming scheme used through out the script
naming = Naming(; directory = ["figures", "hodgkin_huxley"])
push!(naming, NamingField(:params => :(parse_param_vector(params)); subdirectory_behavior=ProjectKeeper.make_subdirectory))
push!(naming, NamingField(:starting_values; include_key=true))
push!(naming, NamingField(:time_range; include_key=true))
push!(naming, NamingField(:saveat; include_key=true))

Random.seed!(12345)

#########################################
#		Setting up the system	 		#
#########################################

# Setting up independent variable namely time t and dependent variable V(t), W(t)
@variables t V(t) m(t) h(t) n(t)
@parameters Cₘ, Iₑ, gₙ, Vₙ, gₖ, Vₖ, gₗ, Vₗ

# Our system is one ODE, so we only need differential with respect to time
D = Differential(t)

# Helper functions for our system
# For m
αₘ(V) = 0.1 * (25 - V) / (exp((25 - V) / 10) - 1)
βₘ(V) = 4 * exp(-V/18)
# For h
αₕ(V) = 0.07 * exp(-V/20)
βₕ(V) = 1/(exp((30 - V)/10) + 1)
# For n
αₙ(V) = 0.01 * (10 - V) / (exp((10 - V) / 10) - 1)
βₙ(V) = 0.125 * exp(-V/80)

# Governing equation(s) for our system
V̇(V, m, h, n, Cₘ, Iₑ, gₙ, Vₙ, gₖ, Vₖ, gₗ, Vₗ) = 1 / Cₘ * (Iₑ - gₙ * m^3 * h * (V - Vₙ) - gₖ * n^4 * (V - Vₖ) - gₗ * (V - Vₗ))
ṁ(V, m, h, n) = αₘ(V) * (1 - m) - βₘ(V)*m
ḣ(V, m, h, n) = αₕ(V) * (1 - h) - βₕ(V)*h
ṅ(V, m, h, n) = αₙ(V) * (1 - n) - βₙ(V)*n

# Parameter values taken from https://doi.org/10.1017/CBO9780511815706 (for us Gerstner2002-it)
@overrides params = [
    :Cₘ => 1, # 1 Or Cₘ => 5
	:Iₑ => 9, # 0 or 80 or 170 or 9.78 or 154.527
	:gₙ => 120,
	:Vₙ => 115,
	:gₖ => 36,
	:Vₖ => -12,
	:gₗ => 0.3,
	:Vₗ => 10.6
]
@overrides starting_values = [-20, 0.5, 0.5, 0.5]
@overrides time_range = (0.0, 100.0)

#########################################
#		Solving the system		 		#
#########################################

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(V) ~ V̇(V, m, h, n, Cₘ, Iₑ, gₙ, Vₙ, gₖ, Vₖ, gₗ, Vₗ),
	D(m) ~ ṁ(V, m, h, n),
	D(h) ~ ḣ(V, m, h, n),
	D(n) ~ ṅ(V, m, h, n)
])

ode_problem = ODEProblem(ode_system, starting_values, time_range, ode_params(params))

# We expect the ODE problem to be stiff, so by default we use
# the Rosenbrock23 method
@overrides saveat = 0.25
solution = solve(ode_problem, Rosenbrock23(); saveat)

#########################################
#	Plotting the solved trajectory	 	#
#########################################

# For ease of handling, we create a temporary object to hide away saving logic
data_to_save = feedSaveData(
    solution,
    (:V, :m, :h, :n), # names of variables
    (coords, t) -> [
        V̇(coords..., values(params)...),
        ṁ(coords...),
		ḣ(coords...),
		ṅ(coords...)
    ]
)

if log_plot()
    figure = generated_trajectory_plot(data_to_save; legend_outside=true, resolution=(325, 300))
    display(figure)

    filesave(naming("generated-trajectory"; as_directory = false), figure)
end

#########################################
#	Saving the solved trajectories	 	#
#########################################

dataframe = DataFrame(prepareSaveData(data_to_save, true)...)
not_test_suite() && CSV.write(naming("data"; filetype="csv", directory=["datasets", "hodgkin_huxley"], as_directory=false), dataframe)

if isdefined(Main, :GENERATE_DIRECTLY) && GENERATE_DIRECTLY && not_test_suite()
	@overrides csv_name = "hodgkin_huxley.csv"
    CSV.write(here(csv_name), dataframe)
end