#########################################
#		Program intilizatization	 	#
#########################################

using Revise
using ModelingToolkit, DifferentialEquations
using LinearAlgebra, Random, Statistics
using DataFrames, CSV
using CairoMakie, ColorSchemes, LaTeXStrings

using ForwardDiff, ForwardDiffChainRules
using Zygote
# using Optim, ImplicitDifferentiation
using Metaheuristics

# My own packages
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper

include("hodgkin_huxley_ode.jl")

Random.seed!(12345)
naming = Naming([:shown_case])
push!(naming, NamingField(:μ; include_key=true))
push!(naming, NamingField(:α; include_key=true))
push!(naming, NamingField(:β; include_key=true))

# Extract Hodgkin-Huxley (HH) data from its CSV
hh_variables = (:V, :m, :h, :n)

@overrides csv_name_inifite = "hodgkin_huxley_infinite_train.csv"
@overrides csv_name_finite = "hodgkin_huxley_finite_train.csv"

hh_time_inf, hh_trajectory_inf, hh_derivatives_inf = extract_data(here(csv_name_inifite), hh_variables)
hh_time_fin, hh_trajectory_fin, hh_derivatives_fin = extract_data(here(csv_name_finite), hh_variables)

#################################################
#		Useful function definitions				#
#################################################

get_initial_cond(Λ, u_obs) = (Λ*u_obs)[:, 1]
distribute(bound, times) = bound .* ones(times)

absinf(x) = max(abs.(x))

to_state(p, Λ) = [p..., Λ...]
from_state(p_Λ) = (p_Λ[1:5], reshape(p_Λ[6:end], (2, 4)))

L₂_loss(estimate, truth; func=abs2) = sum(func, estimate .- truth)
R²_loss(estimate, truth; func=abs2) = L₂_loss(estimate, truth; func) / L₂_loss(truth, mean(truth); func)

function ode_loss(estimate, truth; func=abs2)
    return mean(func, estimate .- truth) + # calculate "residuals" of fitted trajectory
           α * abs(1 - std(estimate) / std(truth)) + # check that it has the same standard deviation
           β * abs(1 - std(estimate[:, 1:(end÷2)]) / std(estimate[:, (end÷2+1):end]))
    # check that estimate st. deviation is the same across the data
end

function partial_ode_loss(estimate, truth; func=abs2)
    return mean(func, estimate .- truth) + # calculate "residuals" of fitted trajectory
           α * abs(1 - std(estimate) / std(truth)) # check that it has the same standard deviation
end

#################################################
#		Infinite train in HH model				#
#################################################

u_used = hh_trajectory_inf
du_used = hh_derivatives_inf
t_used = hh_time_inf

u_used_fin = hh_trajectory_fin
du_used_fin = hh_derivatives_fin
t_used_fin = hh_time_fin

@overrides μ = 0
@overrides α = 1e3
@overrides β = 1e2

shown_case = "infinite_train"

Λ_bound = 5
p_upper = 1
p_lower = 0.1
iₑ_bound = 1

constraints = BoxConstrainedSpace(
	lb = [distribute(p_lower, 4)..., -iₑ_bound, distribute(-Λ_bound, 8)...],
	ub = [distribute(p_upper, 4)..., iₑ_bound, distribute(Λ_bound, 8)...]
)

# Define the differential equation
function FHN(u, p, t)
    V, W = u
    a, b, c, d, iₑ = p
    dV = V - V^3 / 3 - W + iₑ
    dW = a * (b * V - c * W + d)
    return [dV, dW]
end

function linear_transform_cost(p_Λ, u_obs, du_obs, t_obs)
    p, Λ = from_state(p_Λ)
	IC = get_initial_cond(Λ, u_obs)

    ode = ODEProblem(FHN, IC, (t_obs[1], t_obs[end]), p)
    sol = solve(ode, Rosenbrock23(); saveat=t_obs)

    trajectory = hcat(reshape.(sol.u, 2, 1)...)
    transformed = (Λ*u_obs)[:, 1:size(trajectory, 2)]

    return ode_loss(trajectory, transformed) + μ * norm(p)
end

# Find the optimal parameters
result = optimize(p_Λ -> linear_transform_cost(p_Λ, u_used, du_used, t_used), 
	constraints, 
	WOA(N = 600)
)
println(result)
p_optimal, Λ_optimal = from_state(minimizer(result))
IC_optimal = get_initial_cond(Λ_optimal, u_used)

prob = ODEProblem(FHN, IC_optimal, (t_used[1], t_used[end]), p_optimal)
sol = solve(prob, Rosenbrock23(), saveat=t_used)
# we can use any t we desire, as FHN is autonomous
sol_dt = [FHN(u, p_optimal, 0) for u in sol.u]

trajectory = hcat(reshape.(sol.u, 2, 1)...)
trajectory_dt = hcat(reshape.(sol_dt, 2, 1)...)

u_part = u_used[:, 1:size(trajectory, 2)]
du_part = du_used[:, 1:size(trajectory_dt, 2)]
t_part = t_used[1:size(trajectory, 2)]

# Printing useful info
r_squared = 1 - R²_loss(trajectory, Λ_optimal * u_part)
println("Optimal parameters: ", p_optimal)
println("R-squared: ", r_squared)

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_part, t_part; legend_outside=true, legend_onside=true)
    display(figure)

    filesave(naming("linear_transformation_comparison"), figure)
end

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_part, t_part; 
		legend_outside=true, 
		legend_onside=false,
		show_title=true,
		title=L"\text{\textbf{Linear - Infinite train}}",
		position=:rb,
		nbanks=2,
		resolution=(400,345)
	)
    display(figure)

    filesave(naming("linear_transformation_comparison", including=["pres"]), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_part, trajectory, -6 .. 6, -2.5 .. 4, f;
            legend_outside=true,
            shift_colors=true
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_part, trajectory, -6 .. 6, -2.5 .. 4, f;
            legend_outside=false,
            shift_colors=true,
			show_legend=true,
            title=L"\text{\textbf{Linear - Infinite train}}",
			resolution=(400,345),
			position=:rb,
			native_label=L"\text{native FHN}",
			transform_label=L"\text{lin. transf. HH}"
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"; including=["pres"]), figure)
end

if log_console()
	filesave_text(sprintf("%5.3f", r_squared), naming("r_squared"; filetype="txt"))

    filesave_text(to_tex_matrix(Λ_optimal'; decimals=3), naming("optimal_values"; including=["Λ"], filetype="txt"))
    filesave_text(to_tex_vector(p_optimal; decimals=3), naming("optimal_values"; including=["p"], filetype="txt"))
    filesave_text(
		to_tex_vector(IC_optimal; 
			decimals=3, 
			use_brackets = false, 
			borders=('[',']')
		), 
		naming("optimal_values"; including=["IC"], filetype="txt")
	)
end

#################################################
#		Finite train in HH model				#
#################################################

shown_case = "finite_train"

@overrides αₛ = 1.0
α = αₛ

to_reduced_state(p, IC) = [p..., IC...]
from_reduced_state(p_IC) = (p_IC[1:5], p_IC[6:7])

function ode_cost(p_IC, Λ, u_obs, du_obs, t_obs)
    p, IC = from_reduced_state(p_IC)

    ode = ODEProblem(FHN, IC, (t_obs[1], t_obs[end]), p)
    sol = solve(ode, Rosenbrock23(); saveat=t_obs)

    trajectory = hcat(reshape.(sol.u, 2, 1)...)
    transformed = (Λ*u_obs)[:, 1:size(trajectory, 2)]

    return partial_ode_loss(trajectory, transformed) + μ * norm(p)
end

result = optimize(p_IC -> ode_cost(p_IC, Λ_optimal, u_used_fin, du_used_fin, t_used_fin),
    to_reduced_state(p₀, IC₀),
    Optim.Options(iterations=optim_iterations)
)
p_reduced_optimal, IC_reduced_optimal = from_reduced_state(result.minimizer)

prob = ODEProblem(FHN, IC_reduced_optimal, (t_used_fin[1], t_used_fin[end]), p_reduced_optimal)
sol = solve(prob, Rosenbrock23(), saveat=t_used_fin)

trajectory = hcat(reshape.(sol.u, 2, 1)...)
u_part = u_used_fin[:, 1:size(trajectory, 2)]
t_part = t_used_fin[1:size(trajectory, 2)]

ode_loss(trajectory, Λ_optimal * u_part)
mean(abs2, trajectory .- Λ_optimal * u_part)
α * abs(1 - std(trajectory) / std(Λ_optimal * u_part))
β * abs(1 - std(trajectory[:, 1:(end÷2)]) / std(trajectory[:, (end÷2+1):end]))
μ * norm(p_reduced_optimal)

r_squared = 1 - R²_loss(trajectory, Λ_optimal * u_part)
println("Optimal parameters: ", p_optimal)
println("R-squared: ", r_squared)

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_part, t_part; legend_outside = true, legend_onside = true)
    display(figure)

    filesave(naming("linear_transformation_comparison"), figure)
end

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_part, t_part;
        legend_outside=false,
        legend_onside=false,
        show_title=true,
        title=L"\text{\textbf{Linear - Finite train}}",
        position=:rb,
        nbanks=2,
        resolution=(400, 345)
    )
    display(figure)

    filesave(naming("linear_transformation_comparison", including=["pres"]), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_reduced_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_part, trajectory, -6 .. 6, -2.5 .. 4, f;
            legend_outside=true,
            shift_colors=true
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_reduced_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_part, trajectory, -6 .. 6, -2.5 .. 4, f;
            legend_outside=false,
            shift_colors=true,
            show_legend=true,
            title=L"\text{\textbf{Linear - Finite train}}",
            resolution=(400, 345),
            position=:rb,
            native_label=L"\text{native FHN}",
            transform_label=L"\text{lin. transf. HH}"
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"; including=["pres"]), figure)
end

if log_console()
	filesave_text(sprintf("%5.3f", r_squared), naming("r_squared"; filetype="txt"))

    filesave_text(to_tex_vector(p_reduced_optimal; decimals=3), naming("optimal_values"; including=["p"], filetype="txt"))
    filesave_text(
		to_tex_vector(IC_reduced_optimal; 
			decimals=3, 
			use_brackets = false, 
			borders=('[',']')
		), 
		naming("optimal_values"; including=["IC"], filetype="txt")
	)
end

#################################################
#		Sigmoid Nonlinear transformation		#
#################################################

shown_case = "sigmoid_infinite_train"

# Override value back to α
@overrides α = α

to_sigmoid_state(p, IC, Λ::AbstractMatrix, σ::AbstractVector, ρ::AbstractVector) = [p..., IC..., Λ..., σ..., ρ...]
from_sigmoid_state(state) = (state[1:5], state[6:7], reshape(state[8:(8+14-1)], (2, 7)), state[(end-5):(end-3)], state[(end-2):end])

@overrides Λₛ = [
    1.0 -36.0 0.0 0.0 1.0 1.0 1.0
    0.0 0.0 0.5 -0.5 1.0 1.0 1.0
]

@overrides σ₀ = [1.0, 1.0, 1.0]
@overrides ρ₀ = [0.0, 0.0, 0.0]

sigmoid(x, a, x₀) = log((x * a) / (1 - x)) + x₀

function add_sigmoids(hh_trajectory, σ, ρ)
    hh_augmented = [[u..., [sigmoid(uᵢ, σ[i], ρ[i]) for (i, uᵢ) in enumerate(u[2:end])]...] for u in eachcol(hh_trajectory)]
    return hcat(reshape.(hh_augmented, 7, 1)...)
end

function sigmoid_transform_cost(state, u_obs, du_obs, t_obs)
    p, IC, Λ, σ, ρ = from_sigmoid_state(state)

    ode = ODEProblem(FHN, IC, (t_obs[1], t_obs[end]), p)
    sol = solve(ode, Rosenbrock23(); saveat=t_obs)

    hh_augmented = add_sigmoids(u_obs, σ, ρ)
    trajectory = hcat(reshape.(sol.u, 2, 1)...)

    transformed = (Λ*hh_augmented)[:, 1:size(trajectory, 2)]

    return ode_loss(trajectory, transformed) + μ * norm(p)
end

# Find the optimal parameters
result = optimize(state -> sigmoid_transform_cost(state, u_used, du_used, t_used),
    to_sigmoid_state(p₀, IC₀, Λₛ, σ₀, ρ₀),
    Optim.Options(iterations=optim_iterations)
)
p_optimal, IC_optimal, Λ_optimal, σ_optimal, ρ_optimal = from_sigmoid_state(result.minimizer)

prob = ODEProblem(FHN, IC_optimal, (t_used[1], t_used[end]), p_optimal)
sol = solve(prob, Rosenbrock23(), saveat=t_used)
# we can use any t we desire, as FHN is autonomous
sol_dt = [FHN(u, p_optimal, 0) for u in sol.u]

trajectory = hcat(reshape.(sol.u, 2, 1)...)
trajectory_dt = hcat(reshape.(sol_dt, 2, 1)...)

u_part = u_used[:, 1:size(trajectory, 2)]
du_part = du_used[:, 1:size(trajectory_dt, 2)]
t_part = t_used[1:size(trajectory, 2)]

u_augmented = add_sigmoids(u_part, σ_optimal, ρ_optimal)

# Printing useful info
r_squared = 1 - R²_loss(trajectory, Λ_optimal * u_augmented)
println("Optimal parameters: ", p_optimal)
println("R-squared: ", r_squared)

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_augmented, t_part; legend_outside=true, legend_onside=true)
    display(figure)

    filesave(naming("linear_transformation_comparison"), figure)
end

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_augmented, t_part;
        legend_outside=false,
        legend_onside=false,
        show_title=true,
        title=L"\text{\textbf{Signomid - Infinite train}}",
        position=:rb,
        nbanks=2,
        resolution=(400, 345)
    )
    display(figure)

    filesave(naming("linear_transformation_comparison", including=["pres"]), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_augmented, trajectory, -6 .. 6, -2.5 .. 5, f;
            legend_outside=true,
            title=L"\textbf{\text{Sigmoid transformation attractor}}",
            shift_colors=true
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_augmented, trajectory, -6 .. 6, -2.5 .. 5, f;
            legend_outside=false,
            shift_colors=true,
            show_legend=true,
            title=L"\text{\textbf{Sigmoid - Infinite train}}",
            resolution=(400, 345),
            position=:rb,
            native_label=L"\text{native FHN}",
            transform_label=L"\sigma\text{-transf. HH}"
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"; including=["pres"]), figure)
end


if log_console()
	filesave_text(sprintf("%5.3f", r_squared), naming("r_squared"; filetype="txt"))

    filesave_text(to_tex_matrix(Λ_optimal'; decimals=3), naming("optimal_values"; including=["Λ"], filetype="txt"))

    filesave_text(to_tex_vector(σ_optimal; decimals=3), naming("optimal_values"; including=["σ"], filetype="txt"))
    filesave_text(to_tex_vector(ρ_optimal; decimals=3), naming("optimal_values"; including=["ρ"], filetype="txt"))

    filesave_text(to_tex_vector(p_optimal; decimals=3), naming("optimal_values"; including=["p"], filetype="txt"))
    filesave_text(
		to_tex_vector(IC_optimal; 
			decimals=3, 
			use_brackets = false, 
			borders=('[',']')
		), 
		naming("optimal_values"; including=["IC"], filetype="txt")
	)
end

#################################################
#	Sigmoid Nonlinear transformation (finite)	#
#################################################

shown_case = "sigmoid_finite_train"

α = αₛ

function sigmoid_ode_cost(p_IC, Λ, σ, ρ, u_obs, du_obs, t_obs)
    p, IC = from_reduced_state(p_IC)

    ode = ODEProblem(FHN, IC, (t_obs[1], t_obs[end]), p)
    sol = solve(ode, Rosenbrock23(); saveat=t_obs)

    hh_augmented = add_sigmoids(u_obs, σ, ρ)
    trajectory = hcat(reshape.(sol.u, 2, 1)...)

    transformed = (Λ*hh_augmented)[:, 1:size(trajectory, 2)]

    return partial_ode_loss(trajectory, transformed) + μ * norm(p)
end

result = optimize(p_IC -> sigmoid_ode_cost(p_IC, Λ_optimal, σ_optimal, ρ_optimal, u_used_fin, du_used_fin, t_used_fin),
    to_reduced_state(p₀, IC₀),
    Optim.Options(iterations=optim_iterations)
)
p_reduced_optimal, IC_reduced_optimal = from_reduced_state(result.minimizer)

prob = ODEProblem(FHN, IC_reduced_optimal, (t_used_fin[1], t_used_fin[end]), p_reduced_optimal)
sol = solve(prob, Rosenbrock23(), saveat=t_used_fin)
# we can use any t we desire, as FHN is autonomous
sol_dt = [FHN(u, p_optimal, 0) for u in sol.u]

trajectory = hcat(reshape.(sol.u, 2, 1)...)
trajectory_dt = hcat(reshape.(sol_dt, 2, 1)...)

u_part = u_used_fin[:, 1:size(trajectory, 2)]
du_part = du_used_fin[:, 1:size(trajectory_dt, 2)]
t_part = t_used_fin[1:size(trajectory, 2)]

u_augmented = add_sigmoids(u_part, σ_optimal, ρ_optimal)

# Printing useful info
r_squared = 1 - R²_loss(trajectory, Λ_optimal * u_augmented)
println("Optimal parameters: ", p_reduced_optimal)
println("R-squared: ", r_squared)

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_augmented, t_part; legend_outside=true, legend_onside=true)
    display(figure)

    filesave(naming("linear_transformation_comparison"), figure)
end

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_augmented, t_part;
        legend_outside=false,
        legend_onside=false,
        show_title=true,
        title=L"\text{\textbf{Signomid - Finite train}}",
        position=:rb,
        nbanks=2,
        resolution=(400, 345)
    )
    display(figure)

    filesave(naming("linear_transformation_comparison", including=["pres"]), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_reduced_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_augmented, trajectory, -6 .. 6, -2.5 .. 7.5, f;
            legend_outside=true,
            title=L"\textbf{\text{Sigmoid transformation attractor}}",
            shift_colors=true
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_reduced_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_augmented, trajectory, -6 .. 6, -2.5 .. 7.5, f;
            legend_outside=false,
            shift_colors=true,
            show_legend=true,
            title=L"\text{\textbf{Sigmoid - Finite train}}",
            resolution=(400, 345),
            position=:rt,
            native_label=L"\text{native FHN}",
            transform_label=L"\sigma\text{-transf. HH}"
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"; including=["pres"]), figure)
end

if log_console()
	filesave_text(sprintf("%5.3f", r_squared), naming("r_squared"; filetype="txt"))

    filesave_text(to_tex_vector(p_reduced_optimal; decimals=3), naming("optimal_values"; including=["p"], filetype="txt"))
    filesave_text(
		to_tex_vector(IC_reduced_optimal; 
			decimals=3, 
			use_brackets = false, 
			borders=('[',']')
		), 
		naming("optimal_values"; including=["IC"], filetype="txt")
	)
end

#################################################
#	Exponential Nonlinear transformation		#
#################################################

shown_case = "exponential_infinite_train"

@overrides α = α
@overrides βₑ = 10.0
β = βₑ

to_exp_state(p, IC, Λ::AbstractMatrix, λ) = [p..., IC..., Λ..., λ...]
from_exp_state(state) = (state[1:5], state[6:7], reshape(state[8:(8+10-1)], (2, 5)), state[(end-3):end])

@overrides Λₑ = [
    1.0 36.0 0.0 0.0 1.0
    0.0 0.0 0.5 -0.5 1.0
]

@overrides λ₀ = [1.0, 1.0, 1.0, 1.0]

exponential(u, λ) = exp(λ' * u)

function add_exponential(hh_trajectory, λ)
    hh_augmented = [[u..., exponential(u, λ)] for u in eachcol(hh_trajectory)]
    return hcat(reshape.(hh_augmented, 5, 1)...)
end

function exponential_transform_cost(state, u_obs, du_obs, t_obs)
    p, IC, Λ, λ = from_exp_state(state)

    ode = ODEProblem(FHN, IC, (t_obs[1], t_obs[end]), p)
    sol = solve(ode, Rosenbrock23(); saveat=t_obs)

    hh_augmented = add_exponential(u_obs, λ)
    trajectory = hcat(reshape.(sol.u, 2, 1)...)

    transformed = (Λ*hh_augmented)[:, 1:size(trajectory, 2)]

    return ode_loss(trajectory, transformed) + μ * norm(p)
end

# Find the optimal parameters
result = optimize(state -> exponential_transform_cost(state, u_used, du_used, t_used),
    to_exp_state(p₀, IC₀, Λₑ, λ₀),
    Optim.Options(iterations=optim_iterations)
)
p_optimal, IC_optimal, Λ_optimal, λ_optimal = from_exp_state(result.minimizer)

prob = ODEProblem(FHN, IC_optimal, (t_used[1], t_used[end]), p_optimal)
sol = solve(prob, Rosenbrock23(), saveat=t_used)
# we can use any t we desire, as FHN is autonomous
sol_dt = [FHN(u, p_optimal, 0) for u in sol.u]

trajectory = hcat(reshape.(sol.u, 2, 1)...)
trajectory_dt = hcat(reshape.(sol_dt, 2, 1)...)

u_part = u_used[:, 1:size(trajectory, 2)]
du_part = du_used[:, 1:size(trajectory_dt, 2)]
t_part = t_used[1:size(trajectory, 2)]

u_augmented = add_exponential(u_part, λ_optimal)

# Printing useful info
r_squared = 1 - R²_loss(trajectory, Λ_optimal * u_augmented)
println("Optimal parameters: ", p_optimal)
println("R-squared: ", r_squared)

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_augmented, t_part; legend_outside=true, legend_onside=true)
    display(figure)

    filesave(naming("linear_transformation_comparison"), figure)
end

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_augmented, t_part;
        legend_outside=false,
        legend_onside=false,
        show_title=true,
        title=L"\text{\textbf{Exponential - Infinite train}}",
        position=:rt,
        nbanks=2,
        resolution=(400, 345)
    )
    display(figure)

    filesave(naming("linear_transformation_comparison", including=["pres"]), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_augmented, trajectory, -6 .. 6, -2.5 .. 7.5, f;
            legend_outside=true,
            title=L"\textbf{\text{Exponential transformation attractor}}",
            shift_colors=true
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_augmented, trajectory, -6 .. 6, -2.5 .. 7.5, f;
            legend_outside=false,
            shift_colors=true,
            show_legend=true,
            title=L"\text{\textbf{Exponential - Infinite train}}",
            resolution=(400, 345),
            position=:rt,
            native_label=L"\text{native FHN}",
            transform_label=L"e\text{-transf. HH}"
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"; including=["pres"]), figure)
end

if log_console()
	filesave_text(sprintf("%5.3f", r_squared), naming("r_squared"; filetype="txt"))

    filesave_text(to_tex_matrix(Λ_optimal'; decimals=3), naming("optimal_values"; including=["Λ"], filetype="txt"))

    filesave_text(to_tex_vector(λ_optimal; decimals=3), naming("optimal_values"; including=["λ"], filetype="txt"))

    filesave_text(to_tex_vector(p_optimal; decimals=3), naming("optimal_values"; including=["p"], filetype="txt"))
    filesave_text(
		to_tex_vector(IC_optimal; 
			decimals=3, 
			use_brackets = false, 
			borders=('[',']')
		), 
		naming("optimal_values"; including=["IC"], filetype="txt")
	)
end

#################################################
# Exponential Nonlinear transformation (finite)	#
#################################################

shown_case = "exponential_finite_train"

α = αₛ

function exponential_ode_cost(p_IC, Λ, λ, u_obs, du_obs, t_obs)
    p, IC = from_reduced_state(p_IC)

    ode = ODEProblem(FHN, IC, (t_obs[1], t_obs[end]), p)
    sol = solve(ode, Rosenbrock23(); saveat=t_obs)

    hh_augmented = add_exponential(u_obs, λ)
    trajectory = hcat(reshape.(sol.u, 2, 1)...)

    transformed = (Λ*hh_augmented)[:, 1:size(trajectory, 2)]

    return partial_ode_loss(trajectory, transformed) + μ * norm(p)
end

result = optimize(p_IC -> exponential_ode_cost(p_IC, Λ_optimal, λ_optimal, u_used_fin, du_used_fin, t_used_fin),
    to_reduced_state(p₀, IC₀),
    Optim.Options(iterations=optim_iterations)
)
p_reduced_optimal, IC_reduced_optimal = from_reduced_state(result.minimizer)

prob = ODEProblem(FHN, IC_reduced_optimal, (t_used_fin[1], t_used_fin[end]), p_reduced_optimal)
sol = solve(prob, Rosenbrock23(), saveat=t_used_fin)
# we can use any t we desire, as FHN is autonomous
sol_dt = [FHN(u, p_optimal, 0) for u in sol.u]

trajectory = hcat(reshape.(sol.u, 2, 1)...)
trajectory_dt = hcat(reshape.(sol_dt, 2, 1)...)

u_part = u_used_fin[:, 1:size(trajectory, 2)]
du_part = du_used_fin[:, 1:size(trajectory_dt, 2)]
t_part = t_used_fin[1:size(trajectory, 2)]

u_augmented = add_exponential(u_part, λ_optimal)

# Printing useful info
r_squared = 1 - R²_loss(trajectory, Λ_optimal * u_augmented)
println("Optimal parameters: ", p_reduced_optimal)
println("R-squared: ", r_squared)

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_augmented, t_part; legend_outside=true, legend_onside=true)
    display(figure)

    filesave(naming("linear_transformation_comparison"), figure)
end

if log_plot()
    figure = linear_transformation_comparison(trajectory, Λ_optimal * u_augmented, t_part;
        legend_outside=false,
        legend_onside=false,
        show_title=true,
        title=L"\text{\textbf{Exponential - Finite train}}",
        position=:rt,
        nbanks=2,
        resolution=(400, 345)
    )
    display(figure)

    filesave(naming("linear_transformation_comparison", including=["pres"]), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_reduced_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_augmented, trajectory, -6 .. 6, -2.5 .. 7.5, f;
            legend_outside=true,
            title=L"\textbf{\text{Exponential transformation attractor}}",
            shift_colors=true
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"), figure)
end

if log_plot()
    figure = let f(V, W) = Point2f(FHN([V, W], p_reduced_optimal, 0))
        plot_linear_transformation(Λ_optimal * u_augmented, trajectory, -6 .. 6, -2.5 .. 7.5, f;
            legend_outside=false,
            shift_colors=true,
            show_legend=true,
            title=L"\text{\textbf{Exponential - Finite train}}",
            resolution=(400, 345),
            position=:rt,
            native_label=L"\text{native FHN}",
            transform_label=L"e\text{-transf. HH}"
        )
    end
    display(figure)

    filesave(naming("linear_transformation_phase_space"; including=["pres"]), figure)
end

if log_console()
	filesave_text(sprintf("%5.3f", r_squared), naming("r_squared"; filetype="txt"))
	
    filesave_text(to_tex_vector(p_reduced_optimal; decimals=3), naming("optimal_values"; including=["p"], filetype="txt"))
    filesave_text(
		to_tex_vector(IC_reduced_optimal; 
			decimals=3, 
			use_brackets = false, 
			borders=('[',']')
		), 
		naming("optimal_values"; including=["IC"], filetype="txt")
	)
end

#################################################
#		Transform FHN back to HH 				#
#################################################

# Λᵢ = pinv(Λ_optimal)

# # Pseudoinverse does NOT transform FHN to HH well (which makes sense)
# # See here
# fig, ax = lines(t_part, u_part[1, :]; label="HH: original V")
# lines!(ax, t_part, (Λᵢ*trajectory)[1, :]; label="FHN: transformed to V")
# lines!(ax, t_part, u_part[2, :]; label="HH: original m")
# lines!(ax, t_part, (Λᵢ*trajectory)[2, :]; label="FHN: transformed to m")
# axislegend(; position=:rb)
# display(fig)

# # Therefore I have no idea how to draw a phase portrait
# # The following code does NOT really work
# base_corner = [-300, 0, 0, 0]
# opposite_corner = [300, 1, 1, 1]

# base_corner_transformed = Λ_optimal * base_corner
# opposite_corner_transformed = Λ_optimal * opposite_corner

# figure = let f(X, Y) = Point2f(Λ_optimal * HH(Λᵢ * [X, Y], HH_params()))
#     streamplot(f,
#         base_corner_transformed[1] .. opposite_corner_transformed[1],
#         base_corner_transformed[2] .. opposite_corner_transformed[2];
#         stepsize=0.05, arrow_size=8, maxsteps=40, colormap=:Nizami
#     )
# end