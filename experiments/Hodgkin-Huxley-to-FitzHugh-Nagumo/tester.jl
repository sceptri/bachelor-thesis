############################################
# USE runner.jl INSTEAD - only for testing #
############################################

# Signals, that test runs should NOT save anything
TEST_DO_SAVES = false

# Only to be used when running tester.jl via console (aka in `julia tester.jl;`)
using Pkg;
Pkg.activate("..");

using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper
using DataDrivenDynamics.Noiser, DataDrivenDynamics.Sparse
using DataDrivenDiffEq, DataDrivenSparse

# Do you want to see Plots and/or console output?
# By default, all is shown
# Only console output (uncomment line below)⬇
# VERBOSE = OnlyConsole
# Only plots (uncomment line below)⬇
# VERBOSE = OnlyPlots
# Nothing (uncomment line below)⬇
# VERBOSE = NoOutput

# Set desired parameters
PARAMETERS = Dict(
    # --- Variables used in Generator ---
	# CSV name to use across both files
	"csv_name" => "hodgkin_huxley_infinite_train.csv",

    # Parameters of HH model
	# Parameter values taken from https://doi.org/10.1017/CBO9780511815706 (for us Gerstner2002-it)
    "params" => [
		:Cₘ => 1, # 1 Or Cₘ => 5
		:Iₑ => 9, # 4 or 6 or 9
		:gₙ => 120,
		:Vₙ => 115,
		:gₖ => 36,
		:Vₖ => -12,
		:gₗ => 0.3,
		:Vₗ => 10.6
	],
    # Initial condition of the HH model
    "starting_values" => [-20, 0.5, 0.5, 0.5],
    # Time range on which to simulate the HH model
    "time_range" => (0.0, 100.0),
    # Time intervals between each measuremnt of the trajectory
    "saveat" => 0.25,

    # --- Variables used in Learner ---
	# CSV name variant for loading data about infinite train of spikes in HH model
	"csv_name_infinite" => "hodgkin_huxley_infinite_train.csv",
	# Finite variant
	"csv_name_finite" => "hodgkin_huxley_finite_train.csv",

	# Parameter "weighing" the effect of norm of vector of FHN coefficients
	"μ" => 0.1,
	# Parameter "weighing" the dissimilarity between standard deviation of
	# the trajectory of the discovered FHN model compared to the original trajectory of HH 
	"α" => 50.0,
	# Parameter "weighing" the dissimilarity between standard deviation of
	# the first and second halves of the trajectory from discovered FHN model
	# > Increasing it means, that the optimization will try to find FHN parameters such that
	# > the standard deviation stays the same in time (which leads to infinite train)
	"β" => 1.0,

	# --- Linear
	# Our initial guess of the linear transformation matrix
	"Λ₀" => [
		1.0 36.0 0.0 0.0
		0.0 0.0 0.5 -0.5
	],
	# Our initial guess of the FHN model parameters that best describe
	# the transformed HH model
	"p₀" => [0.45, 1.0, 0.8, 0.7, 0.8],
	# Our initial guess, which intial condition for the FHN model simulation
	# will be optimal. Arguments of the `get_initial_cond` function:
	# - Λ 		: transformation matrix
	# - u_obs	: trajectory, from which to take the first observation and transform it
	#				via supplied transformation matrix
	"IC₀" => @later(get_initial_cond(Λ₀, u_used)),

	# Maximum number of iterations used for finding the optima (using Nelder-Mead method)
	"optim_iterations" => 10000,

	# α value for the finite part discovery (using the same α might prove inadequate)
	"αₛ" => 1.0,

	# --- Sigmoid
	# Initial guess for nonlinear transformation including sigmoids of m,h,n
	"Λₛ" => [
		1.0 36.0 0.0 0.0 1.0 1.0 1.0
		0.0 0.0 0.5 -0.5 1.0 1.0 1.0
	],
	# Initial guess for the scaling parameter of sigmoid functions
	"σ₀" => [1.0, 1.0, 1.0],
	# Initial guess for the translation parameter of sigmoid functions
	"ρ₀" => [0.0, 0.0, 0.0],

	# --- Exponential
	# Initial guess for nonlinear transformation including exponential
	"Λₑ" => [
		1.0 36.0 0.0 0.0 1.0
		0.0 0.0 0.5 -0.5 1.0
	],
	# Initial guess for the linear combination of HH state variables
	# in the argument of the exponential
	"λ₀" => [1.0, 1.0, 1.0, 1.0],
	# β value for the exponential part (as using the same β might prove inadequate)
	"βₑ" => 10.0
)

# Generate `data.csv` file in the current directory?
GENERATE_DIRECTLY = true
# If not, you have to supply "your own" `data.csv` in the same
# directory as the `learner.jl`
# >	Every time `generator.jl` runs, it creates CSV file in datasets folder
# > so you can take any CSV from there and simply rename it to `data.csv` 

# Comment out (with '#') files you do NOT want to run
# Alternatively, you can use `nogen` or `nolearn` command line options like so:
# `julia runner.jl nogen`
do_unless("nogen") && include("hodgkin_huxley_generator.jl")
do_unless("nolearn") && include("learner.jl")
