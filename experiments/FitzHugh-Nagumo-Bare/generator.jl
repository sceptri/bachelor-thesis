#>--OPTIONAL
using Revise
#>--END
using ModelingToolkit, DifferentialEquations
using DataFrames, CSV, Random

# Setting up independent variable namely time t and dependent variable V(t), W(t)
@variables t V(t) W(t)
@parameters a b c d iₑ

D = Differential(t)

# Governing equations for our system
V̇(V, W, a, b, c, d, iₑ) = V - V^3 / 3 - W + iₑ
Ẇ(V, W, a, b, c, d, iₑ) = a * (b * V - c * W + d)

params = [
    a => 0.08, b => 1.0,
    c => 0.8, d => 0.7,
    iₑ => 0.8
]
starting_values = [3.3, -2.0]
time_range = (0.0, 100.0)

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(V) ~ V̇(V, W, a, b, c, d, iₑ),
    D(W) ~ Ẇ(V, W, a, b, c, d, iₑ)
])
ode_problem = ODEProblem(ode_system, starting_values, time_range, params)

# The ODE problem may be stiff, so use a stiff solver
solution = solve(ode_problem, Rosenbrock23(); saveat=0.1)

# Compute the derivatives from the data
dict = Dict(params)
derivatives = [[
    V̇(V, W, dict[a], dict[b], dict[c], dict[d], dict[iₑ]),
    Ẇ(V, W, dict[a], dict[b], dict[c], dict[d], dict[iₑ]),
] for (V, W) in solution.u]

# transform vectors of states and derivatives into matrices
X = vcat(reshape.(solution.u, 1, 2)...)
Ẋ = vcat(reshape.(derivatives, 1, 2)...)
times = 0:0.1:100
