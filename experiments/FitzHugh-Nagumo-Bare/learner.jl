#>--OPTIONAL
using Revise
using ModelingToolkit, DifferentialEquations
using DataFrames, CSV, Random

include("generator.jl")
#>--END

#>--STEP
using DataDrivenDiffEq, DataDrivenSparse
@named data_driven_problem = ContinuousDataDrivenProblem(X', times, Ẋ')
#>--END
#>--STEP
@variables V, W
term_library = polynomial_basis([V, W], 3)
@named model_basis = Basis(term_library, [V, W])
#>--END
#>--STEP
optimizer = STLSQ(0.025)
#>--END
#>--STEP
solution = solve(data_driven_problem, model_basis, optimizer); print(solution)
fitted_system = get_basis(solution); print(fitted_system)
fitted_parameters = get_parameter_map(fitted_system)
#>--END

using DataDrivenDynamics.Sparse
using DataDrivenDiffEq, DataDrivenSparse

N = 100
Y = hcat(reverse(1:N) .^ 2, (1:N) .^ 2)
Ẏ = hcat(-2 * ones(N), 2 * ones(N))

problem = ContinuousDataDrivenProblem(Y', 1:N, Ẏ')
@variables V, W
lib = polynomial_basis([V,W], 1)
base = Basis(lib, [V,W])

optimizer = STLSQ(0.025)
@time solution = solve(problem, base, optimizer); print(solution);
fitted_system = get_basis(solution); print(fitted_system);
fitted_parameters = get_parameter_map(fitted_system)

optimizer_A = ARDLS(1e-1)
@time solution_A = solve(problem, base, optimizer_A); print(solution_A);
fitted_system_A = get_basis(solution_A); print(fitted_system_A);
fitted_parameters_A = get_parameter_map(fitted_system_A)