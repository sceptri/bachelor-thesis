using Revise
using CairoMakie, LaTeXStrings
using Random, Statistics, Distributions

# My own packages
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator
using DataDrivenDynamics.Noiser

naming = Naming([:used_noise]; filetype="pdf")
Random.seed!(12345)

@overrides elements = 10000
@overrides μ = 1
@overrides σ = 4

data = rand(Normal(μ, σ), (1, elements))

# ------- Basic additive noise
@overrides μₐ = 0
@overrides σₐ = 3

used_noise, basic_additive_noise = @varname_and_value AdditiveNoise(μₐ, σₐ)
noisy_data = basic_additive_noise(data)

if log_plot()
    figure = plot_noise_on_points(data, noisy_data)
    display(figure)

    filesave(naming("noise", including=[:("μₐ:$μₐ"), :("σₐ:$σₐ")]), figure)
end

if log_console()
	@show var(data), std(data)
	@show var(noisy_data), std(noisy_data)
	@show (σ^2 + σₐ^2), sqrt(σ^2 + σₐ^2)
end

# ------- Basic multiplicative noise
@overrides μₘ = 1
@overrides σₘ = 0.2

used_noise, basic_multiplicative_noise = @varname_and_value MultiplicativeNoise(μₘ, σₘ)
noisy_data = basic_multiplicative_noise(data)

if log_plot()
    figure = plot_noise_on_points(data, noisy_data)
    display(figure)

    filesave(naming("noise", including=[:("μₘ:$μₘ"), :("σₘ:$σₘ")]), figure)
end

if log_console()
	@show var(data), std(data)
	@show var(noisy_data), std(noisy_data)
	# from here: https://en.wikipedia.org/wiki/Distribution_of_the_product_of_two_random_variables
	@show (σ^2 + μ^2) * (σₘ^2 + μₘ^2) - μ^2 * μₘ^2, sqrt((σ^2 + μ^2) * (σₘ^2 + μₘ^2) - μ^2 * μₘ^2)
end

# ------- Variance additive noise
@overrides μ₁ = 0
@overrides σ₁ = 0.2

used_noise, variance_additive_noise = @varname_and_value AdditiveVarianceNoise(μ₁, σ₁)
noisy_data = variance_additive_noise(data)

if log_plot()
    figure = plot_noise_on_points(data, noisy_data)
    display(figure)

    filesave(naming("noise", including=[:("μ₁:$μ₁"), :("σ₁:$σ₁")]), figure)
end

if log_console()
	@show var(data), std(data)
	@show var(noisy_data), std(noisy_data)
	@show (1 + σ₁) * σ^2, sqrt((1 + σ₁) * σ^2)
end

# ------- Composite noise
used_noise, composite_noise = @varname_and_value CompositeNoise(basic_multiplicative_noise, basic_additive_noise)
noisy_data = composite_noise(data)

if log_plot()
    figure = plot_noise_on_points(data, noisy_data)
    display(figure)

    filesave(naming("noise"), figure)
end

# -------------------------------------------------------
# ------------| Different base dataset |-----------------
# -------------------------------------------------------

x_range = range(0, 100, length=10000)
@overrides f = x -> (x * sin(x))
y_values = f.(x_range)

@overrides func_label = L"x \cdot \sin (x)" 

# ------ Additive noise
@overrides μₐ_func = 0
@overrides σₐ_func = 3

used_noise, basic_additive_noise = @varname_and_value AdditiveNoise(μₐ_func, σₐ_func)
noisy_y = basic_additive_noise(y_values)

if log_plot()
    figure = plot_noise_on_func(x_range, y_values, noisy_y; func_label)
    display(figure)

    filesave(naming("example: sin(x) times x", including=[:("μₐ:$(μₐ_func)"), :("σₐ:$(σₐ_func)")]), figure)
end

# ------- Multiplicative noise
@overrides μₘ_func = 1
@overrides σₘ_func = 0.2

used_noise, basic_multiplicative_noise = @varname_and_value MultiplicativeNoise(μₘ_func, σₘ_func)
noisy_y = basic_multiplicative_noise(y_values)

if log_plot()
    figure = plot_noise_on_func(x_range, y_values, noisy_y; func_label)
    display(figure)

    filesave(naming("example: sin(x) times x", including=[:("μₘ:$(μₘ_func)"), :("σₘ:$(σₘ_func)")]), figure)
end

# ------ Additive Variance noise
@overrides μ₁_func = 0 
@overrides σ₁_func = 0.2
used_noise, variance_additive_noise = @varname_and_value AdditiveVarianceNoise(μ₁_func, σ₁_func)
noisy_y = variance_additive_noise(y_values)

if log_plot()
    figure = plot_noise_on_func(x_range, y_values, noisy_y; func_label)
    display(figure)

    filesave(naming("example: sin(x) times x", including=[:("μ₁:$(μ₁_func)"), :("σ₁:$(σ₁_func)")]), figure)
end

# ------ Composite noise
used_noise, composite_noise = @varname_and_value CompositeNoise(basic_multiplicative_noise, basic_additive_noise)
noisy_y = composite_noise(y_values)

if log_plot()
    figure = plot_noise_on_func(x_range, y_values, noisy_y; func_label)
    display(figure)

    filesave(naming("example: sin(x) times x"), figure)
end