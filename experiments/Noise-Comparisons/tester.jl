############################################
# USE runner.jl INSTEAD - only for testing #
############################################

# Signals, that test runs should NOT save anything
TEST_DO_SAVES = false

# Only to be used when running tester.jl via console (aka in `julia tester.jl;`)
using Pkg;
Pkg.activate("..");

using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Noiser
using LaTeXStrings

# Do you want to see Plots and/or console output?
# By default, all is shown
# Only console output (uncomment line below)⬇
# VERBOSE = OnlyConsole
# Only plots (uncomment line below)⬇
# VERBOSE = OnlyPlots
# Nothing (uncomment line below)⬇
# VERBOSE = NoOutput

# Set desired parameters
PARAMETERS = Dict(
    # --- Noise on random data points ---
	# Generation of random points (that are going to be noised)
	# Number of points to generate
	"elements" => 10000,
	# Parameters of a normal distribution used for genreating such points
	"μ" => 1,
	"σ" => 4,

	# -- Additive Noise
	# Parameters of a additive white Gaussian noise
	"μₐ" => 0,
	"σₐ" => 3,

	# -- Multiplicative Noise
	# Parameters of a multiplicative white Gaussian noise
	"μₘ" => 1,
	"σₘ" => 0.2,

	# -- Additive Variance Noise
	# Parameters of a additive white Gaussian noise
	# with the variance of data
	"μ₁" => 0,
	"σ₁" => 0.2,

	# --- Noise on a testing function ---
	# Testing function to be used for comparison
	# between different noises
	"f" => x -> (x * sin(x)),
	# Plot label for the testing function
	"func_label" => L"x \cdot \sin (x)",

	# -- Additive Noise
	# Parameters of a additive white Gaussian noise
	"μₐ_func" => 0,
	"σₐ_func" => 3,

	# -- Multiplicative Noise
	# Parameters of a multiplicative white Gaussian noise
	"μₘ_func" => 1,
	"σₘ_func" => 0.2,

	# -- Additive Variance Noise
	# Parameters of a additive white Gaussian noise
	# with the variance of data
	"μ₁_func" => 0,
	"σ₁_func" => 0.2
)

# Run the actual code
include("generator.jl")
