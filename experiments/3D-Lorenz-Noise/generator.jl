#########################################
#		Program intilizatization	 	#
#########################################

###
#	Data generator file for 3D Lorenz system
#	The system is given by equations
#
#		ẋ = σ (y - x)
#		ẏ = x (ρ - z) - y
#		ż = x ⋅ y  - β ⋅ z
#
#	Purpose of this is to generate an ensemble of trajectories
#	that are to be used to explore behavior of SINDy on different
#	noise levels
###

using Revise
using ModelingToolkit, DifferentialEquations
using CairoMakie, LaTeXStrings
using DataFrames, CSV, Random

# My own packages
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator

# Define the naming scheme used through out the script
naming = Naming()
push!(naming, NamingField(:params => :(parse_param_vector(params)); subdirectory_behavior=ProjectKeeper.make_subdirectory))
push!(naming, NamingField(:starting_values; include_key=true))
push!(naming, NamingField(:full_time; include_key=true))
push!(naming, NamingField(:time_range; include_key=true))
push!(naming, NamingField(:saveat; include_key=true))
push!(naming, NamingField(:ic_count; include_key=true))

# For reproducibility reasons, set a random seed
Random.seed!(12345)

# Set variables used in ODE, as well as parameters
@variables t x(t) y(t) z(t)
@parameters σ, ρ, β
# and define a differential
D = Differential(t)

# Set the equations defining the system
ẋ(x, y, z, σ) = σ * (y - x)
ẏ(x, y, z, ρ) = x * (ρ - z) - y
ż(x, y, z, β) = x * y - β * z

# and their parameters, along with initial condition and an interval to simulate the ODE upon
@overrides params = [
    :σ => 10,
    :ρ => 28,
    :β => 8 / 3
]
@overrides starting_values = [1, 1, 1]
@overrides full_time = (0.0, 30.0)

# Construct the ODESystem object
@named ode_system = ODESystem([
    D(x) ~ ẋ(x, y, z, σ),
    D(y) ~ ẏ(x, y, z, ρ),
    D(z) ~ ż(x, y, z, β)
])

# Solve the system for a long time to get a trajactory, from which we can sample
# desired initial conditions
@overrides saveat = 0.01
ode_problem = ODEProblem(ode_system, starting_values, full_time, ode_params(params))
solution = solve(ode_problem, Tsit5(); saveat)

# Actually sample the used initial conditions
@overrides ic_count = 10
initial_conditions = rand(solution.u, ic_count)
# and define the simulation time interval for each of thes ICs 
@overrides time_range = (0.0, 10)

# Construct a ODEProblem anew, this time modified to work with EnsembleProblem
ode_problem = ODEProblem(ode_system,
    initial_conditions[1],
    time_range,
    ode_params(params)
)

"""
EnsembleProblem function, which allows us to change starting value based on current index
"""
function problem_function(problem, index, repeat)
    @. problem.u0 = initial_conditions[index]
    problem
end

# Solve the EnsembleProblem for all of our initial conditions
ensemble_problem = EnsembleProblem(ode_problem; prob_func=problem_function)
solution = solve(ensemble_problem; saveat, trajectories=length(initial_conditions))

# Convert the solution to a saveable format
data_to_save = feedSaveData(
    solution,
    (:x, :y, :z), # names of variables
    (coords, t) -> [
        ẋ(coords..., values(params)[1]),
        ẏ(coords..., values(params)[2]),
        ż(coords..., values(params)[3])
    ]
)

# Plot the solution if needed
if log_plot()
    figure = generated_trajectory_plot(data_to_save; legend_outside=true, resolution=(450, 300))
    display(figure)

    filesave(naming("generated-trajectory"), figure)
end

# Save the found trajectories into a CSV (first converting it to a DataFrame)
dataframe = DataFrame(prepareSaveData(data_to_save, true)...)
not_test_suite() && CSV.write(naming("data"; filetype="csv", directory="datasets", as_directory=false), dataframe)

if isdefined(Main, :GENERATE_DIRECTLY) && GENERATE_DIRECTLY && not_test_suite()
	CSV.write(here("data.csv"), dataframe)
end