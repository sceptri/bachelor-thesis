\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{V} = 0.7 + 0.83 \cdot \hat{V}  - 0.85 \cdot \hat{W}  - 0.29 \cdot \hat{V} ^3,\\
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{W} = 0.05 + 0.08 \cdot \hat{V}  - 0.05 \cdot \hat{W} 
