#########################################
#		Program intilizatization	 	#
#########################################

###
#	Data generator file for 2D FitzHugh-Nagumo for noise comparisons
#	The system is given by equations
#
#		V̇ = V - V³ / 3 - W + iₑ
#		Ẇ = a ⋅ (b ⋅ V - c ⋅ W + d)
#
#	Purpose of this file is to generate simple singular trajectory
###

using Revise
using ModelingToolkit, DifferentialEquations
using CairoMakie, LaTeXStrings
using DataFrames, CSV, Random

# My own packages
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator

# Define the naming scheme used through out the script
naming = Naming([:params => :(parse_param_vector(params)),])
push!(naming, NamingField(:starting_values; include_key=true))
push!(naming, NamingField(:time_range; include_key=true))
push!(naming, NamingField(:saveat; include_key=true))

Random.seed!(12345)

#########################################
#		Setting up the system	 		#
#########################################

# Setting up independent variable namely time t and dependent variable V(t), W(t)
@variables t V(t) W(t)
@parameters a b c d iₑ

# Our system is one ODE, so we only need differential with respect to time
D = Differential(t)

# Governing equation(s) for our system
V̇(V, W, a, b, c, d, iₑ) = V - V^3 / 3 - W + iₑ
Ẇ(V, W, a, b, c, d, iₑ) = a * (b * V - c * W + d)

@overrides params = [
    :a => 0.08,
    :b => 1.0,
    :c => 0.8,
    :d => 0.7,
    :iₑ => 0.8
]
@overrides starting_values = [3.3, -2.0]
@overrides time_range = (0.0, 100.0)

#########################################
#		Solving the system		 		#
#########################################

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(V) ~ V̇(V, W, a, b, c, d, iₑ),
    D(W) ~ Ẇ(V, W, a, b, c, d, iₑ)
])

ode_problem = ODEProblem(ode_system, starting_values, time_range, ode_params(params))

# We expect the ODE problem to be stiff, so by default we use
# the Rosenbrock23 method
@overrides saveat = 0.5
solution = solve(ode_problem, Rosenbrock23(); saveat)

#########################################
#	Plotting the solved trajectory	 	#
#########################################

# For ease of handling, we create a temporary object to hide away saving logic
data_to_save = feedSaveData(
    solution,
    (:V, :W), # names of variables
    (coords, t) -> [
        V̇(coords..., values(params)...),
        Ẇ(coords..., values(params)...)
    ]
)

if log_plot()
	figure = generated_trajectory_plot(data_to_save)
	display(figure)

	filesave(naming("generated-trajectory"), figure)
end

if log_plot()
	figure = let f(V, W) = Point2f(V̇(V, W, values(params)...), Ẇ(V, W, values(params)...))
        phase_space_plot(data_to_save, f)
    end

	display(figure)
	filesave(naming("all-variables-generated-trajectory"), figure)
end

#########################################
#	Saving the solved trajectories	 	#
#########################################

dataframe = DataFrame(prepareSaveData(data_to_save, true)...)
not_test_suite() && CSV.write(naming("data"; filetype="csv", directory="datasets", as_directory=false), dataframe)

if isdefined(Main, :GENERATE_DIRECTLY) && GENERATE_DIRECTLY && not_test_suite()
    CSV.write(here("data.csv"), dataframe)
end