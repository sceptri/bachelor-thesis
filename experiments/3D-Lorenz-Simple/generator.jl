#########################################
#		Program intilizatization	 	#
#########################################

###
#	Data generator file for 3D Lorenz system
#	The system is given by equations
#
#		ẋ = σ (y - x)
#		ẏ = x (ρ - z) - y
#		ż = x ⋅ y  - β ⋅ z
#
#	Purpose of this is to generate simple singular trajectory
###

using Revise
using ModelingToolkit, DifferentialEquations
using CairoMakie, LaTeXStrings
using DataFrames, CSV, Random

# My own packages
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator

# Define the naming scheme used through out the script
naming = Naming([:params => :(parse_param_vector(params)),])
push!(naming, NamingField(:starting_values; include_key=true))
push!(naming, NamingField(:time_range; include_key=true))
push!(naming, NamingField(:saveat; include_key=true))

Random.seed!(12345)

#########################################
#		Setting up the system	 		#
#########################################

# Setting up independent variable namely time t and dependent variable x(t), y(t), z(t)
@variables t x(t) y(t) z(t)
@parameters σ, ρ, β

# Our system is one ODE, so we only need differential with respect to time
D = Differential(t)

# Governing equation(s) for our system
ẋ(x, y, z, σ) = σ * (y - x)
ẏ(x, y, z, ρ) = x * (ρ - z) - y
ż(x, y, z, β) = x * y - β * z

@overrides params = [
    :σ => 10,
    :ρ => 28,
    :β => 8 / 3
]
@overrides starting_values = [1, 1, 1]
@overrides time_range = (0.0, 30)

#########################################
#		Solving the system		 		#
#########################################

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(x) ~ ẋ(x, y, z, σ),
    D(y) ~ ẏ(x, y, z, ρ),
    D(z) ~ ż(x, y, z, β)
])

ode_problem = ODEProblem(ode_system, starting_values, time_range, ode_params(params))

# We use Tsit5 to solve the problem (generate the trajectory/ies)
# Other methods can be DP5, which is equivalent to matlab ode45
# > Tsit5 is said to often be more effcient for non-stiff problems
# `saveat` defines how often a position should be saved
@overrides saveat = 0.01
solution = solve(ode_problem, Rosenbrock23(); saveat)

#########################################
#	Plotting the solved trajectory	 	#
#########################################

# For ease of handling, we create a temporary object to hide away saving logic
data_to_save = feedSaveData(
    solution,
    (:x, :y, :z), # names of variables
    (coords, t) -> [
        ẋ(coords..., values(params)[1]),
        ẏ(coords..., values(params)[2]),
        ż(coords..., values(params)[3])
    ]
)

if log_plot()
    figure = generated_trajectory_plot(data_to_save; legend_outside = true, resolution = (450, 300))
    display(figure)

    filesave(naming("generated-trajectory"), figure)
end
#########################################
#	Saving the solved trajectories	 	#
#########################################

dataframe = DataFrame(prepareSaveData(data_to_save, true)...)
not_test_suite() && CSV.write(naming("data"; filetype="csv", directory="datasets", as_directory=false), dataframe)

if isdefined(Main, :GENERATE_DIRECTLY) && GENERATE_DIRECTLY && not_test_suite()
    CSV.write(here("data.csv"), dataframe)
end