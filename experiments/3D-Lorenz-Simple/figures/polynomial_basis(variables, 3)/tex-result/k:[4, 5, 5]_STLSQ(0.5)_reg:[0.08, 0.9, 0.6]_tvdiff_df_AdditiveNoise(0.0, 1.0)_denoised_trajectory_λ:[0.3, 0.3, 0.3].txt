\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{x} =&\hphantom{+} 9.31 \cdot \hat{y}  - 9.16 \cdot \hat{x} ,\\
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{y} =&\hphantom{+} 9.72 \cdot \hat{y}  - 5.42 - 11.82 \cdot \hat{x} ,\\
\frac{\mathrm{d}}{\mathrm{d}\,t}\hat{z} =&\hphantom{+} 0.93 \cdot \hat{x}  \cdot \hat{y}  - 10.75 - 2.03 \cdot \hat{z} 
