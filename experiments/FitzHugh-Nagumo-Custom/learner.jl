#>--OPTIONAL
using Revise
using ModelingToolkit, DifferentialEquations
using DataFrames, CSV, Random

using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator
using DataDrivenDynamics.SINDy

include("generator.jl")
#>--END

basis = PolynomialLibrary(3, 2)
optimizer = STLS()
Ξ = discover(X, Ẋ, basis, optimizer; max_iter=100)
prettyprint(Ξ, basis; vars=["V" "W"])

optimizer_A = ARDLS()
Ξ_A, history = discover(X, Ẋ, basis, optimizer_A; max_iter=100)
prettyprint(Ξ_A, basis; vars=["V" "W"])


# --- Really *TOY* example ---
optimizer = STLS()
optimizer_A = ARDLS()

N = 100
Y = hcat(reverse(1:N).^2,(1:N).^2)
Ẏ = hcat(-2 * ones(N), 2*ones(N))

base = PolynomialLibrary(1, 2)
base(Y)

@time xi = discover(Y, Ẏ, base, optimizer_A; max_iter = 100)
prettyprint(xi, base; vars=["x" "y"])

Xi = discover(Y, Ẏ, base, optimizer; max_iter = 100)
prettyprint(Xi, base; vars=["x" "y"])

A = [1 2; 3 4]
b = [1, 2]
inv(A) * b
A \ b