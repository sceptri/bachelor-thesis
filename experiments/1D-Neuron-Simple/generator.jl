#########################################
#		Program intilizatization	 	#
#########################################

###
#	Data generator file for 1D Neuron experiment
#	The system is given by equation
#
#		ẋ = k ⋅ x ⋅ (a - x) ⋅ (x - 1) + I
#
#	Purpose of this is to generate simple singular trajectory
###

using Revise
using ModelingToolkit, DifferentialEquations
using CairoMakie, LaTeXStrings
using DataFrames, CSV, Random

# My own packages
using DataDrivenDynamics
using DataDrivenDynamics.ProjectKeeper, DataDrivenDynamics.Generator

# Define the naming scheme used through out the script
naming = Naming([:params => :(parse_param_vector(params)),])
push!(naming, NamingField(:starting_values; include_key=true))
push!(naming, NamingField(:time_range; include_key=true))
push!(naming, NamingField(:saveat; include_key=true))

Random.seed!(12345)

#########################################
#		Setting up the system	 		#
#########################################

# Setting up independent variable namely time t and dependent variable x(t) 
@variables t x(t)
@parameters k a I

# Our system is one ODE, so we only need differential with respect to time
D = Differential(t)

# Governing equation(s) for our system
ẋ(x, k, a, I) = k * x * (a - x) * (x - 1) + I

parameters = [
    :k => 1,
    :a => 0.5,
    :I => 1
]
starting_values = [-1]
time_range = (0.0, 100)

# For better illustration of the behaviour of a given system
# we draw square phase portrait
if log_plot()
    f(x, t) = Point2f(ẋ(x, values(parameters)...), 1)

    lower_limit = -10
    upper_limit = 10

    # Drawing the vector fieĺd, default arrow_size = 0.07
    streamplot(f, lower_limit .. upper_limit, lower_limit .. upper_limit, colormap=:magma, arrow_size=10)
end

#########################################
#		Solving the system		 		#
#########################################

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([D(x) ~ ẋ(x, k, a, I)])
ode_problem = ODEProblem(ode_system, starting_values, time_range, ode_params(parameters))

# We use Tsit5 to solve the problem (generate the trajectory/ies)
# Other methods can be DP5, which is equivalent to matlab ode45
# > Tsit5 is said to often be more effcient for non-stiff problems
# `saveat` defines how often a position should be saved
saveat = 0.01
solution = solve(ode_problem, Rosenbrock23(); saveat)

#########################################
#	Saving the solved trajectory	 	#
#########################################

# For ease of handling, we create a temporary object to hide away saving logic
data_to_save = feedSaveData(
    solution,
    (:x,), # names of variables
    (x, t) -> ẋ(x, values(parameters)...)
)

if log_plot()
    figure = generated_trajectory_plot(data_to_save; legend_outside=true, resolution=(450, 300))
    display(figure)

    filesave(naming("generated-trajectory"), figure)
end

#########################################
#	Saving the solved trajectories	 	#
#########################################

dataframe = DataFrame(prepareSaveData(data_to_save, true)...)
not_test_suite() && CSV.write(naming("data"; filetype="csv", directory="datasets", as_directory=false), dataframe)