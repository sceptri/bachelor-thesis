#########################################
#		Program intilizatization	 	#
#########################################

###
#	Data generator file for 3D Lorenz system
#	The system is given by equations
#
#		̇x = σ (y - x)
#		ẏ = x (ρ - z) - y
#		ż = x ⋅ y  - β ⋅ z
#
#	Purpose of this is to generate simple singular trajectory
###

using ModelingToolkit
using GLMakie
using DifferentialEquations
using DataFrames
using CSV

include("../../src/julia/utils.jl")
include("../../src/julia/generator-utils.jl")

#########################################
#		Setting up the system	 		#
#########################################

# Setting up independent variable namely time t and dependent variable x(t) 
@variables t x(t) y(t) z(t)
@parameters σ, ρ, β

# Our system is one ODE, so we only need differential with respect to time
D = Differential(t)

# Governing equation(s) for our system
ẋ(x, y, z, σ) = σ * (y - x)
ẏ(x, y, z, ρ) = x * (ρ - z) - y
ż(x, y, z, β) = x * y - β * z

params = [
    :σ => 10,
    :ρ => 28,
    :β => 8 / 3
]

starting_values = [1, 1, 1]

time_range = (0.0, 30)

#########################################
#		Solving the system		 		#
#########################################

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(x) ~ ẋ(x, y, z, σ),
    D(y) ~ ẏ(x, y, z, ρ),
    D(z) ~ ż(x, y, z, β)
])

ode_problem = ODEProblem(ode_system, starting_values, time_range, ode_params(params))

# We use Tsit5 to solve the problem (generate the trajectory/ies)
# Other methods can be DP5, which is equivalent to matlab ode45
# > Tsit5 is said to often be more effcient for non-stiff problems
# `saveat` defines how often a position should be saved
solution = solve(ode_problem, Tsit5(), saveat=0.01)

#########################################
#	Plotting the solved trajectory	 	#
#########################################

# For ease of handling, we create a temporary object to hide away saving logic
data_to_save = feedSaveData(
    solution,
    (:x, :y, :z), # names of variables
    (coords, t) -> [
        ẋ(coords..., values(params)[1]),
        ẏ(coords..., values(params)[2]),
        ż(coords..., values(params)[3])
    ]
)

if log_plot()
    figure = Figure(resolution=(700, 600))
    axis = Axis(figure[1, 1], backgroundcolor=:white)

    for trajectory in data_to_save.trajectories
        lines!(axis, Float64.(trajectory'[1, :]),
            linewidth=1,
            colormap=:thermal,
            color=range(0, 1, length=length(solution.u))
        )
    end

    figure
end

#########################################
#	Saving the solved trajectories	 	#
#########################################

dataframe = DataFrame(prepareSaveData(data_to_save, true)...)
not_test_suite() && CSV.write(here("full_data.csv"), dataframe)