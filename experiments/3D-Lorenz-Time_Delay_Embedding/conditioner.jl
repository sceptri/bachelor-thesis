#########################################
#		Program intilizatization	 	#
#########################################

###
#	Conditioner for data extracted from full_data.csv
# 	Only 1 variable will be used, then using
# 			Time-Delay Embedding
# 	we will try to reconstruct the trajectory in a artificial basis 
###

using DataFrames
using CSV
using GLMakie
using LinearAlgebra

GLMakie.activate!()

include("../../src/julia/utils.jl")
include("../../src/julia/generator-utils.jl")

using .SaveUtils, .LogUtils

naming = Naming([:extract_variable]; filetype="png")
push!(naming, NamingField(:delays_count, :time_shifts; include_key=true))

extract_variable = :x

# Loading trajectory (and time) data from supplied csv
full_dataframe = DataFrame(CSV.File(here("full_data.csv")))

log_plot() && lines(Matrix(full_dataframe[:, symbol_at.([:x, :y, :z], 1)]))

extracted_trajectory = full_dataframe[:, symbol_at(extract_variable, 1)]

extracted_time = full_dataframe[:, symbol_at(:time, 1)]
extracted_timestep = (extracted_time[end] - extracted_time[begin]) / length(extracted_time)

log_plot() && GLMakie.lines(extracted_trajectory)

delays_count = 6

matrix_width = length(extracted_trajectory) - delays_count
henkel = zeros(eltype(extracted_trajectory), (delays_count, matrix_width))
for index in 1:matrix_width
    henkel[:, index] .= extracted_trajectory[index:(delays_count+index-1)]
end

U, S, V = svd(henkel)

if log_plot()
    fig = GLMakie.scatter(S / sum(S))
    filesave(naming("singular-values"), fig)
    display(fig)
end

significant_variables = 3
trajectories = V[:, 1:significant_variables]'

log_console() && println(sum(S[1:significant_variables]) / sum(S))

#########################################
#	Plotting the solved trajectory	 	#
#########################################

# For ease of handling, we create a temporary object to hide away saving logic
data_to_save = feedSaveData(
    trajectories',
    collect(0:size(trajectories)[2]-1) .* extracted_timestep,
    (:α, :β, :γ), # names of variables
)

if log_plot()
    figure = Figure()

    for variable_index in 1:significant_variables
        grid_layout = figure[variable_index, 1] = GridLayout()
        axis = Axis(grid_layout[1, 1], backgroundcolor=:white)

        for iter_trajectory in data_to_save.trajectories
            current_trajectory = iter_trajectory'[variable_index, :]

            lines!(axis, Float64.(current_trajectory),
                linewidth=1,
                colormap=:thermal,
                color=range(0, 1, length=length(current_trajectory))
            )
        end
    end

    grid_layout = figure[significant_variables+1, 1] = GridLayout()
    axis = Axis(grid_layout[1, 1], backgroundcolor=:white)

    lines!(axis, Float64.(extracted_trajectory), linewidth=1)

    filesave(naming("time-delay-embedding-each-variable"; including=[:("significant-variables:$significant_variables")]), figure)
    figure
end

if log_plot()
    figure = Figure()
    axis = Axis3(figure[1, 1], backgroundcolor=:white)

    for iter_trajectory in data_to_save.trajectories
        x, y, z = [Float64.(row) for row in eachrow(iter_trajectory')] .|> collect

        current_trajectory = Point3.(x, y, z)

        GLMakie.lines!(axis, current_trajectory;
            linewidth=1,
            colormap=:thermal,
            color=range(0, 1, length=length(current_trajectory))
        )
    end

    # GLMakie can only save to PNG, for vector formats use CairoMakie
    filesave(naming("time-delay-embedding"; including=[:("significant-variables:$significant_variables")]), figure)

    figure
end

#########################################
#	Saving the solved trajectories	 	#
#########################################

save_dataframe = DataFrame(prepareSaveData(data_to_save)...)
not_test_suite() && CSV.write(here("data.csv"), save_dataframe)